package repository

import (
	"fmt"
	types2 "gitlab.yellow-idea.com/yellow-idea-go-data-center/modules-standard/line/types"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	"testing"
)

func TestLineUserRepository_CreateData(t *testing.T) {
	payload := &types2.TableDimLineUser{
		//Name: "Koko",
	}

	a := NewLineUserRepository(1,"Demo")
	res := a.CreateData(payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}

func TestLineUserRepository_ListData(t *testing.T) {
	payload := &types2.FilterLineUserList{
		Sort:   "",
		Order:  "",
		Offset: 0,
		Limit:  10,
		//IsExport: False,
		Filter: &types2.FilterLineUserListItems{
			Name: "",
		},
	}

	a := NewLineUserRepository(1,"Demo")
	res := a.ListData(payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(helperSDK.InterfaceToJson(&res.Rows))
}

func TestLineUserRepository_ShowData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewLineUserRepository(1,"Demo")
	res := a.ShowData(id)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(helperSDK.InterfaceToJson(&res.Data))
}

func TestLineUserRepository_UpdateData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewLineUserRepository(1,"Demo")
	payload := &types2.TableDimLineUser{
		//Name: "Koko-v2",
	}
	res := a.UpdateData(id, payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}

func TestLineUserRepository_DeleteData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewLineUserRepository(1,"Demo")
	res := a.DeleteData(id)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}
