package types

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type TableDimLineUser struct {
	ID               primitive.ObjectID `json:"id" bson:"_id"`
	LineUserId       string             `json:"line_user_id" bson:"line_user_id"`
	LineDisplayName  string             `json:"line_display_name" bson:"line_display_name"`
	LineDisplayImage string             `json:"line_display_image" bson:"line_display_image"`
	Email            string             `json:"email" bson:"email"`
	PhoneNumber      string             `json:"phone_number" bson:"phone_number"`
	CreatedAt        time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt        time.Time          `json:"updated_at" bson:"updated_at"`
	DeletedAt        time.Time          `json:"deleted_at" bson:"deleted_at"`
}

type RequestUpdateProfile struct {
	LineUserId       string `json:"mid" bson:"mid"`
	LineDisplayName  string `json:"name" bson:"name"`
	LineDisplayImage string `json:"avatar" bson:"avatar"`
}

type FilterLineUserListItems struct {
	Name string `json:"name" bson:"name"`
}

type FilterLineUserList struct {
	Sort     string `json:"sort" bson:"sort"`
	Order    string `json:"order" bson:"order"`
	Offset   int64  `json:"offset" bson:"offset"`
	Limit    int64  `json:"limit" bson:"limit"`
	IsExport bool   `json:"is_export" bson:"is_export"`
	Filter   *FilterLineUserListItems
}
