package service

import (
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules-standard/line/repository"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules-standard/line/types"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
)

type LineUserService interface {
	ListData(payload *types.FilterLineUserList) *typesSDK.SaveRepositoryResponse
	ShowData(id string) *typesSDK.SaveRepositoryResponse
	CreateData(payload *types.TableDimLineUser) *typesSDK.SaveRepositoryResponse
	UpdateData(id string, payload *types.TableDimLineUser) *typesSDK.SaveRepositoryResponse
	DeleteData(id string) *typesSDK.SaveRepositoryResponse
	// App
	UpdateProfile(payload *types.RequestUpdateProfile) *typesSDK.SaveRepositoryResponse
}

type lineUserService struct {
	LineUserRepository repository.LineUserRepository
}

func NewLineUserService(mgoServerType int8, mgoDatabaseName string) LineUserService {
	return &lineUserService{
		LineUserRepository: repository.NewLineUserRepository(mgoServerType, mgoDatabaseName),
	}
}

func (x *lineUserService) ListData(payload *types.FilterLineUserList) *typesSDK.SaveRepositoryResponse {
	res := x.LineUserRepository.ListData(payload)
	return res
}

func (x *lineUserService) ShowData(id string) *typesSDK.SaveRepositoryResponse {
	res := x.LineUserRepository.ShowData(id)
	return res
}

func (x *lineUserService) CreateData(payload *types.TableDimLineUser) *typesSDK.SaveRepositoryResponse {
	res := x.LineUserRepository.CreateData(payload)
	return res
}

func (x *lineUserService) UpdateData(id string, payload *types.TableDimLineUser) *typesSDK.SaveRepositoryResponse {
	res := x.LineUserRepository.UpdateData(id, payload)
	return res
}

func (x *lineUserService) DeleteData(id string) *typesSDK.SaveRepositoryResponse {
	res := x.LineUserRepository.DeleteData(id)
	return res
}

func (x *lineUserService) UpdateProfile(payload *types.RequestUpdateProfile) *typesSDK.SaveRepositoryResponse {
	id := x.LineUserRepository.ShowDataByMid(payload.LineUserId)
	if id == "-" {
		return x.LineUserRepository.CreateData(&types.TableDimLineUser{
			LineUserId:       payload.LineUserId,
			LineDisplayName:  payload.LineDisplayName,
			LineDisplayImage: payload.LineDisplayImage,
		})
	} else {
		return x.LineUserRepository.UpdateData(helperSDK.InterfaceToString(id), &types.TableDimLineUser{
			LineUserId:       payload.LineUserId,
			LineDisplayName:  payload.LineDisplayName,
			LineDisplayImage: payload.LineDisplayImage,
		})
	}
}
