package service

import (
	"fmt"
	types2 "gitlab.yellow-idea.com/yellow-idea-go-data-center/modules-standard/line/types"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	"testing"
)

func TestLineUserService_CreateData(t *testing.T) {
	payload := &types2.TableDimLineUser{
		//Name: "Koko-oook",
	}

	a := NewLineUserService()
	res := a.CreateData(payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}

func TestLineUserService_ListData(t *testing.T) {
	payload := &types2.FilterLineUserList{
		Sort:   "",
		Order:  "",
		Offset: 0,
		Limit:  10,
		//IsExport: False,
		Filter: &types2.FilterLineUserListItems{
			Name: "",
		},
	}
	a := NewLineUserService()
	res := a.ListData(payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(helperSDK.InterfaceToJson(&res.Rows))
}

func TestLineUserService_ShowData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewLineUserService()
	res := a.ShowData(id)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(helperSDK.InterfaceToJson(&res.Data))
}

func TestLineUserService_UpdateData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewLineUserService()
	payload := &types2.TableDimLineUser{
		//Name: "Koko-v2",
	}
	res := a.UpdateData(id, payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}

func TestLineUserService_DeleteData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewLineUserService()
	res := a.DeleteData(id)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}
