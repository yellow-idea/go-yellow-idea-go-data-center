package router

import (
	"github.com/gin-gonic/gin"
	routerDutchie "gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchie_v2/router"
	routerDutcmill "gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchmill/router"
	routerEnfagrow "gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/enfagrow/router"
	routerKohKae "gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/kohkae/router"
	routerNivea "gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/nivea/router"
	routerUniliverMTKhukitd "gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/unilever-khukitd/router"
	routerUniliverMTMakro "gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/unilever-makro/router"
)

func RouteAlphaV1GroupApp(r *gin.Engine) *gin.Engine {
	// Note: Demo Router
	//DemoRouterApp(r)
	routerDutchie.DutchieV2RouterApp(r)
	routerKohKae.KohKaeRouterApp(r)
	routerNivea.NiveaRouterApp(r)
	routerEnfagrow.EnFaGrowRouterApp(r)
	return r
}

func RouteAlphaV1GroupCms(r *gin.Engine) *gin.Engine {
	// Note: Demo Router
	//DemoRouterCms(r)
	routerDutchie.DutchieV2RouterCms(r)
	routerKohKae.KohKaeRouterCms(r)
	routerNivea.NiveaRouterCms(r)
	routerDutcmill.RouteCms(r)
	routerUniliverMTMakro.RouteCms(r)
	routerUniliverMTKhukitd.RouteCms(r)
	routerEnfagrow.EnFaGrowRouterCms(r)
	return r
}
