package router

import (
	"fmt"
	"github.com/gin-gonic/gin"
	demoService "gitlab.yellow-idea.com/yellow-idea-go-data-center/service/demo"
	typesDemo "gitlab.yellow-idea.com/yellow-idea-go-data-center/types/demo"
	httpSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/http"
	"net/http"
)

func DemoRouterApp(r *gin.Engine) {
	// Demo
	dr := r.Group("/demo")
	{
		s := demoService.NewDemoService()
		dr.GET("/welcome", func(c *gin.Context) {
			res := s.GetDemo2()
			c.String(http.StatusOK, res)
		})
	}
}

func DemoRouterCms(r *gin.Engine) {
	// Demo
	dr := r.Group("/demo")
	{
		s := demoService.NewDemoService()
		dr.GET("/welcome", func(c *gin.Context) {
			res := s.GetDemo2()
			c.String(http.StatusOK, res)
		})
		dr.GET("/user/:name", func(c *gin.Context) {
			name := c.Param("name")
			res := s.GetDemo1(name)
			c.String(http.StatusOK, res)
		})
		dr.GET("/", func(c *gin.Context) {
			res := s.GetDemo3()
			c.JSON(http.StatusOK, res)
		})
		// List
		dr.POST("/list", func(c *gin.Context) {
			var payload *typesDemo.ListDataRequest
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := s.ListData(payload)
			c.JSON(http.StatusOK, &gin.H{
				"rows":  res.Rows,
				"total": res.Total,
			})
		})
		// Show
		dr.GET("/show/:id", func(c *gin.Context) {
			id := c.Param("id")
			res := s.ShowData(id)
			c.JSON(http.StatusOK, res.Data)
		})
		// Create
		dr.POST("/new", func(c *gin.Context) {
			var payload *typesDemo.TableDemo
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := s.CreateData(payload)
			c.JSON(http.StatusOK, res)
		})
		// Update
		dr.PUT("/edit/:id", func(c *gin.Context) {
			id := c.Param("id")
			var payload *typesDemo.TableDemo
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := s.UpdateData(id, payload)
			c.JSON(http.StatusOK, res)
		})
		// Delete
		dr.DELETE("/:id", func(c *gin.Context) {
			id := c.Param("id")
			res := s.DeleteData(id)
			c.JSON(http.StatusOK, res)
		})
	}
}
