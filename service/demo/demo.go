package demo

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/repository"
	repoDemo "gitlab.yellow-idea.com/yellow-idea-go-data-center/repository/demo"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/service"
	typesDemo "gitlab.yellow-idea.com/yellow-idea-go-data-center/types/demo"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
)

type demoService struct {
	DemoRepository repository.DemoRepository
}

func NewDemoService() service.DemoService {
	return &demoService{
		DemoRepository: repoDemo.NewDemoRepository(),
	}
}

func (x *demoService) GetDemo1(name string) string {
	return fmt.Sprintf("Hello Version 3 %s", name)
}

func (x *demoService) GetDemo2() string {
	return "Hello World from Go Version 3"
}

func (x *demoService) GetDemo3() *gin.H {
	return &gin.H{
		"text": "Welcome to gin lambda server. Version 3",
	}
}

func (x *demoService) ListData(payload *typesDemo.ListDataRequest) *typesSDK.SaveRepositoryResponse {
	res := x.DemoRepository.ListData(payload)
	return res
}

func (x *demoService) ShowData(id string) *typesSDK.SaveRepositoryResponse {
	res := x.DemoRepository.ShowData(id)
	return res
}

func (x *demoService) CreateData(payload *typesDemo.TableDemo) *typesSDK.SaveRepositoryResponse {
	res := x.DemoRepository.CreateData(payload)
	return res
}

func (x *demoService) UpdateData(id string, payload *typesDemo.TableDemo) *typesSDK.SaveRepositoryResponse {
	res := x.DemoRepository.UpdateData(id, payload)
	return res
}

func (x *demoService) DeleteData(id string) *typesSDK.SaveRepositoryResponse {
	res := x.DemoRepository.DeleteData(id)
	return res
}
