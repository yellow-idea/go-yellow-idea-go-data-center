package service

import (
	"github.com/gin-gonic/gin"
	typesDemo "gitlab.yellow-idea.com/yellow-idea-go-data-center/types/demo"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
)

type DemoService interface {
	GetDemo1(name string) string
	GetDemo2() string
	GetDemo3() *gin.H
	ListData(payload *typesDemo.ListDataRequest) *typesSDK.SaveRepositoryResponse
	ShowData(id string) *typesSDK.SaveRepositoryResponse
	CreateData(payload *typesDemo.TableDemo) *typesSDK.SaveRepositoryResponse
	UpdateData(id string, payload *typesDemo.TableDemo) *typesSDK.SaveRepositoryResponse
	DeleteData(id string) *typesSDK.SaveRepositoryResponse
}