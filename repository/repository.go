package repository

import (
	typesDemo "gitlab.yellow-idea.com/yellow-idea-go-data-center/types/demo"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
)

type DemoRepository interface {
	ListData(payload *typesDemo.ListDataRequest) *typesSDK.SaveRepositoryResponse
	ShowData(id string) *typesSDK.SaveRepositoryResponse
	CreateData(payload *typesDemo.TableDemo) *typesSDK.SaveRepositoryResponse
	UpdateData(id string, payload *typesDemo.TableDemo) *typesSDK.SaveRepositoryResponse
	DeleteData(id string) *typesSDK.SaveRepositoryResponse
}
