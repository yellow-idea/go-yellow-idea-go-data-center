package demo

import (
	"fmt"
	typesDemo "gitlab.yellow-idea.com/yellow-idea-go-data-center/types/demo"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	"testing"
)

func TestDemoRepository_CreateData(t *testing.T) {
	payload := &typesDemo.TableDemo{
		Name:     "Koko",
		LastName: "Manmono",
		Age:      12,
		City:     "Bangkok",
	}

	a := NewDemoRepository()
	res := a.CreateData(payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}

func TestDemoRepository_ListData(t *testing.T) {
	payload := &typesDemo.ListDataRequest{
		Sort:   "",
		Order:  "",
		Offset: 0,
		Limit:  10,
		//IsExport: False,
		Filter: &typesDemo.ListFilterDataRequest{
			Name: "",
		},
	}

	a := NewDemoRepository()
	res := a.ListData(payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(helperSDK.InterfaceToJson(&res.Rows))
}

func TestDemoRepository_ShowData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewDemoRepository()
	res := a.ShowData(id)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(helperSDK.InterfaceToJson(&res.Data))
}

func TestDemoRepository_UpdateData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewDemoRepository()
	payload := &typesDemo.TableDemo{
		Name:     "Koko-v2",
		LastName: "Manmono-v2",
		Age:      12,
		City:     "Bangkok-v2",
	}
	res := a.UpdateData(id, payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}

func TestDemoRepository_DeleteData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewDemoRepository()
	res := a.DeleteData(id)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}
