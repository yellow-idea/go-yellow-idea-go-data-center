package demo

import (
	"context"
	"fmt"
	constGlobal "gitlab.yellow-idea.com/yellow-idea-go-data-center/constants"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/helper"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/repository"
	typesDemo "gitlab.yellow-idea.com/yellow-idea-go-data-center/types/demo"
	driverMongoSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/drivers/mongodb"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type demoRepository struct {
	MongoDetail    *typesSDK.SecretMongoDetail
	MongoClient    driverMongoSDK.MongoClientType
	DBName         string
	CollectionName string
}

func NewDemoRepository() repository.DemoRepository {
	return &demoRepository{
		MongoDetail:    helper.SecretGetMongoDetail(constGlobal.DemoMongoServerType),
		MongoClient:    driverMongoSDK.MongoClient,
		DBName:         constGlobal.DemoMongoDatabaseName,
		CollectionName: "demo",
	}
}

func (d *demoRepository) ListData(payload *typesDemo.ListDataRequest) *typesSDK.SaveRepositoryResponse {
	resultsEmpty := make([]*typesDemo.TableDemo, 0)

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	findOptions := options.Find()
	findOptions.SetSort(bson.D{{"updated_at", -1}})
	findOptions.SetSkip(payload.Offset)
	findOptions.SetLimit(payload.Limit)

	var results []*typesDemo.TableDemo

	filter := bson.D{}
	if payload.Filter.Name != "" {
		filter = append(filter, bson.E{
			Key:   "name",
			Value: bson.D{{"$regex", fmt.Sprintf(".*%s.*", payload.Filter.Name)}},
		})
	}
	cur, err := collection.Find(context.TODO(), filter, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem typesDemo.TableDemo
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Data Not Found",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	_ = cur.Close(context.TODO())

	// Clear Data
	defer func() { results = nil }()

	// Output
	if results == nil {
		results = resultsEmpty
	}
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Rows:       results,
		Total:      0,
	}
}

func (d *demoRepository) ShowData(id string) *typesSDK.SaveRepositoryResponse {
	// Valid & Convert ID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
			Data:       nil,
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Data:       nil,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	var result *typesDemo.TableDemo
	filter := &bson.D{{"_id", _id}}
	err = collection.FindOne(context.TODO(), filter).Decode(&result)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Data:       nil,
		}
	}

	// Clear Data
	defer func() { result = nil }()

	// Output
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Data:       result,
	}
}

func (d *demoRepository) CreateData(payload *typesDemo.TableDemo) *typesSDK.SaveRepositoryResponse {
	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	s := &bson.D{
		{"_id", primitive.NewObjectID()},
		{"name", payload.Name},
		{"last_name", payload.LastName},
		{"age", payload.Age},
		{"city", payload.City},
		{"created_at", payload.CreatedAt},
		{"updated_at", payload.CreatedAt},
	}
	res, err := collection.InsertOne(context.TODO(), s)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Create Error",
		}
	}

	// Output
	return &typesSDK.SaveRepositoryResponse{
		ID:         res.InsertedID,
		CodeReturn: 1,
		Message:    "Create Success",
	}
}

func (d *demoRepository) UpdateData(id string, payload *typesDemo.TableDemo) *typesSDK.SaveRepositoryResponse {
	// Valid & Convert ID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	filter := &bson.D{{"_id", _id}}
	s := &bson.D{
		{"name", payload.Name},
		{"last_name", payload.LastName},
		{"age", payload.Age},
		{"city", payload.City},
		{"updated_at", helperSDK.GetTimeNowGMT()},
	}
	update := &bson.D{{"$set", s}}
	res, err := collection.UpdateOne(context.TODO(), filter, update)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			ID:         _id,
			CodeReturn: -1,
			Message:    "Update Error",
		}
	}

	// Output Error
	if res.ModifiedCount <= 0 {
		return &typesSDK.SaveRepositoryResponse{
			ID:         _id,
			CodeReturn: 2,
			Message:    "Data Not Found",
		}
	}

	// Output
	return &typesSDK.SaveRepositoryResponse{
		ID:         _id,
		CodeReturn: 1,
		Message:    "Update Success",
	}
}

func (d *demoRepository) DeleteData(id string) *typesSDK.SaveRepositoryResponse {
	// Valid & Convert ID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	filter := bson.D{{"_id", _id}}
	res, err := collection.DeleteOne(context.TODO(), filter)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			ID:         _id,
			CodeReturn: -1,
			Message:    "Delete Error",
		}
	}

	// Output Error
	if res.DeletedCount <= 0 {
		return &typesSDK.SaveRepositoryResponse{
			ID:         _id,
			CodeReturn: 2,
			Message:    "Data Not Found",
		}
	}

	// Output Error
	return &typesSDK.SaveRepositoryResponse{
		ID:         _id,
		CodeReturn: 1,
		Message:    "Delete Success",
	}
}
