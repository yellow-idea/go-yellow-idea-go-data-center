package types

import "time"

type FilterReportNewDashboardBill struct {
	Sort   string `json:"sort" bson:"sort"`
	Order  string `json:"order" bson:"order"`
	Limit  int    `json:"limit" bson:"limit"`
	Offset int    `json:"offset" bson:"offset"`
	Filter struct {
		BillNoAdmin string    `json:"bill_no_admin" bson:"bill_no_admin"`
		PhoneNumber string    `json:"phone_number" bson:"phone_number"`
		BillNo      string    `json:"bill_no" bson:"bill_no"`
		ShopType    string    `json:"shop_type" bson:"shop_type"`
		Status      string    `json:"status" bson:"status"`
		StartDate   time.Time `json:"start_date" bson:"start_date"`
		EndDate     time.Time `json:"end_date" bson:"end_date"`
	} `json:"filter" bson:"filter"`
}
