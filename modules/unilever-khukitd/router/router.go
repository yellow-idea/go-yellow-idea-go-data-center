package router

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/unilever-khukitd/service"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/unilever-khukitd/types"
	httpSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/http"
	"net/http"
)

func RouteCms(r *gin.Engine) {
	// Report
	rep := r.Group("/cms/unilever/mt-khukitd/report")
	{
		sr := service.NewReportService()
		rep.POST("/bill/new-xls-dashboard-bill", func(c *gin.Context) {
			var payload *types.FilterReportNewDashboardBill
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := sr.ReportNewDashboardBill(payload)
			c.JSON(http.StatusOK, &gin.H{
				"data": res.Data,
			})
		})
		rep.POST("/bill/new-xls-dashboard-summary", func(c *gin.Context) {
			//var payload *types.RequestGetSurveyReport
			//defer func() { payload = nil }()
			//err := c.BindJSON(&payload)
			//if err != nil {
			//	fmt.Println(err)
			//	httpSDK.ResponseBadRequest(c)
			//	return
			//}
			res := sr.ReportNewDashboardSummary()
			c.JSON(http.StatusOK, &gin.H{
				"data": res.Data,
			})
		})
	}
}
