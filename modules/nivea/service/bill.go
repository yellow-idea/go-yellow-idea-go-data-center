package service

import (
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/nivea/constants"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/nivea/repository"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/nivea/types"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type BillService interface {
	ListData(payload *types.FilterBillList) *typesSDK.SaveRepositoryResponse
	ShowData(id string) *typesSDK.SaveRepositoryResponse
	CreateData(payload *types.TableFactBill) *typesSDK.SaveRepositoryResponse
	UpdateData(id string, payload *types.TableFactBill) *typesSDK.SaveRepositoryResponse
	DeleteData(id string) *typesSDK.SaveRepositoryResponse
	ReportUploadBill(payload *types.FilterReportBillUploadRequest) *typesSDK.SaveRepositoryResponse
	ReportUploadBillSuccess(payload *types.FilterReportBillUploadSuccessRequest) *typesSDK.SaveRepositoryResponse
	ReportUploadBillSuccessV2(payload *types.FilterReportBillUploadSuccessRequest) *typesSDK.SaveRepositoryResponse
	ReportBillCustomer(payload *types.FilterReportRegisterRequest) *typesSDK.SaveRepositoryResponse                 // TODO:
	ReportUploadBillComplaint(payload *types.FilterReportBillUploadSuccessRequest) *typesSDK.SaveRepositoryResponse // TODO:
	ReportUploadBillDetail(id string) *typesSDK.SaveRepositoryResponse
	ReportUploadBillDetailSave(id string, payload *types.TableFactBill) *typesSDK.SaveRepositoryResponse
	ReportUploadBillAll(payload *types.FilterReportBillUploadRequest) *typesSDK.SaveRepositoryResponse
	ReportPointByUser(payload *types.FilterReportRegisterRequest) *typesSDK.SaveRepositoryResponse
	BillNoAdminCheckDuplicate(billNoAdmin string) *typesSDK.SaveRepositoryResponse
	// App
	Upload(payload *types.RequestBillUpload) *typesSDK.SaveRepositoryResponse
	GetHistory(payload *types.RequestBillHistory) *typesSDK.SaveRepositoryResponse
	Complaint(payload *types.TableFactBill) *typesSDK.SaveRepositoryResponse
}

type billService struct {
	BillRepository repository.BillRepository
	LogRepository  repository.LogRepository
	//LineUserRepository repository.LineUserRepository
	RegisterRepository repository.RegisterRepository
}

func NewBillService() BillService {
	return &billService{
		BillRepository: repository.NewBillRepository(),
		LogRepository:  repository.NewLogRepository(),
		//LineUserRepository: repoNivea.NewLineUserRepository(),
		RegisterRepository: repository.NewRegisterRepository(),
	}
}

func (x *billService) ListData(payload *types.FilterBillList) *typesSDK.SaveRepositoryResponse {
	return x.BillRepository.ListData(payload)
}

func (x *billService) ShowData(id string) *typesSDK.SaveRepositoryResponse {
	return x.BillRepository.ShowData(id)
}

func (x *billService) CreateData(payload *types.TableFactBill) *typesSDK.SaveRepositoryResponse {
	payload.ImgUrl = helperSDK.S3Base64ToCDN(payload.ImgUrl)
	return x.BillRepository.CreateData(payload)
}

func (x *billService) UpdateData(id string, payload *types.TableFactBill) *typesSDK.SaveRepositoryResponse {
	payload.ImgUrl = helperSDK.S3Base64ToCDN(payload.ImgUrl)
	return x.BillRepository.UpdateData(id, payload)
}

func (x *billService) DeleteData(id string) *typesSDK.SaveRepositoryResponse {
	return x.BillRepository.DeleteData(id)
}

func (x *billService) Upload(payload *types.RequestBillUpload) *typesSDK.SaveRepositoryResponse {
	// TODO: Valid
	// TODO: Complaint Flow
	userId, _ := primitive.ObjectIDFromHex(payload.UserID)
	//usrLineData := x.LineUserRepository.ShowData(payload.mid)
	payload.ImgURL = helperSDK.S3Base64ToCDN(payload.ImgURL)
	res := x.BillRepository.CreateData(&types.TableFactBill{
		UserID: userId,
		//Mid:         "", // TODO: Find
		BillNo:      payload.BillNo,
		ImgUrl:      payload.ImgURL,
		IsComplaint: payload.IsComplaint,
		ShopType:    payload.ShopType,
		ShopType2:   payload.ShopType2,
		BillProduct: make([]interface{}, 0),
	})
	return res
}

func (x *billService) ReportUploadBill(payload *types.FilterReportBillUploadRequest) *typesSDK.SaveRepositoryResponse {
	res := x.BillRepository.ReportUploadBill(payload)
	// Export Excel
	if payload.IsExport {
		fileName := helperSDK.GetTimeNowGMT().Format("200601021504") + "-ReportUploadBill.xlsx"
		x.GenerateExcelUploadBill(res, fileName, 1)
		// Upload
		res.Data = helperSDK.S3FileToCDN("/tmp/"+fileName, fmt.Sprintf("%s/%s", constants.UploadFolder, fileName))
	}
	return res
}

func (x *billService) ReportPointByUser(payload *types.FilterReportRegisterRequest) *typesSDK.SaveRepositoryResponse {
	res := x.BillRepository.ReportBillCustomer(payload)

	// Export Excel
	if payload.IsExport {
		f := excelize.NewFile()
		sheet1 := "Sheet1"
		fileName := helperSDK.GetTimeNowGMT().Format("200601021504") + "-ReportBillCustomer.xlsx"
		rowsTmp := res.Rows.([]*types.ResponseReportBillCustomer)
		var rows = make([]*types.ResponseReportBillCustomer, 0)
		for _, v := range rowsTmp {
			for i := 0; i < v.TotalPoint; i++ {
				rows = append(rows, v)
			}
		}
		//rows := res.Rows.([]*types.ResponseReportBillCustomer)
		_ = f.SetCellValue(sheet1, "A1", "userID")
		_ = f.SetCellValue(sheet1, "B1", "Display Name")
		_ = f.SetCellValue(sheet1, "C1", "Name")
		_ = f.SetCellValue(sheet1, "D1", "Last Name")
		_ = f.SetCellValue(sheet1, "E1", "Phone Number")
		_ = f.SetCellValue(sheet1, "F1", "Address")
		_ = f.SetCellValue(sheet1, "G1", "Province")
		_ = f.SetCellValue(sheet1, "H1", "Postcode")
		for i, v := range rows {
			_ = f.SetCellValue(sheet1, fmt.Sprintf("A%d", i+2), v.ID.LineUserID)
			_ = f.SetCellValue(sheet1, fmt.Sprintf("B%d", i+2), v.ID.LineDisplayName)
			_ = f.SetCellValue(sheet1, fmt.Sprintf("C%d", i+2), v.ID.FirstName)
			_ = f.SetCellValue(sheet1, fmt.Sprintf("D%d", i+2), v.ID.LastName)
			_ = f.SetCellValue(sheet1, fmt.Sprintf("E%d", i+2), v.ID.PhoneNumber)
			_ = f.SetCellValue(sheet1, fmt.Sprintf("F%d", i+2), v.ID.Address)
			_ = f.SetCellValue(sheet1, fmt.Sprintf("G%d", i+2), v.ID.ProvinceID)
			_ = f.SetCellValue(sheet1, fmt.Sprintf("H%d", i+2), v.ID.ZipCode)
		}
		if err := f.SaveAs("/tmp/" + fileName); err != nil {
			fmt.Println(err)
		}
		// Upload
		res.Data = helperSDK.S3FileToCDN("/tmp/"+fileName, fmt.Sprintf("%s/%s", constants.UploadFolder, fileName))
	}
	return res
}

func (x *billService) ReportUploadBillSuccess(payload *types.FilterReportBillUploadSuccessRequest) *typesSDK.SaveRepositoryResponse {
	res := x.BillRepository.ReportUploadBillSuccess(payload)
	// Export Excel
	if payload.IsExport {
		fileName := helperSDK.GetTimeNowGMT().Format("200601021504") + "-ReportUploadBillSuccess.xlsx"
		x.GenerateExcelUploadBill(res, fileName, 2)
		// Upload
		res.Data = helperSDK.S3FileToCDN("/tmp/"+fileName, fmt.Sprintf("%s/%s", constants.UploadFolder, fileName))
	}
	return res
}

func (x *billService) ReportUploadBillSuccessV2(payload *types.FilterReportBillUploadSuccessRequest) *typesSDK.SaveRepositoryResponse {
	res := x.BillRepository.ReportUploadBillSuccessV2(payload)
	// Export Excel
	if payload.IsExport {
		fileName := helperSDK.GetTimeNowGMT().Format("200601021504") + "-ReportUploadBillSuccessV2.xlsx"
		x.GenerateExcelUploadBill(res, fileName, 2)
		// Upload
		res.Data = helperSDK.S3FileToCDN("/tmp/"+fileName, fmt.Sprintf("%s/%s", constants.UploadFolder, fileName))
	}
	return res
}

func (x *billService) ReportUploadBillAll(payload *types.FilterReportBillUploadRequest) *typesSDK.SaveRepositoryResponse {
	return x.BillRepository.ReportUploadBillAll(payload)
}

func (x *billService) ReportBillCustomer(payload *types.FilterReportRegisterRequest) *typesSDK.SaveRepositoryResponse {
	res := x.BillRepository.ReportBillCustomer(payload)
	// Export Excel
	if payload.IsExport {
		f := excelize.NewFile()
		sheet1 := "Sheet1"
		fileName := helperSDK.GetTimeNowGMT().Format("200601021504") + "-ReportBillCustomer.xlsx"
		rows := res.Rows.([]*types.ResponseReportBillCustomer)
		_ = f.SetCellValue(sheet1, "A1", "userID")
		_ = f.SetCellValue(sheet1, "B1", "Display Name")
		_ = f.SetCellValue(sheet1, "C1", "Name")
		_ = f.SetCellValue(sheet1, "D1", "Last Name")
		_ = f.SetCellValue(sheet1, "E1", "Phone Number")
		_ = f.SetCellValue(sheet1, "F1", "Gender")
		_ = f.SetCellValue(sheet1, "G1", "Birth Day")
		_ = f.SetCellValue(sheet1, "H1", "Email")
		_ = f.SetCellValue(sheet1, "I1", "Address")
		_ = f.SetCellValue(sheet1, "J1", "Province")
		_ = f.SetCellValue(sheet1, "K1", "Postcode")
		_ = f.SetCellValue(sheet1, "L1", "Total Point")
		_ = f.SetCellValue(sheet1, "M1", "Current Point")
		_ = f.SetCellValue(sheet1, "N1", "Bill Summary")
		_ = f.SetCellValue(sheet1, "O1", "Total Bill")
		_ = f.SetCellValue(sheet1, "P1", "Total Nivea Bill Summary")
		for i, v := range rows {
			_ = f.SetCellValue(sheet1, fmt.Sprintf("A%d", i+2), v.ID.LineUserID)
			_ = f.SetCellValue(sheet1, fmt.Sprintf("B%d", i+2), v.ID.LineDisplayName)
			_ = f.SetCellValue(sheet1, fmt.Sprintf("C%d", i+2), v.ID.FirstName)
			_ = f.SetCellValue(sheet1, fmt.Sprintf("D%d", i+2), v.ID.LastName)
			_ = f.SetCellValue(sheet1, fmt.Sprintf("E%d", i+2), v.ID.PhoneNumber)
			_ = f.SetCellValue(sheet1, fmt.Sprintf("F%d", i+2), v.ID.Sex)
			_ = f.SetCellValue(sheet1, fmt.Sprintf("G%d", i+2), v.ID.Birthday)
			_ = f.SetCellValue(sheet1, fmt.Sprintf("H%d", i+2), v.ID.Email)
			_ = f.SetCellValue(sheet1, fmt.Sprintf("I%d", i+2), v.ID.Address)
			_ = f.SetCellValue(sheet1, fmt.Sprintf("J%d", i+2), v.ID.ProvinceID)
			_ = f.SetCellValue(sheet1, fmt.Sprintf("K%d", i+2), v.ID.ZipCode)
			_ = f.SetCellValue(sheet1, fmt.Sprintf("L%d", i+2), v.TotalPoint)
			_ = f.SetCellValue(sheet1, fmt.Sprintf("M%d", i+2), v.CurrentPoint)
			_ = f.SetCellValue(sheet1, fmt.Sprintf("N%d", i+2), v.BillSummary)
			_ = f.SetCellValue(sheet1, fmt.Sprintf("O%d", i+2), v.TotalBill)
			_ = f.SetCellValue(sheet1, fmt.Sprintf("P%d", i+2), v.TotalNiveaBillSummary)
		}
		if err := f.SaveAs("/tmp/" + fileName); err != nil {
			fmt.Println(err)
		}
		// Upload
		res.Data = helperSDK.S3FileToCDN("/tmp/"+fileName, fmt.Sprintf("%s/%s", constants.UploadFolder, fileName))
	}
	return res
}

func (x *billService) ReportUploadBillComplaint(payload *types.FilterReportBillUploadSuccessRequest) *typesSDK.SaveRepositoryResponse {
	res := x.BillRepository.ReportUploadBillComplaint(payload)
	// Export Excel
	if payload.IsExport {
		fileName := helperSDK.GetTimeNowGMT().Format("200601021504") + "-ReportUploadBillSuccess.xlsx"
		x.GenerateExcelUploadBill(res, fileName, 2)
		// Upload
		res.Data = helperSDK.S3FileToCDN("/tmp/"+fileName, fmt.Sprintf("%s/%s", constants.UploadFolder, fileName))
	}
	return res
}

func (x *billService) GenerateExcelUploadBill(res *typesSDK.SaveRepositoryResponse, fileName string, t int8) {
	f := excelize.NewFile()
	sheet1 := "Sheet1"
	rows := res.Rows.([]*types.TableFactBill)
	// Header
	_ = f.SetCellValue(sheet1, "A1", "userID")
	_ = f.SetCellValue(sheet1, "B1", "Bill_Image")
	_ = f.SetCellValue(sheet1, "C1", "ยอดขายโก๋แก่ (บาท)")
	_ = f.SetCellValue(sheet1, "D1", "ยอดรวมใบเสร็จ (บาท)")
	_ = f.SetCellValue(sheet1, "E1", "Shop by Admin")
	_ = f.SetCellValue(sheet1, "F1", "วันที่ใบเสร็จโดย Admin (Bill Date By Admin)")
	_ = f.SetCellValue(sheet1, "G1", "วันที่ตรวจใบเสร็จโดยระบบ (Update Date)")
	_ = f.SetCellValue(sheet1, "AT1", "วันที่ลูกค้าส่งใบเสร็จ (Create Date)")
	_ = f.SetCellValue(sheet1, "H1", "Bill_No. by Admin")
	_ = f.SetCellValue(sheet1, "I1", "Status")
	_ = f.SetCellValue(sheet1, "J1", "Duplicate")
	_ = f.SetCellValue(sheet1, "K1", "โก๋แก่ลันเตาซอสศรีราชา 10 บาท")
	_ = f.SetCellValue(sheet1, "L1", "จำนวน/หน่วย")
	_ = f.SetCellValue(sheet1, "M1", "ราคา")
	_ = f.SetCellValue(sheet1, "N1", "โก๋แก่ลันเตาซอสศรีราชา 20 บาท")
	_ = f.SetCellValue(sheet1, "O1", "จำนวน/หน่วย")
	_ = f.SetCellValue(sheet1, "P1", "ราคา")
	_ = f.SetCellValue(sheet1, "Q1", "โก๋แก่ลันเตาปลาหมึก 10 บาท")
	_ = f.SetCellValue(sheet1, "R1", "จำนวน/หน่วย")
	_ = f.SetCellValue(sheet1, "S1", "ราคา")
	_ = f.SetCellValue(sheet1, "T1", "โก๋แก่ลันเตาปลาหมึก 20 บาท")
	_ = f.SetCellValue(sheet1, "U1", "จำนวน/หน่วย")
	_ = f.SetCellValue(sheet1, "V1", "ราคา")
	_ = f.SetCellValue(sheet1, "W1", "โก๋แก่ลันเตาอบเกลือ 10 บาท")
	_ = f.SetCellValue(sheet1, "X1", "จำนวน/หน่วย")
	_ = f.SetCellValue(sheet1, "Y1", "ราคา")
	_ = f.SetCellValue(sheet1, "Z1", "โก๋แก่ลันเตาอบเกลือ 20 บาท")
	_ = f.SetCellValue(sheet1, "AA1", "จำนวน/หน่วย")
	_ = f.SetCellValue(sheet1, "AB1", "ราคา")
	_ = f.SetCellValue(sheet1, "AC1", "โก๋แก่ลันเตาวาซาบิ 10 บาท")
	_ = f.SetCellValue(sheet1, "AD1", "จำนวน/หน่วย")
	_ = f.SetCellValue(sheet1, "AE1", "ราคา")
	_ = f.SetCellValue(sheet1, "AF1", "โก๋แก่ลันเตาวาซาบิ 20 บาท")
	_ = f.SetCellValue(sheet1, "AG1", "จำนวน/หน่วย")
	_ = f.SetCellValue(sheet1, "AH1", "ราคา")
	_ = f.SetCellValue(sheet1, "AI1", "กลุ่มสินค้าในใบเสร็จ")
	_ = f.SetCellValue(sheet1, "AJ1", "Display Name")
	_ = f.SetCellValue(sheet1, "AK1", "Name")
	_ = f.SetCellValue(sheet1, "AL1", "Last_Name")
	_ = f.SetCellValue(sheet1, "AM1", "Phone_Number")
	_ = f.SetCellValue(sheet1, "AN1", "Gender")
	_ = f.SetCellValue(sheet1, "AO1", "Birth_Day")
	_ = f.SetCellValue(sheet1, "AP1", "Email")
	_ = f.SetCellValue(sheet1, "AQ1", "Address")
	_ = f.SetCellValue(sheet1, "AR1", "Province")
	_ = f.SetCellValue(sheet1, "AS1", "Postcode")
	// Body
	for i, v := range rows {
		_ = f.SetCellValue(sheet1, fmt.Sprintf("A%d", i+2), v.LineUserID)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("B%d", i+2), v.ImgUrl)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("C%d", i+2), v.NiveaBillSummary)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("D%d", i+2), v.BillSummary)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("E%d", i+2), v.ShopByAdmin)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("F%d", i+2), v.BillDateByAdmin)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("G%d", i+2), v.UpdatedAt)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("AT%d", i+2), v.CreatedAt)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("H%d", i+2), v.BillNoAdmin)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("I%d", i+2), v.StatusTitle)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("J%d", i+2), v.IsDuplicate)

		if v.Regular1Amount != 0 && v.Regular1Price != 0 {
			_ = f.SetCellValue(sheet1, fmt.Sprintf("K%d", i+2), "1")
		} else {
			_ = f.SetCellValue(sheet1, fmt.Sprintf("K%d", i+2), "")
		}
		_ = f.SetCellValue(sheet1, fmt.Sprintf("L%d", i+2), v.Regular1Amount)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("M%d", i+2), v.Regular1Price)

		if v.Regular2Amount != 0 && v.Regular2Price != 0 {
			_ = f.SetCellValue(sheet1, fmt.Sprintf("N%d", i+2), "1")
		} else {
			_ = f.SetCellValue(sheet1, fmt.Sprintf("N%d", i+2), "")
		}
		_ = f.SetCellValue(sheet1, fmt.Sprintf("O%d", i+2), v.Regular2Amount)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("P%d", i+2), v.Regular2Price)

		if v.Df1Amount != 0 && v.Df1Price != 0 {
			_ = f.SetCellValue(sheet1, fmt.Sprintf("Q%d", i+2), "1")
		} else {
			_ = f.SetCellValue(sheet1, fmt.Sprintf("Q%d", i+2), "")
		}
		_ = f.SetCellValue(sheet1, fmt.Sprintf("R%d", i+2), v.Df1Amount)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("S%d", i+2), v.Df1Price)

		if v.Df2Amount != 0 && v.Df2Price != 0 {
			_ = f.SetCellValue(sheet1, fmt.Sprintf("T%d", i+2), "1")
		} else {
			_ = f.SetCellValue(sheet1, fmt.Sprintf("T%d", i+2), "")
		}
		_ = f.SetCellValue(sheet1, fmt.Sprintf("U%d", i+2), v.Df2Amount)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("V%d", i+2), v.Df2Price)

		if v.Bio1Amount != 0 && v.Bio1Price != 0 {
			_ = f.SetCellValue(sheet1, fmt.Sprintf("W%d", i+2), "1")
		} else {
			_ = f.SetCellValue(sheet1, fmt.Sprintf("W%d", i+2), "")
		}
		_ = f.SetCellValue(sheet1, fmt.Sprintf("X%d", i+2), v.Bio1Amount)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("Y%d", i+2), v.Bio1Price)

		if v.Bio2Amount != 0 && v.Bio2Price != 0 {
			_ = f.SetCellValue(sheet1, fmt.Sprintf("Z%d", i+2), "1")
		} else {
			_ = f.SetCellValue(sheet1, fmt.Sprintf("Z%d", i+2), "")
		}
		_ = f.SetCellValue(sheet1, fmt.Sprintf("AA%d", i+2), v.Bio2Amount)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("AB%d", i+2), v.Bio2Price)

		if v.Greek1Amount != 0 && v.Greek1Price != 0 {
			_ = f.SetCellValue(sheet1, fmt.Sprintf("AC%d", i+2), "1")
		} else {
			_ = f.SetCellValue(sheet1, fmt.Sprintf("AC%d", i+2), "")
		}
		_ = f.SetCellValue(sheet1, fmt.Sprintf("AD%d", i+2), v.Greek1Amount)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("AE%d", i+2), v.Greek1Price)

		if v.Greek2Amount != 0 && v.Greek2Price != 0 {
			_ = f.SetCellValue(sheet1, fmt.Sprintf("AF%d", i+2), "1")
		} else {
			_ = f.SetCellValue(sheet1, fmt.Sprintf("AF%d", i+2), "")
		}
		_ = f.SetCellValue(sheet1, fmt.Sprintf("AG%d", i+2), v.Greek2Amount)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("AH%d", i+2), v.Greek2Price)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("AI%d", i+2), v.ProductGroup)

		_ = f.SetCellValue(sheet1, fmt.Sprintf("AJ%d", i+2), v.LineDisplayName)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("AK%d", i+2), v.FirstName)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("AL%d", i+2), v.LastName)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("AM%d", i+2), v.PhoneNumber)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("AN%d", i+2), v.Sex)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("AO%d", i+2), v.Birthday)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("AP%d", i+2), v.Email)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("AQ%d", i+2), v.Address)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("AR%d", i+2), v.Province)
		_ = f.SetCellValue(sheet1, fmt.Sprintf("AS%d", i+2), v.ZipCode)
	}
	if err := f.SaveAs("/tmp/" + fileName); err != nil {
		fmt.Println(err)
	}
}

func (x *billService) ReportUploadBillDetail(id string) *typesSDK.SaveRepositoryResponse {
	return x.BillRepository.ReportUploadBillDetail(id)
}

// TODO: Refactor One Connection
func (x *billService) ReportUploadBillDetailSave(id string, payload *types.TableFactBill) *typesSDK.SaveRepositoryResponse {
	res := x.BillRepository.UpdateData(id, payload)
	if res.CodeReturn == 1 {
		// NOTE: Update Point
		usrID := helperSDK.InterfaceObjectIdToString(payload.UserID)
		_ = x.RegisterRepository.UpdatePointByRegisterID(usrID)
		// NOTE: Send Message
		//s := constants.GetDetailLineChanel()
		//var msg []string
		//msg = append(msg, constants.GetMessageAfterAdminSaveBill(helperSDK.GetTimeFormat1(helperSDK.GetTimeNowGMT()), payload.Point, payload.NiveaBillSummary, payload.StatusID))
		//_ = helperSDK.LinePushMessage(s.ChannelAccessToken, payload.LineUserID, fmt.Sprintf("%s", msg))
		//x.LogRepository.SavePushMessageBill(id, fmt.Sprintf("%s", msg))
	}
	return res
}

func (x *billService) GetHistory(payload *types.RequestBillHistory) *typesSDK.SaveRepositoryResponse {
	return x.BillRepository.GetHistory(payload)
}

func (x *billService) Complaint(payload *types.TableFactBill) *typesSDK.SaveRepositoryResponse {
	var res *typesSDK.SaveRepositoryResponse
	if payload.IsComplaint == 1 {
		payload.ImgUrl = helperSDK.S3Base64ToCDN(payload.ImgUrl)
		res = x.BillRepository.ComplaintAndReNew(payload)
	} else {
		res = x.BillRepository.ComplaintByUser(payload)
	}
	if res.CodeReturn == 1 {
		// NOTE: Update Point
		usrID := helperSDK.InterfaceObjectIdToString(payload.UserID)
		_ = x.RegisterRepository.UpdatePointByRegisterID(usrID)
	}
	return res
}

func (x *billService) BillNoAdminCheckDuplicate(billNoAdmin string) *typesSDK.SaveRepositoryResponse {
	check := x.BillRepository.BillNoAdminCheckDuplicate(billNoAdmin)
	if check {
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Duplicate",
		}
	} else {
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 1,
			Message:    "Pass",
		}
	}
}
