package service

import (
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/nivea/constants"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/nivea/repository"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/nivea/types"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
)

type PremiumService interface {
	ListData(payload *types.FilterPremiumList) *typesSDK.SaveRepositoryResponse
	ShowData(id string) *typesSDK.SaveRepositoryResponse
	CreateData(payload *types.TableDimPremium) *typesSDK.SaveRepositoryResponse
	UpdateData(id string, payload *types.TableDimPremium) *typesSDK.SaveRepositoryResponse
	DeleteData(id string) *typesSDK.SaveRepositoryResponse
	ImportUpdateData(payload []*types.RequestPremiumRedeemImportUpdate) *typesSDK.SaveRepositoryResponse
	ReportRedeemHistory(payload *types.FilterReportPremiumRedeemHistory) *typesSDK.SaveRepositoryResponse // TODO:
	ReportRedeemHistorySave(payload *types.FilterReportPremiumRedeemHistorySave) *typesSDK.SaveRepositoryResponse
	// App
	Redeem(payload *types.RequestPremiumRedeem) *typesSDK.SaveRepositoryResponse
}

type premiumService struct {
	PremiumRepository  repository.PremiumRepository
	RegisterRepository repository.RegisterRepository
}

func NewPremiumService() PremiumService {
	return &premiumService{
		PremiumRepository:  repository.NewPremiumRepository(),
		RegisterRepository: repository.NewRegisterRepository(),
	}
}

func (x *premiumService) ListData(payload *types.FilterPremiumList) *typesSDK.SaveRepositoryResponse {
	return x.PremiumRepository.ListData(payload)
}

func (x *premiumService) ShowData(id string) *typesSDK.SaveRepositoryResponse {
	return x.PremiumRepository.ShowData(id)
}

func (x *premiumService) CreateData(payload *types.TableDimPremium) *typesSDK.SaveRepositoryResponse {
	payload.ImgURL = helperSDK.S3Base64ToCDN(payload.ImgURL)
	for i, v := range payload.Items {
		payload.Items[i].ImgURL = helperSDK.S3Base64ToCDN(v.ImgURL)
	}
	return x.PremiumRepository.CreateData(payload)
}

func (x *premiumService) UpdateData(id string, payload *types.TableDimPremium) *typesSDK.SaveRepositoryResponse {
	payload.ImgURL = helperSDK.S3Base64ToCDN(payload.ImgURL)
	for i, v := range payload.Items {
		payload.Items[i].ImgURL = helperSDK.S3Base64ToCDN(v.ImgURL)
	}
	return x.PremiumRepository.UpdateData(id, payload)
}

func (x *premiumService) DeleteData(id string) *typesSDK.SaveRepositoryResponse {
	return x.PremiumRepository.DeleteData(id)
}

func (x *premiumService) ImportUpdateData(payload []*types.RequestPremiumRedeemImportUpdate) *typesSDK.SaveRepositoryResponse {
	return x.PremiumRepository.ImportUpdateData(payload)
}

// TODO: Merge to 1 Repository
func (x *premiumService) Redeem(payload *types.RequestPremiumRedeem) *typesSDK.SaveRepositoryResponse {
	itID := helperSDK.InterfaceObjectIdToString(payload.ItemID)
	usrID := helperSDK.InterfaceObjectIdToString(payload.UserID)

	data := x.PremiumRepository.GetDataItemByItemID(itID)
	if data.CodeReturn != 1 {
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: data.CodeReturn,
			Message:    data.Message,
		}
	}

	data2 := x.RegisterRepository.GetPointByRegisterID(usrID)

	dpr := data.Data.(*types.TableDimPremiumItems)

	pt := dpr.Point
	//isHave := false
	//for _, v := range dpr.Option {
	//	if v.Size == payload.Size {
	//		pt = v.Point
	//		isHave = true
	//	}
	//}
	//if isHave == false {
	//	pt = dpr.Point
	//}

	if data2.Point-pt < 0 {
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Point not Enough",
		}
	}

	res := x.PremiumRepository.Redeem(payload, dpr)
	if res.CodeReturn == 1 {
		_ = x.RegisterRepository.UpdatePointByRegisterID(usrID)
	}
	return res
}

func (x *premiumService) ReportRedeemHistory(payload *types.FilterReportPremiumRedeemHistory) *typesSDK.SaveRepositoryResponse {
	res := x.PremiumRepository.ReportRedeemHistory(payload)
	// Export Excel
	if payload.IsExport {
		f := excelize.NewFile()
		sheet1 := "Sheet1"
		fileName := helperSDK.GetTimeNowGMT().Format("200601021504") + "-ReportRedeemHistory.xlsx"
		rows := res.Rows.([]*types.TableFactPremiumRedeem)
		// Header
		_ = f.SetCellValue(sheet1, "A1", "ID")
		_ = f.SetCellValue(sheet1, "B1", "userID")
		_ = f.SetCellValue(sheet1, "C1", "Display Name")
		_ = f.SetCellValue(sheet1, "D1", "Name")
		_ = f.SetCellValue(sheet1, "E1", "Last Name")
		_ = f.SetCellValue(sheet1, "F1", "Phone Number")
		_ = f.SetCellValue(sheet1, "G1", "Gender")
		_ = f.SetCellValue(sheet1, "H1", "Birth Day")
		_ = f.SetCellValue(sheet1, "I1", "Email")
		_ = f.SetCellValue(sheet1, "J1", "Address")
		_ = f.SetCellValue(sheet1, "K1", "Province")
		_ = f.SetCellValue(sheet1, "L1", "Postcode")
		_ = f.SetCellValue(sheet1, "M1", "Reward_Item")
		_ = f.SetCellValue(sheet1, "N1", "Reward_Dropdown")
		_ = f.SetCellValue(sheet1, "O1", "Size")
		_ = f.SetCellValue(sheet1, "P1", "Redeem_Date")
		_ = f.SetCellValue(sheet1, "Q1", "Tracking_no.")
		_ = f.SetCellValue(sheet1, "R1", "Status")
		// Body
		for i, v := range rows {
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`A%d`, i+2), helperSDK.InterfaceObjectIdToString(v.ID))
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`B%d`, i+2), v.LineUserID)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`C%d`, i+2), v.LineDisplayName)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`D%d`, i+2), v.FirstName)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`E%d`, i+2), v.LastName)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`F%d`, i+2), v.PhoneNumber)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`G%d`, i+2), v.Sex)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`H%d`, i+2), v.Birthday)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`I%d`, i+2), v.Email)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`J%d`, i+2), v.Address)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`K%d`, i+2), v.ProvinceID)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`L%d`, i+2), v.ZipCode)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`M%d`, i+2), v.ParentName)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`N%d`, i+2), v.Name)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`O%d`, i+2), v.Size)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`P%d`, i+2), v.CreatedAt)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`Q%d`, i+2), v.TrackingNo)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`R%d`, i+2), v.TrackingStatusTitle)
		}
		if err := f.SaveAs("/tmp/" + fileName); err != nil {
			fmt.Println(err)
		}
		// Upload
		res.Data = helperSDK.S3FileToCDN("/tmp/"+fileName, fmt.Sprintf("%s/%s", constants.UploadFolder, fileName))
	}
	return res
}

func (x *premiumService) ReportRedeemHistorySave(payload *types.FilterReportPremiumRedeemHistorySave) *typesSDK.SaveRepositoryResponse {
	return x.PremiumRepository.ReportRedeemHistorySave(payload)
}
