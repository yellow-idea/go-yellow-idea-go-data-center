package service

import (
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/nivea/constants"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/nivea/repository"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/nivea/types"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type RegisterService interface {
	ListData(payload *types.FilterRegisterList) *typesSDK.SaveRepositoryResponse
	ShowData(id string) *typesSDK.SaveRepositoryResponse
	CreateData(payload *types.TableDimRegister) *typesSDK.SaveRepositoryResponse
	UpdateData(id string, payload *types.TableDimRegister) *typesSDK.SaveRepositoryResponse
	DeleteData(id string) *typesSDK.SaveRepositoryResponse
	ReportRegister(payload *types.FilterReportRegisterRequest) *typesSDK.SaveRepositoryResponse // TODO:
	// App
	AppInit() *map[string]interface{}
	Register(payload *types.TableDimRegister) *typesSDK.SaveRepositoryResponse
	ShowDataByMid(mid string) *typesSDK.SaveRepositoryResponse
	GetPoint(registerID string) *map[string]interface{}
	GetPremiumHistory(id string) *map[string]interface{}
	GetAddress(registerID string) *typesSDK.SaveRepositoryResponse
	GetAddressById(id string) *typesSDK.SaveRepositoryResponse
	GetAddressDefaultByRegID(registerID string) *typesSDK.SaveRepositoryResponse
	CreateAddress(registerID string, payload *types.TableDimRegisterAddress) *typesSDK.SaveRepositoryResponse
	UpdateAddress(addressID string, payload *types.TableDimRegisterAddress) *typesSDK.SaveRepositoryResponse
	UpdateAddressToDefault(registerID string, addressID string) *typesSDK.SaveRepositoryResponse
}

type registerService struct {
	RegisterRepository  repository.RegisterRepository
	BillRepository      repository.BillRepository
	PremiumRepository   repository.PremiumRepository
	LuckyDrawRepository repository.LuckyDrawRepository
	SettingRepository   repository.SettingRepository
}

func NewRegisterService() RegisterService {
	return &registerService{
		RegisterRepository:  repository.NewRegisterRepository(),
		BillRepository:      repository.NewBillRepository(),
		PremiumRepository:   repository.NewPremiumRepository(),
		LuckyDrawRepository: repository.NewLuckyDrawRepository(),
		SettingRepository:   repository.NewSettingRepository(),
	}
}

func (x *registerService) ListData(payload *types.FilterRegisterList) *typesSDK.SaveRepositoryResponse {
	return x.RegisterRepository.ListData(payload)
}

func (x *registerService) ShowData(id string) *typesSDK.SaveRepositoryResponse {
	return x.RegisterRepository.ShowData(id)
}

func (x *registerService) CreateData(payload *types.TableDimRegister) *typesSDK.SaveRepositoryResponse {
	return x.RegisterRepository.CreateData(payload)
}

func (x *registerService) UpdateData(id string, payload *types.TableDimRegister) *typesSDK.SaveRepositoryResponse {
	return x.RegisterRepository.UpdateData(id, payload)
}

func (x *registerService) DeleteData(id string) *typesSDK.SaveRepositoryResponse {
	return x.RegisterRepository.DeleteData(id)
}

func (x *registerService) AppInit() *map[string]interface{} {
	t1 := time.Now()
	premiumData := x.PremiumRepository.ListAll()
	luckyDraw := x.LuckyDrawRepository.ListAll()
	settingMain := x.SettingRepository.ShowData()
	var p []*types.TableDimPremiumItems
	if len(premiumData) > 0 {
		for i, v := range premiumData {
			for _, v2 := range v.Items {
				p = append(p, v2)
			}
			premiumData[i].Items = nil
		}
	} else {
		p = make([]*types.TableDimPremiumItems, 0)
	}
	// Filter Lucky Draw Active Only
	var l []*types.TableDimLuckyDraw
	if len(luckyDraw) > 0 {
		for _, v := range luckyDraw {
			if v.IsActive == 1 {
				diff := v.EndDate.Sub(t1).Minutes()
				if (diff - 420) > 0 {
					l = append(l, v)
				}
			}
		}
	} else {
		l = make([]*types.TableDimLuckyDraw, 0)
	}

	if l == nil {
		l = make([]*types.TableDimLuckyDraw, 0)
	}
	return &map[string]interface{}{
		"time":               t1,
		"premium_list":       premiumData,
		"premium_list_items": p,
		"lucky_draw_list":    l,
		"setting_data":       settingMain.Data,
	}
}

func (x *registerService) Register(payload *types.TableDimRegister) *typesSDK.SaveRepositoryResponse {
	// TODO: Valid Dup , Bad Request , Format , Require
	res := x.CreateData(payload)
	if res.CodeReturn == 1 {
		res.Message = "Register Success"
		_id := helperSDK.InterfaceObjectIdToString(res.ID)
		res2 := x.ShowData(_id)
		res.Data = res2.Data
		// TODO: Assign Report
	}
	return res
}

func (x *registerService) ShowDataByMid(mid string) *typesSDK.SaveRepositoryResponse {
	return x.RegisterRepository.ShowDataByMid(mid)
}

func (x *registerService) GetPoint(registerID string) *map[string]interface{} {
	resLucky := x.LuckyDrawRepository.RedeemHistory(registerID)

	// Extract data_lucky_draw_items
	var d []*primitive.M
	a := resLucky.Rows.([]primitive.M)
	for _, v := range a {
		_id := v["_id"].(primitive.M)
		d = append(d, &primitive.M{
			"count":   v["count"],
			"item_id": _id["item_id"],
			"name":    _id["name"],
		})
	}

	if d == nil {
		d = make([]*primitive.M, 0)
	}

	dp := x.RegisterRepository.GetPointByRegisterID(registerID)
	return &map[string]interface{}{
		"data_lucky_draw_items": d,
		"data_point":            dp,
	}
}

func (x *registerService) GetPremiumHistory(id string) *map[string]interface{} {
	res := x.PremiumRepository.RedeemHistory(id)
	return &map[string]interface{}{
		"data": res.Data,
	}
}

func (x *registerService) ReportRegister(payload *types.FilterReportRegisterRequest) *typesSDK.SaveRepositoryResponse {
	res := x.RegisterRepository.ReportRegister(payload)
	// Export Excel
	if payload.IsExport {
		f := excelize.NewFile()
		sheet1 := "Sheet1"
		fileName := helperSDK.GetTimeNowGMT().Format("200601021504") + "-ReportRegister.xlsx"
		rows := res.Rows.([]*types.TableDimRegister)
		// Header
		_ = f.SetCellValue(sheet1, "A1", "UserID")
		_ = f.SetCellValue(sheet1, "B1", "Display Name")
		_ = f.SetCellValue(sheet1, "C1", "Name")
		_ = f.SetCellValue(sheet1, "D1", "LastName")
		_ = f.SetCellValue(sheet1, "E1", "Phone Number")
		_ = f.SetCellValue(sheet1, "F1", "Gender")
		_ = f.SetCellValue(sheet1, "G1", "Birth Day")
		_ = f.SetCellValue(sheet1, "H1", "Email")
		_ = f.SetCellValue(sheet1, "I1", "Address")
		_ = f.SetCellValue(sheet1, "J1", "Province")
		_ = f.SetCellValue(sheet1, "K1", "Postcode")
		_ = f.SetCellValue(sheet1, "L1", "Register Date")
		// Body
		for i, v := range rows {
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`A%d`, i+2), v.LineUserID)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`B%d`, i+2), v.LineDisplayName)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`C%d`, i+2), v.FirstName)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`D%d`, i+2), v.LastName)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`E%d`, i+2), v.PhoneNumber)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`F%d`, i+2), v.Sex)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`G%d`, i+2), v.Birthday)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`H%d`, i+2), v.Email)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`I%d`, i+2), v.Address)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`J%d`, i+2), v.ProvinceID)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`K%d`, i+2), v.ZipCode)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`L%d`, i+2), v.CreatedAt)
		}
		if err := f.SaveAs("/tmp/" + fileName); err != nil {
			fmt.Println(err)
		}
		// Upload
		res.Data = helperSDK.S3FileToCDN("/tmp/"+fileName, fmt.Sprintf("%s/%s", constants.UploadFolder, fileName))
	}
	return res
}

func (x *registerService) GetAddress(registerID string) *typesSDK.SaveRepositoryResponse {
	return x.RegisterRepository.GetAddress(registerID)
}

func (x *registerService) GetAddressDefaultByRegID(registerID string) *typesSDK.SaveRepositoryResponse {
	return x.RegisterRepository.GetAddressDefaultByRegID(registerID)
}

func (x *registerService) GetAddressById(id string) *typesSDK.SaveRepositoryResponse {
	return x.RegisterRepository.GetAddressById(id)
}

func (x *registerService) CreateAddress(registerID string, payload *types.TableDimRegisterAddress) *typesSDK.SaveRepositoryResponse {
	return x.RegisterRepository.CreateAddress(registerID, payload)
}

func (x *registerService) UpdateAddress(addressID string, payload *types.TableDimRegisterAddress) *typesSDK.SaveRepositoryResponse {
	return x.RegisterRepository.UpdateAddress(addressID, payload)
}

func (x *registerService) UpdateAddressToDefault(registerID string, addressID string) *typesSDK.SaveRepositoryResponse {
	return x.RegisterRepository.UpdateAddressToDefault(registerID, addressID)
}
