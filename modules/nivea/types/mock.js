// https://rify2cbto4.execute-api.ap-southeast-1.amazonaws.com/dev/api/v1/loyalty_1901

// [APP] Check Register
// Get /register/2f4aa358-f3f1-11e9-be1a-9a0b8a64a8c7/detail
// ถ้าเคยลงทะเบียน
const a18 = {
    "data": {
        "id": "2607d660-8f70-11ea-b167-92c5b11b06cb",
        "line_user_id": "2f4aa358-f3f1-11e9-be1a-9a0b8a64a8c7",
        "first_name": "Sompon",
        "last_name": "Juicerod",
        "email": "",
        "phone_number": "0392838282",
        "address": "Bangkok",
        "province_id": 10000000,
        "zip_code": 92912,
        "birthday": "YYYY-MM-DD",
        "sex": "",
        "created_at": "2020-05-06 15:04:02",
        "updated_at": "2020-05-06 15:04:02",
    }
}

// ถ้ายังไม่เคยลงทะเบียน
const a181 = {
    "data": null
}

// [APP] ลงทะเบียน
// Ex. [POST] /register

const a19 = {
    "first_name": "First Name",
    "last_name": "Last Name",
    "phone_number": "0393929234",
    "birthday": "2020-05-07",
    "sex": "male",
    "address": "Bangkok",
    "zip_code": "10310",
    "province_id": "25000000",
    "email": "",
    "line_user_id": "2f4aad358-f3f1-11e9-be1a-9a0b8a64a8c7"
}

// [APP] ส่งใบเสร็จ
// Ex. [POST] /bill/upload

const a20 = {
    "bill_no": "999",
    "id": 0,
    "img_url": "",
    "is_complaint": 0,
    "shop_type": "name",
    "shop_type_2": "name",
    "user_id": "2607d660-8f70-11ea-b167-92c5b11b06cb"
}

// [APP] ดึงข้อมูล Point
// Ex. [GET] /user/2607d660-8f70-11ea-b167-92c5b11b06cb

const a21 = {
    "data_lucky_draw_items": [
        {
            "count": 3,
            "item_id": ""
        }
    ],
    "data_point": {
        "point": 0,
        "point_lucky_draw": 0,
        "point_premium": 0
    }
}

// [APP] ดึงข้อมูล Lucky Drawn, Premium
// Ex. [GET] https://s3-ap-southeast-1.amazonaws.com/content.yellow-idea/nivea/loyalty_1901_app_init.json?v=1588854042224

const a22 = {
    "lucky_draw_list": [
        {
            "id": "2d8c3272-c33e-11e9-a678-368bd7561479",
            "name": "ลุ้นโทรศัพท์ Samsung Galaxy Note 10+",
            "sequence": 2,
            "description": "ลุ้นโทรศัพท์ Samsung Galaxy Note 10+ จำนวน 2 รางวัล ",
            "point": 4,
            "img_url": "https://s3-ap-southeast-1.amazonaws.com/nivea-web-application-dev-s3-yellow.nivea.upload/images/lucky-draw/core-x1-4fbe1f98-c74b-11e9-8712-c2ef1e03e906.png",
            "start_date": "2019-08-20 00:00:00",
            "end_date": "2019-11-30 23:59:00",
            "is_active": 1,
            "created_at": "2019-08-20 18:32:23",
            "updated_at": "2019-08-26 00:13:03"
        }
    ],
    "premium_list": [
        {
            "id": "4748ef68-c364-11e9-8b5d-722c9680352f",
            "sequence": 1,
            "name": "Product Name",
            "desc_1": "Desc 1",
            "desc_2": "Desc 2",
            "header_1": "Header 1",
            "header_2": "Header 2",
            "header_3": "Header 3",
            "img_url": "",
            "created_at": "2019-08-20 23:05:07",
            "updated_at": "2019-08-26 00:18:38"
        },
        {
            "id": "56214d42-c363-11e9-8b5d-722c9680352f",
            "sequence": 2,
            "name": "Product Name",
            "desc_1": "Desc 1",
            "desc_2": "Desc 2",
            "header_1": "Header 1",
            "header_2": "Header 2",
            "header_3": "Header 3",
            "img_url": "",
            "created_at": "2019-08-20 22:58:22",
            "updated_at": "2019-08-26 00:18:57"
        }
    ],
    "premium_list_items": [
        {
            "id": "474ec726-c364-11e9-8b5d-722c9680352f",
            "parent_id": "4748ef68-c364-11e9-8b5d-722c9680352f",
            "name": "ดัชชี่แบ็คแพค",
            "description": "ดัชชี่แบ็คแพค",
            "option": [
                {
                    "item_stock": 100,
                    "point": 1000,
                    "size": "M"
                },
                {
                    "item_stock": 100,
                    "point": 1000,
                    "size": "L"
                }
            ],
            "start_date": "2019-08-20 00:00:00",
            "end_date": "2019-12-15 23:59:00",
            "img_url": "https://s3-ap-southeast-1.amazonaws.com/nivea-web-application-dev-s3-yellow.nivea.upload/images/redeem/core-x1-474eca28-c364-11e9-8b5d-722c9680352f.jpg",
            "is_active": 1,
            "created_at": "2019-08-20 23:05:07",
            "updated_at": "2019-08-26 00:18:38"
        },
        {
            "id": "5624f8a2-c363-11e9-8b5d-722c9680352f",
            "parent_id": "56214d42-c363-11e9-8b5d-722c9680352f",
            "name": "ดัชชี่เฮลท์ตี้บ็อกซ์",
            "description": "ดัชชี่เฮลท์ตี้บ็อกซ์",
            "option": [
                {
                    "item_stock": 100,
                    "point": 1000,
                    "size": "M"
                },
                {
                    "item_stock": 100,
                    "point": 1000,
                    "size": "L"
                }
            ],
            "start_date": "2019-08-20 00:00:00",
            "end_date": "2019-12-15 23:59:00",
            "img_url": "https://s3-ap-southeast-1.amazonaws.com/nivea-web-application-dev-s3-yellow.nivea.upload/images/redeem/core-x1-5624fb7c-c363-11e9-8b5d-722c9680352f.jpg",
            "is_active": 1,
            "created_at": "2019-08-20 22:58:22",
            "updated_at": "2019-08-26 00:18:57"
        }
    ]
}

// [APP] ลุ้น Lucky Drawn
// Ex. [POST] /lucky-draw/redeem

const a1 = {
    "user_id": "2607d660-8f70-11ea-b167-92c5b11b06cb",
    "item_id": "2d8c3272-c33e-11e9-a678-368bd7561479"
}

// [APP] แลก Redeem
// Ex. [POST] /premium/redeem

const a2 = {
    "user_id": "2607d660-8f70-11ea-b167-92c5b11b06cb",
    "item_id": "ec93c58a-c362-11e9-8b5d-722c9680352f",
    "size": "M"
}

// [APP] ประวัติแลกของรางวัล
// Ex. [GET] /user/2607d660-8f70-11ea-b167-92c5b11b06cb/premium-history

const a3 = {
    "data": [
        {
            "created_at": "2020-05-06 20:40:03",
            "img_url": "https://s3-ap-southeast-1.amazonaws.com/nivea-web-application-dev-s3-yellow.nivea.upload/images/redeem/core-x1-d9eb3bec-c363-11e9-8b5d-722c9680352f.jpg",
            "name": "หมอนผ้าห่ม (มี 3 ลายให้สะสม)",
            "size": "",
            "tracking_no": "",
            "tracking_status_id": 2
        },
        {
            "created_at": "2020-05-07 20:49:15",
            "img_url": "https://s3-ap-southeast-1.amazonaws.com/nivea-web-application-dev-s3-yellow.nivea.upload/images/redeem/core-x1-ec93c8aa-c362-11e9-8b5d-722c9680352f.png",
            "name": "มินิแอลอีดี (มี 3 ลายให้สะสม)",
            "size": "",
            "tracking_no": "",
            "tracking_status_id": 2
        }
    ]
}

// ==================================================
// [CMS] Redeem [Listing]

// Filter
const a4 = {
    "sort": "",
    "order": "asc",
    "offset": 0,
    "limit": 10,
    "filter": {
        "start_date": "2020-04-29 00:00",
        "end_date": "2020-05-06 23:59",
        "name": ""
    }
}

const a5 = {
    "rows": [
        {
            "id": "5e777df688f2880007e1e1a2",
            "sequence": "1",
            "name": "Starbucks Gift Card 500 บาท",
            "img_url": "https://cdn.yellow-idea.com/images/weber-network/64f6baf0-7a38-11ea-bc62-095e68e4a6ec.jpeg",
            "created_at": "2020-03-22T22:02:14.656Z",
            "updated_at": "2020-03-26T14:52:28.469Z"
        }
    ],
    "total": 181
}

// [CMS] Redeem [FORM CREATE, UPDATE]
const a6 = {
    "id": 0,
    "sequence": "1",
    "name": "Product Name",
    "desc_1": "Desc 1",
    "desc_2": "Desc 2",
    "img_url": "",
    "header_1": "Header 1",
    "header_2": "Header 2",
    "header_3": "Header 3",
    "items": [
        {
            "name": "Item 1",
            "description": "Desc",
            "start_date": "2020-05-07 00:00",
            "end_date": "2020-05-07 23:59",
            "img_url": "",
            "is_active": 1,
            "item_stock": 0,
            "option": [
                {
                    "item_stock": 100,
                    "point": 1000,
                    "size": "M"
                },
                {
                    "item_stock": 100,
                    "point": 1000,
                    "size": "L"
                }
            ]
        }
    ],
    "footer": "<p>Footer</p>"
}

// [CMS] Redeem [DELETE]
// [CMS] Lucky Drawn [Listing]

// Filter
const a7 = {
    "sort": "",
    "order": "asc",
    "offset": 0,
    "limit": 10,
    "filter": {
        "start_date": "2020-04-29 00:00",
        "end_date": "2020-05-06 23:59",
        "name": ""
    }
}

const a8 = {
    "rows": [
        {
            "id": "5e777df688f2880007e1e1a2",
            "name": "Starbucks Gift Card 500 บาท",
            "img_url": "",
            "sequence": "1",
            "start_date": "2020-03-22T00:00:00.000Z",
            "end_date": "2021-04-30T23:59:00.000Z",
            "item_stock": 9999,
            "point": 10000,
            "seq": "1",
            "created_at": "2020-03-22T22:02:14.656Z",
            "updated_at": "2020-03-26T14:52:28.469Z"
        }
    ],
    "total": 2
}

// [CMS] Lucky Drawn [FORM CREATE, UPDATE]

const a9 = {
    "id": 0,
    "name": "Lucky Drawn",
    "description": "Description",
    "point": "4",
    "sequence": "1",
    "img_url": "",
    "end_date": "2020-05-06 23:59",
    "start_date": "2020-05-06 00:00",
    "is_active": 1
}

// [CMS] Lucky Drawn [DELETE]

// [CMS] Report ข้อมูลลงทะเบียน [Listing]

// Filter
const a10 = {
    "sort": "",
    "order": "asc",
    "offset": 0,
    "limit": 10,
    "filter": {
        "start_date": "",
        "end_date": "",
        "phone_number": ""
    }
}

const a11 = {
    "rows": [
        {
            "id": "5eb1392897f89d0007b69bae",
            "line_display_name": "นุ่น",
            "line_display_image": "https://profile.line-scdn.net/0hWVb31DcICHlfSiCHR5t3LmMPBhQoZA4xJyxHTHwaAR4lc057YyhDGypMVx0neU0sYCkVGH1CVU10",
            "first_name": "วิรากร",
            "last_name": "เกษรามัญ",
            "phone_number": "0956142455",
            "email": "nunwiragonk@gmail.com",
            "province": "",
            "post_code": "",
            "address": "",
            "sex": "",
            "birthday": "",
            "created_at": "2020-05-05T17:00:08.931Z"
        }
    ],
    "total": 71
}

// [CMS] Report ลูกค้าที่ร่วมกิจกรรม [Listing] เหมือน Weber
// Filter
const a12 = {
    "sort": "",
    "order": "asc",
    "offset": 0,
    "limit": 10,
    "filter": {
        "start_date": "",
        "end_date": "",
        "phone_number": ""
    }
}

const a13 = {
    "rows": [
        {
            "id": {
                "line_user_id": "U1317879ddc45706eb78e59fd9fa2838b",
                "first_name": "พัชรณัฏฐ์",
                "last_name": "แก่นอินทร์",
                "email": "",
                "phone_number": "0656969092",
                "shop_name": "ฟ้าทวีพร สาขาบ่อผุด",
                "shop_province": "",
                "shop_district": "",
                "shop_type": "ETC",
                "shop_type_other": "",
                "user_type": "employee"
            },
            "total_point": 83190,
            "current_point": 83190,
            "bill_summary": 803222,
            "total_bill": 216,
            "updated_at": "2020-05-06T18:22:45.605Z"
        }
    ],
    "total": 29
}

// [CMS] Report ตรวจสอบใบเสร็จ [Listing]
// Filter
const a14 = {
    "sort": "",
    "order": "asc",
    "offset": 0,
    "limit": 10,
    "filter": {
        "start_date": "",
        "end_date": "",
        "phone_number": "",
        "bill_no": "",
        "shop_type": "-1",
        "shop_type_2": "-1"
    }
}

const a15 = {
    "rows": [
        {
            "id": "5eb29e05487a4c000907ee4f",
            "first_name": "พัชรณัฏฐ์",
            "last_name": "แก่นอินทร์",
            "phone_number": "0656969092",
            "img_url": "https://cdn.yellow-idea.com/images/weber-network/e857b670-8f8b-11ea-939e-4dd52e657349.jpeg",
            "bill_no": "",
            "bill_no_admin": "",
            "status_id": 2,
            "is_duplicate": 0,
            "created_at": "2020-05-06T18:22:45.605Z",
            "line_user_id": "U1317879ddc45706eb78e59fd9fa2838b",
            "shop_type": "",
            "shop_type_2": ""
        }
    ],
    "total": 65
}

// [CMS] Report ตรวจสอบใบเสร็จเรียบร้อย [Listing]

// Filter
const a16 = {
    "sort": "",
    "order": "asc",
    "offset": 0,
    "limit": 10,
    "filter": {
        "start_date": "",
        "end_date": "",
        "phone_number": "",
        "bill_no": "",
        "shop_type": "-1",
        "shop_type_2": "-1",
        "status": "-1"
    }
}

const a17 = {
    "rows": [
        {
            "id": "5eb24c4e7927f60007a84a6b",
            "first_name": "พัชรณัฏฐ์",
            "last_name": "แก่นอินทร์",
            "phone_number": "0656969092",
            "img_url": "https://cdn.yellow-idea.com/images/weber-network/337409a0-8f5b-11ea-bea0-597b5ad3bae7.jpeg",
            "bill_no": "",
            "bill_no_admin": "630408-0019",
            "status_id": 1,
            "point": 259,
            "bill_summary": 2595,
            "is_duplicate": 0,
            "updated_at": "2020-05-06T16:27:39.821Z",
            "line_user_id": "U1317879ddc45706eb78e59fd9fa2838b",
            "shop_type": "",
            "shop_type_2": ""
        }
    ],
    "total": 442
}

const bill = {
    "id": "5eb65d31a83a32000704b432",
    "line_display_image": "https://profile.line-scdn.net/0h7RBon6ICaH1_FkbhNkcXKkNTZhAIOG41B3MnS1MUNkUFdX0qSiMgGVIeMkQHcih_QyMjHAkTM09X",
    "email": "sompon2532@gmail.com",
    "first_name": "Sompon",
    "last_name": "Juicerod",
    "phone_number": "0832934832",
    "address": "Bangkok",
    "province": "กรุงเทพมหานคร",
    "img_url": "https://cdn.yellow-idea.com/images/weber-network/9e340470-91c7-11ea-8123-fb3e1f24e4db.png",
    "bill_no": "",
    "bill_no_admin": "",
    "status_id": 2,
    "point": 0,
    "bill_summary": 0,
    "kohkae_bill_summary": 0,
    "by_admin_no": -1,
    "is_duplicate": 0,
    "bill_product": [],
    "created_at": "2020-05-09T14:35:13.554Z",
    "updated_at": "2020-05-09T14:35:13.554Z",
    "register_id": "5eb65d28a83a32000704b431",
    "line_user_id": "U1657f01d26733e532099337ca6ab4e8d",
    "line_display_name": "ไอต้นรอบโลก",
    "status": 2,
    "shop_type": "name",
    "shop_type2": "name",
    "shop_by_admin": "",
    "bill_date_by_admin": "",
    "regular_1_name": "Regular",
    "regular_1_pack": "ถ้วย",
    "regular_1_amount": "",
    "regular_1_price": "",
    "regular_2_name": "Regular",
    "regular_2_pack": "แพค x4",
    "regular_2_amount": "",
    "regular_2_price": "",
    "df_1_name": "DF0",
    "df_1_pack": "ถ้วย",
    "df_1_amount": "",
    "df_1_price": "",
    "df_2_name": "DF0",
    "df_2_pack": "แพค x4",
    "df_2_amount": "",
    "df_2_price": "",
    "bio_1_name": "Bio",
    "bio_1_pack": "ถ้วย",
    "bio_1_amount": "",
    "bio_1_price": "",
    "bio_2_name": "Bio",
    "bio_2_pack": "แพค x4",
    "bio_2_amount": "",
    "bio_2_price": "",
    "greek_1_name": "Greek",
    "greek_1_pack": "ถ้วย",
    "greek_1_amount": "",
    "greek_1_price": "",
    "greek_2_name": "Greek",
    "greek_2_pack": "แพค x3",
    "greek_2_amount": "",
    "greek_2_price": "",
    "kids_1_name": "Kids",
    "kids_1_pack": "ถ้วย",
    "kids_1_amount": "",
    "kids_1_price": "",
    "kids_2_name": "Kids",
    "kids_2_pack": "แพค x4",
    "kids_2_amount": "",
    "kids_2_price": "",
    "brand_1_name": "ขนาดลิตร",
    "brand_1_pack": "ขนาดลิตร",
    "brand_1_amount": "",
    "brand_1_price": ""
}

// db.fact_bill.find({}).forEach(function (x) {
//     if (x.regular_1_amount === "" {
//         x.regular_1_amount = 0;
//     } else {
//         x.regular_1_amount = parseInt(x.regular_1_amount);
//     }
//     if (x.regular_1_price === "" {
//         x.regular_1_price = 0;
//     } else {
//         x.regular_1_price = parseInt(x.regular_1_price);
//     }
//
//     if (x.regular_2_amount === "" {
//         x.regular_2_amount = 0;
//     } else {
//         x.regular_2_amount = parseInt(x.regular_2_amount);
//     }
//     if (x.regular_2_price === "" {
//         x.regular_2_price = 0;
//     } else {
//         x.regular_2_price = parseInt(x.regular_2_price);
//     }
//
//     if (x.df_1_amount === "" {
//         x.df_1_amount = 0;
//     } else {
//         x.df_1_amount = parseInt(x.df_1_amount);
//     }
//     if (x.df_1_price === "" {
//         x.df_1_price = 0;
//     } else {
//         x.df_1_price = parseInt(x.df_1_price);
//     }
//
//     if (x.df_2_amount === "" {
//         x.df_2_amount = 0;
//     } else {
//         x.df_2_amount = parseInt(x.df_2_amount);
//     }
//     if (x.df_2_price === "" {
//         x.df_2_price = 0;
//     } else {
//         x.df_2_price = parseInt(x.df_2_price);
//     }
//
//     if (x.bio_1_amount === "" {
//         x.bio_1_amount = 0;
//     } else {
//         x.bio_1_amount = parseInt(x.bio_1_amount);
//     }
//     if (x.bio_1_price === "" {
//         x.bio_1_price = 0;
//     } else {
//         x.bio_1_price = parseInt(x.bio_1_price);
//     }
//
//     if (x.bio_2_amount === "" {
//         x.bio_2_amount = 0;
//     } else {
//         x.bio_2_amount = parseInt(x.bio_2_amount);
//     }
//     if (x.bio_2_price === "" {
//         x.bio_2_price = 0;
//     } else {
//         x.bio_2_price = parseInt(x.bio_2_price);
//     }
//
//     if (x.greek_1_amount === "" {
//         x.greek_1_amount = 0;
//     } else {
//         x.greek_1_amount = parseInt(x.greek_1_amount);
//     }
//     if (x.greek_1_price === "" {
//         x.greek_1_price = 0;
//     } else {
//         x.greek_1_price = parseInt(x.greek_1_price);
//     }
//
//     if (x.greek_2_amount === "" {
//         x.greek_2_amount = 0;
//     } else {
//         x.greek_2_amount = parseInt(x.greek_2_amount);
//     }
//     if (x.greek_2_price === "" {
//         x.greek_2_price = 0;
//     } else {
//         x.greek_2_price = parseInt(x.greek_2_price);
//     }
//
//     if (x.kids_1_amount === "" {
//         x.kids_1_amount = 0;
//     } else {
//         x.kids_1_amount = parseInt(x.kids_1_amount);
//     }
//     if (x.kids_1_price === "" {
//         x.kids_1_price = 0;
//     } else {
//         x.kids_1_price = parseInt(x.kids_1_price);
//     }
//
//     if (x.kids_2_amount === "" {
//         x.kids_2_amount = 0;
//     } else {
//         x.kids_2_amount = parseInt(x.kids_2_amount);
//     }
//     if (x.kids_2_price === "" {
//         x.kids_2_price = 0;
//     } else {
//         x.kids_2_price = parseInt(x.kids_2_price);
//     }
//
//     if (x.brand_1_amount === "" {
//         x.brand_1_amount = 0;
//     } else {
//         x.brand_1_amount = parseInt(x.brand_1_amount);
//     }
//     if (x.brand_1_price === "" {
//         x.brand_1_price = 0;
//     } else {
//         x.brand_1_price = parseInt(x.brand_1_price);
//     }
//     db.fact_bill.save(x);
// });