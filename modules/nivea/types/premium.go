package types

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type TableDimPremiumItems struct {
	ID          primitive.ObjectID `json:"id" bson:"_id"`
	ParentID    primitive.ObjectID `json:"parent_id" bson:"parent_id"`
	Name        string             `json:"name" bson:"name"`
	Point       int                `json:"point" bson:"point"`
	Description string             `json:"description" bson:"description"`
	StartDate   time.Time          `json:"start_date" bson:"start_date"`
	EndDate     time.Time          `json:"end_date" bson:"end_date"`
	ImgURL      string             `json:"img_url" bson:"img_url"`
	IsActive    int                `json:"is_active" bson:"is_active"`
	ItemStock   int                `json:"item_stock" bson:"item_stock"`
	Option      []struct {
		ItemStock int    `json:"item_stock" bson:"item_stock"`
		Point     int    `json:"point" bson:"point"`
		Size      string `json:"size" bson:"size"`
	} `json:"option" bson:"option"`
	CreatedAt time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time `json:"updated_at" bson:"updated_at"`
}

type TableDimPremium struct {
	ID        primitive.ObjectID      `json:"id" bson:"_id"`
	Sequence  int                     `json:"sequence" bson:"sequence"`
	Name      string                  `json:"name" bson:"name"`
	Desc1     string                  `json:"desc_1" bson:"desc_1"`
	Desc2     string                  `json:"desc_2" bson:"desc_2"`
	ImgURL    string                  `json:"img_url" bson:"img_url"`
	Header1   string                  `json:"header_1" bson:"header_1"`
	Header2   string                  `json:"header_2" bson:"header_2"`
	Header3   string                  `json:"header_3" bson:"header_3"`
	Items     []*TableDimPremiumItems `json:"items" bson:"items"`
	Footer    string                  `json:"footer" bson:"footer"`
	CreatedAt time.Time               `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time               `json:"updated_at" bson:"updated_at"`
}

type TableFactPremiumRedeem struct {
	ID                  primitive.ObjectID `json:"id" bson:"_id"`
	UserID              primitive.ObjectID `json:"user_id" bson:"user_id"`
	ItemID              primitive.ObjectID `json:"item_id" bson:"item_id"`
	ParentID            primitive.ObjectID `json:"parent_item_id" bson:"parent_item_id"`
	Size                string             `json:"size" bson:"size"`
	Point               int                `json:"point" bson:"point"`
	ParentName          string             `json:"parent_name" bson:"parent_name"`
	Name                string             `json:"name" bson:"name"`
	ImgURL              string             `json:"img_url" bson:"img_url"`
	TrackingStatusID    int                `json:"tracking_status_id" bson:"tracking_status_id"`
	TrackingStatusTitle string             `json:"tracking_status_title" bson:"tracking_status_title"`
	TrackingNo          string             `json:"tracking_no" bson:"tracking_no"`
	LineUserID          string             `json:"line_user_id" bson:"line_user_id"`
	LineDisplayName     string             `json:"line_display_name" bson:"line_display_name"`
	LineDisplayImage    string             `json:"line_display_image" bson:"line_display_image"`
	FirstName           string             `json:"first_name" bson:"first_name"`
	LastName            string             `json:"last_name" bson:"last_name"`
	Email               string             `json:"email" bson:"email"`
	PhoneNumber         string             `json:"phone_number" bson:"phone_number"`
	Birthday            string             `json:"birthday" bson:"birthday"`
	Sex                 string             `json:"sex" bson:"sex"`
	Address             string             `json:"address" bson:"address"`
	ZipCode             string             `json:"zip_code" bson:"zip_code"`
	ProvinceID          string             `json:"province_id" bson:"province_id"`
	CreatedAt           time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt           time.Time          `json:"updated_at" bson:"updated_at"`
}

type RequestPremiumRedeem struct {
	UserID primitive.ObjectID `json:"user_id" bson:"user_id"`
	ItemID primitive.ObjectID `json:"item_id" bson:"item_id"`
	Size   string             `json:"size" bson:"size"`
}

type RequestPremiumRedeemImportUpdate struct {
	ID         string `json:"_id" bson:"_id"`
	TrackingNo string `json:"tracking_no" bson:"tracking_no"`
}

type ResponsePremiumHistory []struct {
	ImgURL           string `json:"img_url" bson:"user_id"`
	Name             string `json:"name" bson:"user_id"`
	Size             string `json:"size" bson:"user_id"`
	TrackingNo       string `json:"tracking_no" bson:"user_id"`
	TrackingStatusID int    `json:"tracking_status_id" bson:"user_id"`
	CreatedAt        string `json:"created_at" bson:"user_id"`
}

type FilterPremiumListItems struct {
	Name string `json:"name" bson:"name"`
}

type FilterPremiumList struct {
	Sort     string `json:"sort" bson:"sort"`
	Order    string `json:"order" bson:"order"`
	Offset   int64  `json:"offset" bson:"offset"`
	Limit    int64  `json:"limit" bson:"limit"`
	IsExport bool   `json:"is_export" bson:"is_export"`
	Filter   *FilterPremiumListItems
}
