package constants

import (
	"fmt"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
	"os"
)

// Chanel Detail
func GetDetailLineChanel() *typesSDK.LineChanelDetail {
	env := os.Getenv("ENV_STAGING")
	if env == "" {
		env = "local"
	}
	if env == "local" {
		return &typesSDK.LineChanelDetail{
			ChannelID:          "1656015528",
			ChannelSecret:      "da3c33e0a0d498e169c8fd181580b396",
			ChannelAccessToken: "9Lu23o04Q8dir0UX3g4COKEdwg83Xkog4LkOBgbelNOf+BDnzqL14QIqLrdvSCJdBbPshtNRtrP8mWrVy0ssTmuTNgtaMWh77ZXrkS6maDBZCtiDa9p9UXQnugpsillPS0/fOEMOseMG4X9lWZXJlgdB04t89/1O/w1cDnyilFU=",
		}
	} else if env == "dev" {
		return &typesSDK.LineChanelDetail{
			ChannelID:          "1656015528",
			ChannelSecret:      "da3c33e0a0d498e169c8fd181580b396",
			ChannelAccessToken: "9Lu23o04Q8dir0UX3g4COKEdwg83Xkog4LkOBgbelNOf+BDnzqL14QIqLrdvSCJdBbPshtNRtrP8mWrVy0ssTmuTNgtaMWh77ZXrkS6maDBZCtiDa9p9UXQnugpsillPS0/fOEMOseMG4X9lWZXJlgdB04t89/1O/w1cDnyilFU=",
		}
	} else if env == "prod" {
		return &typesSDK.LineChanelDetail{
			ChannelID:          "1656015528",
			ChannelSecret:      "da3c33e0a0d498e169c8fd181580b396",
			ChannelAccessToken: "9Lu23o04Q8dir0UX3g4COKEdwg83Xkog4LkOBgbelNOf+BDnzqL14QIqLrdvSCJdBbPshtNRtrP8mWrVy0ssTmuTNgtaMWh77ZXrkS6maDBZCtiDa9p9UXQnugpsillPS0/fOEMOseMG4X9lWZXJlgdB04t89/1O/w1cDnyilFU=",
		}
	}
	return &typesSDK.LineChanelDetail{}
}

// Message
func GetMessageAfterAdminSaveBill(vDate string, vPoint int, BillSummary float32, vStatus int8) string {
	return fmt.Sprintf(`{
  "type": "flex",
  "altText": "แจ้งอัพเดทสถานะ",
  "contents": {
    "type": "bubble",
    "hero": {
      "type": "image",
      "url": "https://dev-app-yi-nivea.s3-ap-southeast-1.amazonaws.com/assets/images/logo-top-right.png",
      "size": "xxl",
      "aspectRatio": "2:1",
      "aspectMode": "fit"
    },
    "body": {
      "type": "box",
      "layout": "vertical",
      "spacing": "md",
      "contents": [
        {
          "type": "text",
          "text": "แจ้งอัพเดทสถานะใบเสร็จ",
          "wrap": true,
          "size": "md",
          "weight": "bold"
        },
        {
          "type": "separator"
        },
        {
          "type": "box",
          "layout": "vertical",
          "spacing": "sm",
          "contents": [
            {
              "type": "box",
              "layout": "baseline",
              "contents": [
                {
                  "type": "text",
                  "text": "วันที่/เวลา:",
                  "margin": "sm",
                  "color": "#555555",
                  "flex": 0
                },
                {
                  "type": "text",
                  "text": "%s",
                  "size": "sm",
                  "align": "end"
                }
              ]
            },
            {
              "type": "box",
              "layout": "baseline",
              "contents": [
                {
                  "type": "text",
                  "text": "จำนวนสิทธิ์ที่ได้รับ:",
                  "margin": "sm",
                  "color": "#555555",
                  "flex": 0
                },
                {
                  "type": "text",
                  "text": "%d",
                  "size": "sm",
                  "align": "end"
                }
              ]
            },
            {
              "type": "box",
              "layout": "baseline",
              "contents": [
                {
                  "type": "text",
                  "text": "ยอดใบเสร็จ:",
                  "margin": "sm",
                  "color": "#555555",
                  "flex": 0
                },
                {
                  "type": "text",
                  "text": "%s",
                  "size": "sm",
                  "align": "end"
                }
              ]
            },
            {
              "type": "box",
              "layout": "baseline",
              "contents": [
                {
                  "type": "text",
                  "text": "สถานะ:",
                  "margin": "sm",
                  "color": "#555555",
                  "flex": 0
                },
                {
                  "type": "text",
                  "text": "%s",
                  "size": "sm",
                  "align": "end",
                  "color": "#ff0000"
                }
              ]
            }
          ]
        }
      ]
    }
  }
}`, vDate, vPoint, fmt.Sprintf("%.2f", BillSummary), GetStatusTitleBill(vStatus))
}
