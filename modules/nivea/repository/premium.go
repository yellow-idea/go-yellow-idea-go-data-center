package repository

import (
	"context"
	"fmt"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/helper"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/nivea/constants"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/nivea/types"
	driverMongoSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/drivers/mongodb"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

type PremiumRepository interface {
	UpdateTransaction()
	ListData(payload *types.FilterPremiumList) *typesSDK.SaveRepositoryResponse
	ShowData(id string) *typesSDK.SaveRepositoryResponse
	CreateData(payload *types.TableDimPremium) *typesSDK.SaveRepositoryResponse
	UpdateData(id string, payload *types.TableDimPremium) *typesSDK.SaveRepositoryResponse
	DeleteData(id string) *typesSDK.SaveRepositoryResponse
	ImportUpdateData(payload []*types.RequestPremiumRedeemImportUpdate) *typesSDK.SaveRepositoryResponse
	ReportRedeemHistory(payload *types.FilterReportPremiumRedeemHistory) *typesSDK.SaveRepositoryResponse
	ReportRedeemHistorySave(payload *types.FilterReportPremiumRedeemHistorySave) *typesSDK.SaveRepositoryResponse
	// App
	GetDataItemByItemID(itemID string) *typesSDK.SaveRepositoryResponse
	ListAll() []*types.TableDimPremium
	Redeem(payload *types.RequestPremiumRedeem, data *types.TableDimPremiumItems) *typesSDK.SaveRepositoryResponse
	RedeemHistory(registerID string) *typesSDK.SaveRepositoryResponse
}

type premiumRepository struct {
	MongoDetail      *typesSDK.SecretMongoDetail
	MongoClient      driverMongoSDK.MongoClientType
	DBName           string
	CollectionName   string
	CollectionRedeem string
}

func NewPremiumRepository() PremiumRepository {
	return &premiumRepository{
		MongoDetail:      helper.SecretGetMongoDetail(constants.MongoServerType),
		MongoClient:      driverMongoSDK.MongoClient,
		DBName:           constants.MongoDatabaseName,
		CollectionName:   constants.DimPremium,
		CollectionRedeem: constants.FactPremiumRedeem,
	}
}

func (d *premiumRepository) UpdateTransaction() {
	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
	}
	defer client.Disconnect(context.TODO())

	var results []*types.TableDimPremium
	// Execute
	findOptions := options.Find()
	cur, err := db.Collection(constants.DimPremium).Find(context.TODO(), bson.D{}, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem types.TableDimPremium
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
	}

	_ = cur.Close(context.TODO())

	// Clear Data
	defer func() { results = nil }()

	for _, v := range results {
		// Update Redeem Premium
		fmt.Println(v.ID)
		_, _ = db.Collection(constants.FactPremiumRedeem).UpdateMany(context.TODO(),
			&bson.D{{"parent_item_id", v.ID}},
			&bson.D{{"$set", &bson.D{
				{"parent_name", v.Name},
			}}})
	}
}

func (d *premiumRepository) ListData(payload *types.FilterPremiumList) *typesSDK.SaveRepositoryResponse {
	var total int64
	resultsEmpty := make([]*types.TableDimPremium, 0)

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	findOptions := options.Find()
	findOptions.SetSort(bson.D{{"updated_at", -1}})
	findOptions.SetSkip(payload.Offset)
	findOptions.SetLimit(payload.Limit)

	var results []*types.TableDimPremium

	filter := bson.D{}
	if payload.Filter.Name != "" {
		filter = append(filter, bson.E{
			Key:   "name",
			Value: bson.D{{"$regex", fmt.Sprintf(".*%s.*", payload.Filter.Name)}},
		})
	}
	cur, err := collection.Find(context.TODO(), filter, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem types.TableDimPremium
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Data Not Found",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	_ = cur.Close(context.TODO())

	// Clear Data
	defer func() { results = nil }()

	// Output
	if results == nil {
		results = resultsEmpty
		total = 0
	} else {
		total, _ = collection.CountDocuments(context.TODO(), filter)
	}

	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Rows:       results,
		Total:      int32(total),
	}
}

func (d *premiumRepository) ShowData(id string) *typesSDK.SaveRepositoryResponse {
	// Valid & Convert ID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
			Data:       nil,
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Data:       nil,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	var result *types.TableDimPremium
	filter := &bson.D{{"_id", _id}}
	err = collection.FindOne(context.TODO(), filter).Decode(&result)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Data:       nil,
		}
	}

	// Clear Data
	defer func() { result = nil }()

	// Output
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Data:       result,
	}
}

func (d *premiumRepository) CreateData(payload *types.TableDimPremium) *typesSDK.SaveRepositoryResponse {
	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	_id := primitive.NewObjectID()
	payload.CreatedAt = helperSDK.GetTimeNowGMT()
	for i, v := range payload.Items {
		if v.ID == primitive.NilObjectID {
			payload.Items[i].ID = primitive.NewObjectID()
			payload.Items[i].ParentID = _id
			payload.Items[i].CreatedAt = payload.CreatedAt
			payload.Items[i].UpdatedAt = payload.CreatedAt
		}
	}
	s := &bson.D{
		{"_id", _id},
		{"name", payload.Name},
		{"sequence", payload.Sequence},
		{"desc_1", payload.Desc1},
		{"desc_2", payload.Desc2},
		{"img_url", payload.ImgURL},
		{"header_1", payload.Header1},
		{"header_2", payload.Header2},
		{"header_3", payload.Header3},
		{"items", payload.Items},
		{"footer", payload.Footer},
		{"created_at", payload.CreatedAt},
		{"updated_at", payload.CreatedAt},
	}
	res, err := collection.InsertOne(context.TODO(), s)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Create Error",
		}
	}

	var result *types.TableDimLuckyDraw
	filter := &bson.D{{"_id", res.InsertedID}}
	_ = collection.FindOne(context.TODO(), filter).Decode(&result)

	// Output
	return &typesSDK.SaveRepositoryResponse{
		ID:         res.InsertedID,
		CodeReturn: 1,
		Message:    "Create Success",
		Data:       result,
	}
}

func (d *premiumRepository) UpdateData(id string, payload *types.TableDimPremium) *typesSDK.SaveRepositoryResponse {
	// Valid & Convert ID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	filter := &bson.D{{"_id", _id}}
	updateAt := helperSDK.GetTimeNowGMT()
	for i, v := range payload.Items {
		if v.ID == primitive.NilObjectID {
			payload.Items[i].ID = primitive.NewObjectID()
			payload.Items[i].ParentID = _id
			payload.Items[i].CreatedAt = updateAt
			payload.Items[i].UpdatedAt = updateAt
		} else {
			payload.Items[i].UpdatedAt = updateAt
		}
	}
	s := &bson.D{
		{"name", payload.Name},
		{"sequence", payload.Sequence},
		{"desc_1", payload.Desc1},
		{"desc_2", payload.Desc2},
		{"img_url", payload.ImgURL},
		{"header_1", payload.Header1},
		{"header_2", payload.Header2},
		{"header_3", payload.Header3},
		{"items", payload.Items},
		{"footer", payload.Footer},
		{"updated_at", updateAt},
	}
	update := &bson.D{{"$set", s}}
	res, err := collection.UpdateOne(context.TODO(), filter, update)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			ID:         _id,
			CodeReturn: -1,
			Message:    "Update Error",
		}
	}

	// Output Error
	if res.ModifiedCount <= 0 {
		return &typesSDK.SaveRepositoryResponse{
			ID:         _id,
			CodeReturn: 2,
			Message:    "Data Not Found",
		}
	}

	// Get Data
	var result *types.TableDimPremium
	filter = &bson.D{{"_id", _id}}
	_ = collection.FindOne(context.TODO(), filter).Decode(&result)

	defer func() { result = nil }()

	// Output
	return &typesSDK.SaveRepositoryResponse{
		ID:         _id,
		CodeReturn: 1,
		Message:    "Update Success",
		Data:       result,
	}
}

func (d *premiumRepository) DeleteData(id string) *typesSDK.SaveRepositoryResponse {
	// Valid & Convert ID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	filter := bson.D{{"_id", _id}}
	res, err := collection.DeleteOne(context.TODO(), filter)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			ID:         _id,
			CodeReturn: -1,
			Message:    "Delete Error",
		}
	}

	// Output Error
	if res.DeletedCount <= 0 {
		return &typesSDK.SaveRepositoryResponse{
			ID:         _id,
			CodeReturn: 2,
			Message:    "Data Not Found",
		}
	}

	// Output Error
	return &typesSDK.SaveRepositoryResponse{
		ID:         _id,
		CodeReturn: 1,
		Message:    "Delete Success",
	}
}

func (d *premiumRepository) ImportUpdateData(payload []*types.RequestPremiumRedeemImportUpdate) *typesSDK.SaveRepositoryResponse {
	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	for _, v := range payload {
		if v.TrackingNo == "" {
			continue
		}
		// Valid & Convert ID
		_id, err := primitive.ObjectIDFromHex(v.ID)
		if err != nil {
			fmt.Println(err)
			continue
		}
		_, err = db.Collection(constants.FactPremiumRedeem).UpdateOne(context.TODO(),
			&bson.D{{"_id", _id}},
			&bson.D{{"$set", &bson.D{
				{"tracking_status_id", 1},
				{"tracking_status_title", constants.GetStatusTitleTracking(1)},
				{"tracking_no", v.TrackingNo},
				{"updated_at", helperSDK.GetTimeNowGMT()},
			}}})
		// Output Error
		if err != nil {
			fmt.Println(err)
		}
	}

	// Output
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Update Success",
	}
}

func (d *premiumRepository) GetDataItemByItemID(itemID string) *typesSDK.SaveRepositoryResponse {
	// Valid & Convert ID
	_itemID, err := primitive.ObjectIDFromHex(itemID)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
			Data:       nil,
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Data:       nil,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	var result *types.TableDimPremium
	filter := &bson.D{{"items._id", _itemID}}
	err = collection.FindOne(context.TODO(), filter).Decode(&result)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Data:       nil,
		}
	}

	// Clear Data
	defer func() { result = nil }()

	// Output
	var resultItem *types.TableDimPremiumItems
	for _, v := range result.Items {
		if v.ID == _itemID {
			resultItem = v
		}
	}
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Data:       resultItem,
	}
}

func (d *premiumRepository) ListAll() []*types.TableDimPremium {
	resultsEmpty := make([]*types.TableDimPremium, 0)

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
		return resultsEmpty
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	findOptions := options.Find()
	findOptions.SetSort(&bson.D{{"sequence", 1}})
	var results []*types.TableDimPremium
	t1 := time.Now()
	locationTime, errTime := time.LoadLocation("Asia/Bangkok")
	if errTime != nil {
		fmt.Println(errTime)
	} else {
		t1 = time.Now().In(locationTime)
	}

	cur, err := collection.Find(context.TODO(), bson.D{
		bson.E{
			Key:   "items.start_date",
			Value: bson.M{"$lte": t1}},
	}, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return resultsEmpty
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem types.TableDimPremium
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
		return resultsEmpty
	}

	_ = cur.Close(context.TODO())

	// Clear Data
	defer func() { results = nil }()

	// Output
	if results == nil {
		results = resultsEmpty
	}

	//RemoveIndex := func(s []*types.TableDimPremiumItems, index int) []*types.TableDimPremiumItems {
	//	return append(s[:index], s[index+1:]...)
	//}
	//
	//for i, v := range results {
	//	for i2, v2 := range v.Items {
	//		hs := t1.Sub(v2.StartDate).Hours()
	//		hs, mf := math.Modf(hs)
	//		ms := mf * 60
	//		ms, sf := math.Modf(ms)
	//		ss := sf * 60
	//		if ss > 0 {
	//			RemoveIndex(results[i].Items, i2)
	//		}
	//	}
	//}
	return results
}

func (d *premiumRepository) Redeem(payload *types.RequestPremiumRedeem, data *types.TableDimPremiumItems) *typesSDK.SaveRepositoryResponse {
	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	collection := db.Collection(d.CollectionName)
	var result *types.TableDimPremium
	filter := &bson.D{{"items._id", payload.ItemID}}
	_ = collection.FindOne(context.TODO(), filter).Decode(&result)

	// Clear Data
	defer func() { result = nil }()

	// Check Stock
	countStock := 0
	if result != nil {
		for _, v := range result.Items {
			if v.ID == payload.ItemID {
				countStock = v.ItemStock
			}
		}
	}

	if countStock <= 0 {
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 4,
			Message:    "Stock Not Enough",
		}
	}

	pt := data.Point
	//isHave := false
	//for _, v := range data.Option {
	//	if v.Size == payload.Size {
	//		pt = v.Point
	//		isHave = true
	//	}
	//}
	//if isHave == false {
	//	pt = data.Point
	//}

	// Get Register Data
	collection = db.Collection(constants.DimRegister)
	var registerData *types.TableDimRegister
	err = collection.FindOne(context.TODO(), &bson.D{{"_id", payload.UserID}}).Decode(&registerData)

	// Get Register Address
	collection = db.Collection(constants.DimRegisterAddress)
	var vAdd *types.TableDimRegisterAddress
	err = collection.FindOne(context.TODO(), &bson.D{
		{"user_id", payload.UserID},
		{"default", "1"},
	}).Decode(&vAdd)

	// Get Premium Data
	collection = db.Collection(constants.DimPremium)
	var premiumData *types.TableDimPremium
	err = collection.FindOne(context.TODO(), &bson.D{{"_id", data.ParentID}}).Decode(&premiumData)

	// Execute
	collection = db.Collection(d.CollectionRedeem)
	createdAt := helperSDK.GetTimeNowGMT()
	_, err = collection.InsertOne(context.TODO(), &types.TableFactPremiumRedeem{
		ID:                  primitive.NewObjectID(),
		UserID:              payload.UserID,
		ItemID:              payload.ItemID,
		ParentID:            data.ParentID,
		ParentName:          premiumData.Name,
		Name:                data.Name,
		ImgURL:              data.ImgURL,
		Size:                payload.Size,
		TrackingStatusID:    2,
		TrackingStatusTitle: constants.GetStatusTitleTracking(2),
		Point:               pt,
		LineUserID:          registerData.LineUserID,
		LineDisplayName:     registerData.LineDisplayName,
		LineDisplayImage:    registerData.LineDisplayImage,
		Email:               registerData.Email,
		Birthday:            registerData.Birthday,
		Sex:                 registerData.Sex,
		FirstName:           vAdd.FirstName,
		LastName:            vAdd.LastName,
		PhoneNumber:         vAdd.PhoneNumber,
		Address:             vAdd.Address,
		ProvinceID:          vAdd.ProvinceID,
		ZipCode:             vAdd.ZipCode,
		CreatedAt:           createdAt,
		UpdatedAt:           createdAt,
	})

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Redeem Error",
		}
	}

	// Cut Stock / Update Stock
	updatedAt := helperSDK.GetTimeNowGMT()
	if result != nil {
		for i, v := range result.Items {
			if v.ID == payload.ItemID {
				result.Items[i].ItemStock = result.Items[i].ItemStock - 1
			}
		}
	}
	collection = db.Collection(d.CollectionName)
	_, _ = collection.UpdateOne(context.TODO(),
		&bson.D{{"_id", result.ID}},
		&bson.D{{"$set", &bson.D{
			{"items", result.Items},
			{"updated_at", updatedAt},
		}}})

	// Update IsUsed All Bill
	collection = db.Collection(constants.FactBill)
	_, _ = collection.UpdateMany(context.TODO(),
		&bson.D{{"user_id", payload.UserID}},
		&bson.D{{"$set", &bson.D{
			{"is_used", 1},
			//{"updated_at", updatedAt},
		}}})

	// Clear Data
	defer func() {
		registerData = nil
		vAdd = nil
	}()

	// Output
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Redeem Success",
	}
}

func (d *premiumRepository) RedeemHistory(registerID string) *typesSDK.SaveRepositoryResponse {
	resultsEmpty := make([]*types.TableFactPremiumRedeem, 0)

	// Valid & Convert ID
	_registerID, err := primitive.ObjectIDFromHex(registerID)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
			Data:       resultsEmpty,
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connect Fail",
			Data:       resultsEmpty,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionRedeem)
	findOptions := options.Find()

	var results []*types.TableFactPremiumRedeem
	cur, err := collection.Find(context.TODO(), &bson.D{{"user_id", _registerID}}, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Data:       resultsEmpty,
		}
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem types.TableFactPremiumRedeem
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Data:       resultsEmpty,
		}
	}

	_ = cur.Close(context.TODO())

	// Clear Data
	defer func() { results = nil }()

	// Output
	if results == nil {
		results = resultsEmpty
	}
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Data:       results,
	}
}

func (d *premiumRepository) ReportRedeemHistory(payload *types.FilterReportPremiumRedeemHistory) *typesSDK.SaveRepositoryResponse {
	resultsEmpty := make([]*types.TableFactPremiumRedeem, 0)
	var results []*types.TableFactPremiumRedeem
	filter := bson.D{}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Rows:       resultsEmpty,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(constants.FactPremiumRedeem)
	findOptions := options.Find()
	// No Projection When Export
	if !payload.IsExport {
		findOptions.SetProjection(&bson.M{
			"name":                  1,
			"first_name":            1,
			"last_name":             1,
			"address":               1,
			"email":                 1,
			"phone_number":          1,
			"zip_code":              1,
			"tracking_status_id":    1,
			"tracking_status_title": 1,
			"tracking_no":           1,
			"size":                  1,
			"created_at":            1,
		})
	}
	findOptions.SetSort(&bson.D{{"updated_at", 1}})

	if payload.PhoneNumber != "" {
		filter = append(filter, bson.E{
			Key:   "phone_number",
			Value: bson.D{{"$regex", fmt.Sprintf(".*%s.*", payload.PhoneNumber)}},
		})
	}
	if payload.StatusID != -1 {
		filter = append(filter, bson.E{
			Key:   "tracking_status_id",
			Value: payload.StatusID,
		})
	}
	if !payload.StartDate.IsZero() && !payload.EndDate.IsZero() {
		filter = append(filter, bson.E{
			Key: "created_at",
			Value: bson.M{
				"$gte": payload.StartDate,
				"$lte": payload.EndDate,
			}})
	}
	cur, err := collection.Find(context.TODO(), filter, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem types.TableFactPremiumRedeem
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Data Not Found",
			Rows:       resultsEmpty,
		}
	}

	_ = cur.Close(context.TODO())

	// Clear Data
	defer func() {
		results = nil
	}()

	// Output
	if results == nil {
		results = resultsEmpty
	}
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Rows:       results,
	}
}

func (d *premiumRepository) ReportRedeemHistorySave(payload *types.FilterReportPremiumRedeemHistorySave) *typesSDK.SaveRepositoryResponse {
	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionRedeem)
	filter := &bson.D{{"_id", payload.ID}}
	s := &bson.D{
		{"tracking_status_id", payload.TrackingStatusID},
		{"tracking_no", payload.TrackingNo},
		{"updated_at", helperSDK.GetTimeNowGMT()},
	}
	update := &bson.D{{"$set", s}}
	res, err := collection.UpdateOne(context.TODO(), filter, update)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Update Error",
		}
	}

	// Output Error
	if res.ModifiedCount <= 0 {
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Data Not Found",
		}
	}

	// Output
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Update Success",
	}
}
