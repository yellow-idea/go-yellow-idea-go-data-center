package repository

import (
	"fmt"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/nivea/types"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	"testing"
)

func TestLuckyDrawRepository_CreateData(t *testing.T) {
	payload := &types.TableDimLuckyDraw{
		Name: "Koko",
	}

	a := NewLuckyDrawRepository()
	res := a.CreateData(payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}

func TestLuckyDrawRepository_ListData(t *testing.T) {
	payload := &types.FilterLuckyDrawList{
		Sort:   "",
		Order:  "",
		Offset: 0,
		Limit:  10,
		//IsExport: False,
		Filter: &types.FilterLuckyDrawListItems{
			Name: "",
		},
	}

	a := NewLuckyDrawRepository()
	res := a.ListData(payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(helperSDK.InterfaceToJson(&res.Rows))
}

func TestLuckyDrawRepository_ShowData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewLuckyDrawRepository()
	res := a.ShowData(id)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(helperSDK.InterfaceToJson(&res.Data))
}

func TestLuckyDrawRepository_UpdateData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewLuckyDrawRepository()
	payload := &types.TableDimLuckyDraw{
		Name: "Koko",
	}
	res := a.UpdateData(id, payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}

func TestLuckyDrawRepository_DeleteData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewLuckyDrawRepository()
	res := a.DeleteData(id)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}
