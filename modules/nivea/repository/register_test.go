package repository

import (
	"fmt"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/nivea/types"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	"os"
	"testing"
)

func TestRegisterRepository_CreateData(t *testing.T) {
	payload := &types.TableDimRegister{
		//Name: "Koko",
	}

	a := NewRegisterRepository()
	res := a.CreateData(payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}

func TestRegisterRepository_ListData(t *testing.T) {
	payload := &types.FilterRegisterList{
		Sort:   "",
		Order:  "",
		Offset: 0,
		Limit:  10,
		//IsExport: False,
		Filter: &types.FilterRegisterListItems{
			Name: "",
		},
	}

	a := NewRegisterRepository()
	res := a.ListData(payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(helperSDK.InterfaceToJson(&res.Rows))
}

func TestRegisterRepository_ShowData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewRegisterRepository()
	res := a.ShowData(id)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(helperSDK.InterfaceToJson(&res.Data))
}

func TestRegisterRepository_UpdateData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewRegisterRepository()
	payload := &types.TableDimRegister{
		//Name: "Koko",
	}
	res := a.UpdateData(id, payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}

func TestRegisterRepository_DeleteData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewRegisterRepository()
	res := a.DeleteData(id)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}

func TestUpdateTransaction(t *testing.T) {
	_ = os.Setenv("ENV_STAGING", "prod")
	a := NewRegisterRepository()
	a.UpdateTransaction()
}
