package router

import (
	"fmt"
	"github.com/gin-gonic/gin"
	serviceLine "gitlab.yellow-idea.com/yellow-idea-go-data-center/modules-standard/line/service"
	typesLine "gitlab.yellow-idea.com/yellow-idea-go-data-center/modules-standard/line/types"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/nivea/constants"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/nivea/service"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/nivea/types"
	httpSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/http"
	"go.mongodb.org/mongo-driver/bson"
	"net/http"
)

func NiveaRouterApp(r *gin.Engine) {
	// App Init
	r.GET("/nivea/app_init", func(c *gin.Context) {
		s := service.NewRegisterService()
		c.JSON(http.StatusOK, s.AppInit())
	})
	// LineUser TODO: Standard
	li := r.Group("/nivea/line/user")
	{
		s := serviceLine.NewLineUserService(constants.MongoServerType, constants.MongoDatabaseName)
		// Update Profile
		li.POST("/update", func(c *gin.Context) {
			var payload *typesLine.RequestUpdateProfile
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := s.UpdateProfile(payload)
			c.JSON(http.StatusOK, res)
		})
	}
	// Register
	reg := r.Group("/nivea/register")
	{
		s := service.NewRegisterService()
		// Get Detail
		reg.GET("/detail/:id", func(c *gin.Context) {
			mid := c.Param("id")
			res := s.ShowDataByMid(mid)
			c.JSON(http.StatusOK, &gin.H{
				"data": res.Data,
			})
		})
		// Point
		reg.GET("/point/:id", func(c *gin.Context) {
			id := c.Param("id")
			c.JSON(http.StatusOK, s.GetPoint(id))
		})
		// History Premium Redeem
		reg.GET("/premium-history/:id", func(c *gin.Context) {
			id := c.Param("id")
			c.JSON(http.StatusOK, s.GetPremiumHistory(id))
		})
		// Address Create
		reg.POST("/address/:id", func(c *gin.Context) {
			id := c.Param("id")
			var payload *types.TableDimRegisterAddress
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			c.JSON(http.StatusOK, s.CreateAddress(id, payload))
		})
		// Address Show
		reg.GET("/address/:id", func(c *gin.Context) {
			id := c.Param("id")
			res := s.GetAddressById(id)
			c.JSON(http.StatusOK, &gin.H{
				"data": res.Data,
			})
		})
		// Address List By Register
		reg.GET("/address-list/:id", func(c *gin.Context) {
			id := c.Param("id")
			res := s.GetAddress(id)
			c.JSON(http.StatusOK, &gin.H{
				"datas": res.Rows,
			})
		})
		// Address Set Default
		reg.PUT("/address-default", func(c *gin.Context) {
			var payload map[string]string
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			c.JSON(http.StatusOK, s.UpdateAddressToDefault(payload["user_id"], payload["address_id"]))
		})
		// Address Show Default by Register ID
		reg.GET("/address-default/:id", func(c *gin.Context) {
			id := c.Param("id")
			res := s.GetAddressDefaultByRegID(id)
			c.JSON(http.StatusOK, &gin.H{
				"data": res.Data,
			})
		})
		// Address Update
		reg.PUT("/address/:id", func(c *gin.Context) {
			id := c.Param("id")
			var payload *types.TableDimRegisterAddress
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			c.JSON(http.StatusOK, s.UpdateAddress(id, payload))
		})
		// Register Submit
		reg.POST("", func(c *gin.Context) {
			var payload *types.TableDimRegister
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			if payload.ProvinceID == "" ||
				payload.Birthday == "" {
				c.JSON(http.StatusOK, bson.M{
					"code_return": 2,
					"message":     "Invalid Input",
				})
			} else {
				c.JSON(http.StatusOK, s.Register(payload))
			}
		})
	}
	// Bill
	bi := r.Group("/nivea/bill")
	{
		s := service.NewBillService()
		// Bill Upload
		bi.POST("/upload", func(c *gin.Context) {
			var payload *types.RequestBillUpload
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			c.JSON(http.StatusOK, s.Upload(payload))
		})
		// ประวัติ
		bi.POST("/history", func(c *gin.Context) {
			var payload *types.RequestBillHistory
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := s.GetHistory(payload)
			c.JSON(http.StatusOK, &gin.H{
				"data": res.Rows,
			})
		})
		// ร้องเรียน
		bi.POST("/complaint", func(c *gin.Context) {
			var payload *types.TableFactBill
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			c.JSON(http.StatusOK, s.Complaint(payload))
		})
	}
	// Premium
	pr := r.Group("/nivea/premium")
	{
		s := service.NewPremiumService()
		// TODO: Redeem ตัด stock
		pr.POST("/redeem", func(c *gin.Context) {
			var payload *types.RequestPremiumRedeem
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			c.JSON(http.StatusOK, s.Redeem(payload))
		})
	}
	// LuckyDraw
	luk := r.Group("/nivea/lucky-draw")
	{
		s := service.NewLuckyDrawService()
		// Redeem
		luk.POST("/redeem", func(c *gin.Context) {
			var payload *types.RequestLuckyDrawRedeem
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			c.JSON(http.StatusOK, s.Redeem(payload))
		})
	}
}

func NiveaRouterCms(r *gin.Engine) {
	rep := r.Group("/cms/nivea/report")
	{
		s := service.NewBillService()
		sr := service.NewRegisterService()
		sl := service.NewLuckyDrawService()
		sp := service.NewPremiumService()
		// [CMS] Report ข้อมูลลงทะเบียน
		rep.POST("/register/list", func(c *gin.Context) {
			var payload *types.FilterReportRegisterRequest
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := sr.ReportRegister(payload)
			if payload.IsExport {
				c.JSON(http.StatusOK, res.Data)
			} else {
				c.JSON(http.StatusOK, &gin.H{
					"rows":  res.Rows,
					"total": res.Total,
				})
			}
			//s := make([]map[string]interface{}, 0)
			//s = append(s, map[string]interface{}{
			//	"id":                 "5eb1392897f89d0007b69bae",
			//	"line_display_name":  "นุ่น",
			//	"line_display_image": "https://profile.line-scdn.net/0hWVb31DcICHlfSiCHR5t3LmMPBhQoZA4xJyxHTHwaAR4lc057YyhDGypMVx0neU0sYCkVGH1CVU10",
			//	"first_name":         "วิรากร",
			//	"last_name":          "เกษรามัญ",
			//	"phone_number":       "0956142455",
			//	"email":              "nunwiragonk@gmail.com",
			//	"province":           "Province",
			//	"zip_code":           "Zip_Code",
			//	"address":            "Address",
			//	"sex":                "sex",
			//	"birthday":           "birthday",
			//	"created_at":         "2020-05-05T17:00:08.931Z",
			//})
			//c.JSON(http.StatusOK, &gin.H{
			//	"rows":  s,
			//	"total": len(s),
			//})
		})
		// [CMS] Report ลูกค้าที่ร่วมกิจกรรม
		rep.POST("/bill/list", func(c *gin.Context) {
			var payload *types.FilterReportRegisterRequest
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := s.ReportBillCustomer(payload)
			if payload.IsExport {
				c.JSON(http.StatusOK, res.Data)
			} else {
				c.JSON(http.StatusOK, &gin.H{
					"rows":  res.Rows,
					"total": res.Total,
				})
			}
		})
		// [CMS] Report จำนวนสิทธิ์
		rep.POST("/bill/list-by-point", func(c *gin.Context) {
			var payload *types.FilterReportRegisterRequest
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := s.ReportPointByUser(payload)
			if payload.IsExport {
				c.JSON(http.StatusOK, res.Data)
			} else {
				c.JSON(http.StatusOK, &gin.H{
					"rows":  res.Rows,
					"total": res.Total,
				})
			}
		})
		// [CMS] Report ตรวจสอบใบเสร็จ [Listing]
		rep.POST("/bill/check/list", func(c *gin.Context) {
			var payload *types.FilterReportBillUploadRequest
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := s.ReportUploadBill(payload)
			if payload.IsExport {
				c.JSON(http.StatusOK, res.Data)
			} else {
				c.JSON(http.StatusOK, &gin.H{
					"rows":  res.Rows,
					"total": res.Total,
				})
			}
		})
		// [CMS] Report ตรวจสอบใบเสร็จ [Show]
		rep.GET("/bill/check/detail/:id", func(c *gin.Context) {
			id := c.Param("id")
			res := s.ReportUploadBillDetail(id)
			c.JSON(http.StatusOK, &gin.H{
				"data": res.Data,
			})
		})
		// [CMS] Report ตรวจสอบใบเสร็จ [Show] > Check ใบเสร็จซ้ำ
		rep.GET("/bill/check/duplicate-bill-no/:id", func(c *gin.Context) {
			billNoAdmin := c.Param("id")
			c.JSON(http.StatusOK, s.BillNoAdminCheckDuplicate(billNoAdmin))
		})
		// [CMS] Report ตรวจสอบใบเสร็จ [Save]
		rep.PUT("/bill/check/detail/:id", func(c *gin.Context) {
			id := c.Param("id")
			var payload *types.TableFactBill
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := s.ReportUploadBillDetailSave(id, payload)
			c.JSON(http.StatusOK, res)
		})
		// [CMS] Report ตรวจสอบใบเสร็จเรียบร้อย [Listing]
		rep.POST("/bill/complete/list", func(c *gin.Context) {
			var payload *types.FilterReportBillUploadSuccessRequest
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := s.ReportUploadBillSuccess(payload)
			if payload.IsExport {
				c.JSON(http.StatusOK, res.Data)
			} else {
				c.JSON(http.StatusOK, &gin.H{
					"rows":  res.Rows,
					"total": res.Total,
				})
			}
		})
		// [CMS] Report ตรวจสอบใบเสร็จเรียบร้อย V2 [Listing]
		rep.POST("/bill/complete/list-v2", func(c *gin.Context) {
			var payload *types.FilterReportBillUploadSuccessRequest
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := s.ReportUploadBillSuccessV2(payload)
			if payload.IsExport {
				c.JSON(http.StatusOK, res.Data)
			} else {
				c.JSON(http.StatusOK, &gin.H{
					"rows":  res.Rows,
					"total": res.Total,
				})
			}
		})
		// [CMS] Report ร้องเรียนใบเสร็จ [Listing]
		rep.POST("/bill/complaint/list", func(c *gin.Context) {
			var payload *types.FilterReportBillUploadSuccessRequest
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := s.ReportUploadBillComplaint(payload)
			if payload.IsExport {
				c.JSON(http.StatusOK, res.Data)
			} else {
				c.JSON(http.StatusOK, &gin.H{
					"rows":  res.Rows,
					"total": res.Total,
				})
			}
			//s := make([]map[string]interface{}, 0)
			//s = append(s, map[string]interface{}{
			//	"id":           "5eb816d047f49af1a5d97d7b",
			//	"first_name":   "พัชรณัฏฐ์",
			//	"last_name":    "แก่นอินทร์",
			//	"phone_number": "0656969092",
			//
			//	"previous_point":  0,
			//	"previous_status": 4,
			//
			//	"img_url":       "https://cdn.yellow-idea.com/images/weber-network/337409a0-8f5b-11ea-bea0-597b5ad3bae7.jpeg",
			//	"bill_no":       "A0001",
			//	"bill_no_admin": "630408-0019",
			//	"updated_at":    "2020-05-06T16:27:39.821Z",
			//	"shop_type":     "Shop type",
			//	"shop_type_2":   "Shop type2",
			//})
			//c.JSON(http.StatusOK, &gin.H{
			//	"rows":  s,
			//	"total": len(s),
			//})
		})
		// [CMS] ประวัติแลกของรางวัล [Listing]
		rep.POST("/premium/history/list", func(c *gin.Context) {
			var payload *types.FilterReportPremiumRedeemHistory
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := sp.ReportRedeemHistory(payload)
			if payload.IsExport {
				c.JSON(http.StatusOK, res.Data)
			} else {
				c.JSON(http.StatusOK, &gin.H{
					"data": res.Rows,
				})
			}
			//s := make([]map[string]interface{}, 0)
			//s = append(s, map[string]interface{}{
			//	"id":                    "5eb80f1724501f99538c9948",
			//	"name":                  "มินิแอลอีดี (มี 3 ลายให้สะสม)",
			//	"first_name":            "Sompon",
			//	"last_name":             "Juicerod",
			//	"address":               "Bangkok",
			//	"email":                 "ragnarokmaster03@gmail.com",
			//	"phone_number":          "0392838282",
			//	"zip_code":              "10260",
			//	"tracking_status_id":    2,
			//	"tracking_status_title": "ดำเนินการจัดส่งภายใน 7 วัน",
			//	"tracking_no":           "0008-1",
			//	"size":                  "M",
			//	"created_at":            "2020-05-06T16:27:39.821Z",
			//})
			//s = append(s, map[string]interface{}{
			//	"id":                    "5eb80f1724501f99538c9949",
			//	"name":                  "มินิแอลอีดี (มี 3 ลายให้สะสม)",
			//	"first_name":            "Sompon",
			//	"last_name":             "Juicerod",
			//	"address":               "Bangkok",
			//	"email":                 "ragnarokmaster03@gmail.com",
			//	"phone_number":          "0392838282",
			//	"zip_code":              "10260",
			//	"tracking_status_id":    2,
			//	"tracking_status_title": "ดำเนินการจัดส่งภายใน 7 วัน",
			//	"tracking_no":           "0008-1",
			//	"size":                  "M",
			//	"created_at":            "2020-05-06T16:27:39.821Z",
			//})
			//c.JSON(http.StatusOK, map[string]interface{}{
			//	"data": s,
			//})
		})
		// [CMS] ประวัติแลกของรางวัล [Save]
		rep.POST("/premium/history/save", func(c *gin.Context) {
			var payload *types.FilterReportPremiumRedeemHistorySave
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			c.JSON(http.StatusOK, sp.ReportRedeemHistorySave(payload))
		})
		// [CMS] ประวัติแลกของรางวัล [Update List]
		rep.PUT("/premium/history/list", func(c *gin.Context) {
			var payload []*types.RequestPremiumRedeemImportUpdate
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			c.JSON(http.StatusOK, sp.ImportUpdateData(payload))
		})
		// [CMS] ประวัติลุ้นของรางวัล [Listing]
		rep.POST("/lucky-draw/history/list", func(c *gin.Context) {
			var payload *types.FilterReportLuckyDrawRedeemHistory
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := sl.ReportRedeemHistory(payload)
			if payload.IsExport {
				c.JSON(http.StatusOK, res.Data)
			} else {
				c.JSON(http.StatusOK, &gin.H{
					"rows":  res.Rows,
					"total": res.Total,
				})
			}
			//s := make([]map[string]interface{}, 0)
			//s = append(s, map[string]interface{}{
			//	"id":           "5eb80f1724501f99538c9947",
			//	"last_name":    "Juicerod",
			//	"first_name":   "Sompon",
			//	"phone_number": "0392838282",
			//	"address":      "Address",
			//	"zip_code":     "10260",
			//
			//	"name":         "Nivea Mini Fridge ตู้เย็นโตชิบ้ามินิบาร์ 1.7 คิว",
			//	"created_at":   "2020-05-06T16:27:39.821Z",
			//})
			//c.JSON(http.StatusOK, &gin.H{
			//	"rows":  s,
			//	"total": len(s),
			//})
		})
		// [CMS] Report ตรวจสอบใบเสร็จ [Listing-All]
		rep.POST("/bill/check/all", func(c *gin.Context) {
			var payload *types.FilterReportBillUploadRequest
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := s.ReportUploadBillAll(payload)
			c.JSON(http.StatusOK, &gin.H{
				"rows":  res.Rows,
				"total": res.Total,
			})
		})
	}
	// Premium
	pr := r.Group("/cms/nivea/premium")
	{
		s := service.NewPremiumService()
		// List
		pr.POST("/list", func(c *gin.Context) {
			var payload *types.FilterPremiumList
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := s.ListData(payload)
			c.JSON(http.StatusOK, &gin.H{
				"rows":  res.Rows,
				"total": res.Total,
			})
		})
		// Show
		pr.GET("/show/:id", func(c *gin.Context) {
			id := c.Param("id")
			res := s.ShowData(id)
			c.JSON(http.StatusOK, &gin.H{
				"data": res.Data,
			})
		})
		// Create
		pr.POST("/new", func(c *gin.Context) {
			var payload *types.TableDimPremium
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := s.CreateData(payload)
			c.JSON(http.StatusOK, res)
		})
		// Update
		pr.PUT("/edit/:id", func(c *gin.Context) {
			id := c.Param("id")
			var payload *types.TableDimPremium
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := s.UpdateData(id, payload)
			c.JSON(http.StatusOK, res)
		})
		// Delete
		pr.DELETE("/:id", func(c *gin.Context) {
			id := c.Param("id")
			res := s.DeleteData(id)
			c.JSON(http.StatusOK, res)
		})
	}
	// LuckyDraw
	luk := r.Group("/cms/nivea/lucky-draw")
	{
		s := service.NewLuckyDrawService()
		// List
		luk.POST("/list", func(c *gin.Context) {
			var payload *types.FilterLuckyDrawList
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := s.ListData(payload)
			c.JSON(http.StatusOK, &gin.H{
				"rows":  res.Rows,
				"total": res.Total,
			})
		})
		// Show
		luk.GET("/show/:id", func(c *gin.Context) {
			id := c.Param("id")
			res := s.ShowData(id)
			c.JSON(http.StatusOK, &gin.H{
				"data": res.Data,
			})
		})
		// Create
		luk.POST("/new", func(c *gin.Context) {
			var payload *types.TableDimLuckyDraw
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := s.CreateData(payload)
			c.JSON(http.StatusOK, res)
		})
		// Update
		luk.PUT("/edit/:id", func(c *gin.Context) {
			id := c.Param("id")
			var payload *types.TableDimLuckyDraw
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := s.UpdateData(id, payload)
			c.JSON(http.StatusOK, res)
		})
		// Delete
		luk.DELETE("/:id", func(c *gin.Context) {
			id := c.Param("id")
			res := s.DeleteData(id)
			c.JSON(http.StatusOK, res)
		})
	}

	set := r.Group("/cms/nivea/setting")
	{
		s := service.NewSettingService()
		set.POST("/save", func(c *gin.Context) {
			var payload *types.TableSettingMain
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := s.SaveData(payload)
			c.JSON(http.StatusOK, res)
		})
		set.GET("/", func(c *gin.Context) {
			res := s.ShowData()
			c.JSON(http.StatusOK, &gin.H{
				"data": res.Data,
			})
		})
	}
}
