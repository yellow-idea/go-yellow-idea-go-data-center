package service

import (
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchie_v2/constants"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchie_v2/repository"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchie_v2/types"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
)

type LuckyDrawService interface {
	ListData(payload *types.FilterLuckyDrawList) *typesSDK.SaveRepositoryResponse
	ShowData(id string) *typesSDK.SaveRepositoryResponse
	CreateData(payload *types.TableDimLuckyDraw) *typesSDK.SaveRepositoryResponse
	UpdateData(id string, payload *types.TableDimLuckyDraw) *typesSDK.SaveRepositoryResponse
	DeleteData(id string) *typesSDK.SaveRepositoryResponse
	ReportRedeemHistory(payload *types.FilterReportLuckyDrawRedeemHistory) *typesSDK.SaveRepositoryResponse // TODO:
	// App
	Redeem(payload *types.RequestLuckyDrawRedeem) *typesSDK.SaveRepositoryResponse
}

type luckyDrawService struct {
	LuckyDrawRepository repository.LuckyDrawRepository
	RegisterRepository  repository.RegisterRepository
}

func NewLuckyDrawService() LuckyDrawService {
	return &luckyDrawService{
		LuckyDrawRepository: repository.NewLuckyDrawRepository(),
		RegisterRepository:  repository.NewRegisterRepository(),
	}
}

func (x *luckyDrawService) ListData(payload *types.FilterLuckyDrawList) *typesSDK.SaveRepositoryResponse {
	return x.LuckyDrawRepository.ListData(payload)
}

func (x *luckyDrawService) ShowData(id string) *typesSDK.SaveRepositoryResponse {
	return x.LuckyDrawRepository.ShowData(id)
}

func (x *luckyDrawService) CreateData(payload *types.TableDimLuckyDraw) *typesSDK.SaveRepositoryResponse {
	payload.ImgURL = helperSDK.S3Base64ToCDN(payload.ImgURL)
	return x.LuckyDrawRepository.CreateData(payload)
}

func (x *luckyDrawService) UpdateData(id string, payload *types.TableDimLuckyDraw) *typesSDK.SaveRepositoryResponse {
	payload.ImgURL = helperSDK.S3Base64ToCDN(payload.ImgURL)
	return x.LuckyDrawRepository.UpdateData(id, payload)
}

func (x *luckyDrawService) DeleteData(id string) *typesSDK.SaveRepositoryResponse {
	return x.LuckyDrawRepository.DeleteData(id)
}

// TODO: Merge to 1 Repository
func (x *luckyDrawService) Redeem(payload *types.RequestLuckyDrawRedeem) *typesSDK.SaveRepositoryResponse {
	itID := helperSDK.InterfaceObjectIdToString(payload.ItemID)
	usrID := helperSDK.InterfaceObjectIdToString(payload.UserID)

	data := x.LuckyDrawRepository.ShowData(itID)
	if data.CodeReturn != 1 {
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: data.CodeReturn,
			Message:    data.Message,
		}
	}

	data2 := x.RegisterRepository.GetPointByRegisterID(usrID)

	dl := data.Data.(*types.TableDimLuckyDraw)

	if data2.Point-dl.Point < 0 {
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Point not Enough",
		}
	}

	res := x.LuckyDrawRepository.Redeem(payload, dl.Point)
	if res.CodeReturn == 1 {
		_ = x.RegisterRepository.UpdatePointByRegisterID(usrID)
	}
	return res
}

func (x *luckyDrawService) ReportRedeemHistory(payload *types.FilterReportLuckyDrawRedeemHistory) *typesSDK.SaveRepositoryResponse {
	res := x.LuckyDrawRepository.ReportRedeemHistory(payload)
	// Export Excel
	if payload.IsExport {
		f := excelize.NewFile()
		sheet1 := "Sheet1"
		fileName := helperSDK.GetTimeNowGMT().Format("200601021504") + "-ReportRedeemHistory.xlsx"
		rows := res.Rows.([]*types.TableFactLuckyDrawRedeem)
		// Header
		_ = f.SetCellValue(sheet1, "A1", "userID")
		_ = f.SetCellValue(sheet1, "B1", "Display Name")
		_ = f.SetCellValue(sheet1, "C1", "Name")
		_ = f.SetCellValue(sheet1, "D1", "LastName")
		_ = f.SetCellValue(sheet1, "E1", "Phone Number")
		_ = f.SetCellValue(sheet1, "F1", "Gender")
		_ = f.SetCellValue(sheet1, "G1", "Birth Day")
		_ = f.SetCellValue(sheet1, "H1", "Email")
		_ = f.SetCellValue(sheet1, "I1", "Address")
		_ = f.SetCellValue(sheet1, "J1", "Province")
		_ = f.SetCellValue(sheet1, "K1", "Postcode")
		_ = f.SetCellValue(sheet1, "L1", "Reward_Item")
		_ = f.SetCellValue(sheet1, "M1", "Redeem_Date")
		// Body
		for i, v := range rows {
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`A%d`, i+2), v.LineUserID)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`B%d`, i+2), v.LineDisplayName)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`C%d`, i+2), v.FirstName)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`D%d`, i+2), v.LastName)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`E%d`, i+2), v.PhoneNumber)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`F%d`, i+2), v.Sex)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`G%d`, i+2), v.Birthday)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`H%d`, i+2), v.Email)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`I%d`, i+2), v.Address)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`J%d`, i+2), v.ProvinceID)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`K%d`, i+2), v.ZipCode)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`L%d`, i+2), v.Name)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`M%d`, i+2), v.CreatedAt)
		}
		if err := f.SaveAs("/tmp/" + fileName); err != nil {
			fmt.Println(err)
		}
		// Upload
		res.Data = helperSDK.S3FileToCDN("/tmp/"+fileName, fmt.Sprintf("%s/%s", constants.UploadFolder, fileName))
	}
	return res
}
