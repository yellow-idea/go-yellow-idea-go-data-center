package service

import (
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchie_v2/repository"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchie_v2/types"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
)

type SettingService interface {
	SaveData(payload *types.TableSettingMain) *typesSDK.SaveRepositoryResponse
	ShowData() *typesSDK.SaveRepositoryResponse
}

type settingService struct {
	SettingRepository repository.SettingRepository
}

func NewSettingService() SettingService {
	return &settingService{
		SettingRepository: repository.NewSettingRepository(),
	}
}

func (x *settingService) SaveData(payload *types.TableSettingMain) *typesSDK.SaveRepositoryResponse {
	return x.SettingRepository.SaveData(payload)
}

func (x *settingService) ShowData() *typesSDK.SaveRepositoryResponse {
	return x.SettingRepository.ShowData()
}
