package service

import (
	"fmt"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchie_v2/types"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	"testing"
)

func TestLuckyDrawService_CreateData(t *testing.T) {
	payload := &types.TableDimLuckyDraw{
		Name: "Koko-oook",
	}

	a := NewLuckyDrawService()
	res := a.CreateData(payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}

func TestLuckyDrawService_ListData(t *testing.T) {
	payload := &types.FilterLuckyDrawList{
		Sort:   "",
		Order:  "",
		Offset: 0,
		Limit:  10,
		//IsExport: False,
		Filter: &types.FilterLuckyDrawListItems{
			Name: "",
		},
	}
	a := NewLuckyDrawService()
	res := a.ListData(payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(helperSDK.InterfaceToJson(&res.Rows))
}

func TestLuckyDrawService_ShowData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewLuckyDrawService()
	res := a.ShowData(id)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(helperSDK.InterfaceToJson(&res.Data))
}

func TestLuckyDrawService_UpdateData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewLuckyDrawService()
	payload := &types.TableDimLuckyDraw{
		Name: "Koko-v2",
	}
	res := a.UpdateData(id, payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}

func TestLuckyDrawService_DeleteData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewLuckyDrawService()
	res := a.DeleteData(id)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}
