package repository

import (
	"context"
	"fmt"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/helper"
	constLine "gitlab.yellow-idea.com/yellow-idea-go-data-center/modules-standard/line/constants"
	typesLine "gitlab.yellow-idea.com/yellow-idea-go-data-center/modules-standard/line/types"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchie_v2/constants"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchie_v2/types"
	driverMongoSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/drivers/mongodb"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type BillRepository interface {
	ListData(payload *types.FilterBillList) *typesSDK.SaveRepositoryResponse
	ShowData(id string) *typesSDK.SaveRepositoryResponse
	CreateData(payload *types.TableFactBill) *typesSDK.SaveRepositoryResponse
	UpdateData(id string, payload *types.TableFactBill) *typesSDK.SaveRepositoryResponse
	DeleteData(id string) *typesSDK.SaveRepositoryResponse
	ReportBillCustomer(payload *types.FilterReportRegisterRequest) *typesSDK.SaveRepositoryResponse                 // TODO:
	ReportUploadBill(payload *types.FilterReportBillUploadRequest) *typesSDK.SaveRepositoryResponse                 // TODO:
	ReportUploadBillSuccess(payload *types.FilterReportBillUploadSuccessRequest) *typesSDK.SaveRepositoryResponse   // TODO:
	ReportUploadBillSuccessV2(payload *types.FilterReportBillUploadSuccessRequest) *typesSDK.SaveRepositoryResponse // TODO:
	ReportUploadBillComplaint(payload *types.FilterReportBillUploadSuccessRequest) *typesSDK.SaveRepositoryResponse // TODO:
	ReportUploadBillDetail(id string) *typesSDK.SaveRepositoryResponse                                              // TODO:
	ReportUploadBillAll(payload *types.FilterReportBillUploadRequest) *typesSDK.SaveRepositoryResponse
	BillNoAdminCheckDuplicate(billNoAdmin string) bool
	// App
	GetDataByUser(registerID string) *typesSDK.SaveRepositoryResponse
	GetHistory(payload *types.RequestBillHistory) *typesSDK.SaveRepositoryResponse
	ComplaintByUser(payload *types.TableFactBill) *typesSDK.SaveRepositoryResponse   // TODO:
	ComplaintAndReNew(payload *types.TableFactBill) *typesSDK.SaveRepositoryResponse // TODO: ใช้คู่กับตอนส่งใบเสร็จ
}

type billRepository struct {
	MongoDetail    *typesSDK.SecretMongoDetail
	MongoClient    driverMongoSDK.MongoClientType
	DBName         string
	CollectionName string
}

func NewBillRepository() BillRepository {
	return &billRepository{
		MongoDetail:    helper.SecretGetMongoDetail(constants.MongoServerType),
		MongoClient:    driverMongoSDK.MongoClient,
		DBName:         constants.MongoDatabaseName,
		CollectionName: constants.FactBill,
	}
}

func (d *billRepository) ListData(payload *types.FilterBillList) *typesSDK.SaveRepositoryResponse {
	resultsEmpty := make([]*types.TableFactBill, 0)

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	findOptions := options.Find()
	orderBy := -1
	sort := "updated_at"
	if payload.Sort != "" {
		sort = payload.Sort
	}
	if payload.Order == "asc" {
		orderBy = 1
	}
	findOptions.SetSort(&bson.D{{sort, orderBy}})
	findOptions.SetSkip(payload.Offset)
	findOptions.SetLimit(payload.Limit)

	var results []*types.TableFactBill

	filter := bson.D{}
	if payload.Filter.Name != "" {
		filter = append(filter, bson.E{
			Key:   "name",
			Value: bson.D{{"$regex", fmt.Sprintf(".*%s.*", payload.Filter.Name)}},
		})
	}
	cur, err := collection.Find(context.TODO(), filter, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem types.TableFactBill
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Data Not Found",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	_ = cur.Close(context.TODO())

	// Clear Data
	defer func() { results = nil }()

	// Output
	if results == nil {
		results = resultsEmpty
	}
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Rows:       results,
		Total:      0,
	}
}

func (d *billRepository) ShowData(id string) *typesSDK.SaveRepositoryResponse {
	// Valid & Convert ID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
			Data:       nil,
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Data:       nil,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	var result *types.TableFactBill
	filter := &bson.D{{"_id", _id}}
	err = collection.FindOne(context.TODO(), filter).Decode(&result)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Data:       nil,
		}
	}

	// Clear Data
	defer func() { result = nil }()

	// Output
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Data:       result,
	}
}

func (d *billRepository) CreateData(payload *types.TableFactBill) *typesSDK.SaveRepositoryResponse {
	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}

	defer client.Disconnect(context.TODO())

	// Get Register Data
	collection := db.Collection(constants.DimRegister)
	var registerData *types.TableDimRegister
	err = collection.FindOne(context.TODO(), &bson.D{{"_id", payload.UserID}}).Decode(&registerData)

	// Get Register Address
	collection = db.Collection(constants.DimRegisterAddress)
	var vAdd *types.TableDimRegisterAddress
	err = collection.FindOne(context.TODO(), &bson.D{
		{"user_id", payload.UserID},
		{"default", "1"},
	}).Decode(&vAdd)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "No User Data",
		}
	}

	payload.LineUserID = registerData.LineUserID
	payload.LineDisplayName = registerData.LineDisplayName
	payload.LineDisplayImage = registerData.LineDisplayImage
	payload.Birthday = registerData.Birthday
	payload.Sex = registerData.Sex
	payload.Email = registerData.Email
	payload.FirstName = vAdd.FirstName
	payload.LastName = vAdd.LastName
	payload.PhoneNumber = vAdd.PhoneNumber
	payload.Address = vAdd.Address
	payload.Province = vAdd.ProvinceID
	payload.ZipCode = vAdd.ZipCode

	// Execute
	collection = db.Collection(constLine.DimLineUser)
	var lineUserData *typesLine.TableDimLineUser
	err = collection.FindOne(context.TODO(), &bson.D{{"line_user_id", registerData.LineUserID}}).Decode(&lineUserData)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "No Line Data",
			Data:       registerData,
		}
	}

	payload.LineUserID = registerData.LineUserID
	payload.LineDisplayName = lineUserData.LineDisplayName
	payload.LineDisplayImage = lineUserData.LineDisplayImage

	// Execute
	collection = db.Collection(d.CollectionName)
	payload.ID = primitive.NewObjectID()
	payload.StatusID = 2
	payload.StatusTitle = constants.GetStatusTitleBill(2)
	payload.CreatedAt = helperSDK.GetTimeNowGMT()
	payload.UpdatedAt = payload.CreatedAt
	res, err := collection.InsertOne(context.TODO(), payload)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Create Error",
		}
	}

	// Update Duplicate
	// Count Other Bill Duplicate
	totalDup, _ := collection.CountDocuments(context.TODO(), &bson.D{
		{"bill_no", payload.BillNo},
		{"user_id", payload.UserID},
		{"_id", &bson.D{{"$ne", payload.ID}}},
	})
	if totalDup > 0 {
		_, _ = collection.UpdateMany(context.TODO(),
			&bson.D{
				{"bill_no", payload.BillNo},
				{"user_id", payload.UserID},
			}, &bson.D{{
				"$set", &bson.D{
					{"is_duplicate", 1},
					//{"updated_at", payload.CreatedAt},
				},
			}})
		_, _ = collection.UpdateMany(context.TODO(),
			&bson.D{
				{"bill_no_admin", payload.BillNo},
				{"user_id", payload.UserID},
			}, &bson.D{{
				"$set", &bson.D{
					{"is_duplicate", 1},
					//{"updated_at", payload.CreatedAt},
				},
			}})
	}

	// Clear Data
	defer func() {
		registerData = nil
		vAdd = nil
	}()

	// Output
	return &typesSDK.SaveRepositoryResponse{
		ID:         res.InsertedID,
		CodeReturn: 1,
		Message:    "Create Success",
	}
}

func (d *billRepository) UpdateData(id string, payload *types.TableFactBill) *typesSDK.SaveRepositoryResponse {
	// Valid & Convert ID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	filter := &bson.D{{"_id", _id}}
	s := bson.D{
		{"by_admin_no", payload.ByAdminNo},
		{"bill_summary", payload.BillSummary},
		{"bill_no_admin", payload.BillNoAdmin},
		{"bill_no", payload.BillNo},
		{"point", payload.Point},
		{"status_id", payload.StatusID},
		{"status_title", constants.GetStatusTitleBill(payload.StatusID)},

		{"bill_date_by_admin", payload.BillDateByAdmin},
		{"shop_by_admin", payload.ShopByAdmin},
		{"bill_product", payload.BillProduct},
		{"dutchie_bill_summary", payload.DutchieBillSummary},

		{"is_checked", payload.IsChecked},

		{"regular_2_price", payload.Regular2Price},
		{"regular_2_amount", payload.Regular2Amount},
		{"regular_2_pack", payload.Regular2Pack},
		{"regular_2_name", payload.Regular2Name},
		{"regular_1_price", payload.Regular1Price},
		{"regular_1_amount", payload.Regular1Amount},
		{"regular_1_pack", payload.Regular1Pack},
		{"regular_1_name", payload.Regular1Name},

		{"df_2_price", payload.Df2Price},
		{"df_2_amount", payload.Df2Amount},
		{"df_2_pack", payload.Df2Pack},
		{"df_2_name", payload.Df2Name},
		{"df_1_price", payload.Df1Price},
		{"df_1_amount", payload.Df1Amount},
		{"df_1_pack", payload.Df1Pack},
		{"df_1_name", payload.Df1Name},

		{"bio_2_price", payload.Bio2Price},
		{"bio_2_amount", payload.Bio2Amount},
		{"bio_2_pack", payload.Bio2Pack},
		{"bio_2_name", payload.Bio2Name},
		{"bio_1_price", payload.Bio1Price},
		{"bio_1_amount", payload.Bio1Amount},
		{"bio_1_pack", payload.Bio1Pack},
		{"bio_1_name", payload.Bio1Name},

		{"greek_2_price", payload.Greek2Price},
		{"greek_2_amount", payload.Greek2Amount},
		{"greek_2_pack", payload.Greek2Pack},
		{"greek_2_name", payload.Greek2Name},
		{"greek_1_price", payload.Greek1Price},
		{"greek_1_amount", payload.Greek1Amount},
		{"greek_1_pack", payload.Greek1Pack},
		{"greek_1_name", payload.Greek1Name},

		{"kids_2_price", payload.Kids2Price},
		{"kids_2_amount", payload.Kids2Amount},
		{"kids_2_pack", payload.Kids2Pack},
		{"kids_2_name", payload.Kids2Name},
		{"kids_1_price", payload.Kids1Price},
		{"kids_1_amount", payload.Kids1Amount},
		{"kids_1_pack", payload.Kids1Pack},
		{"kids_1_name", payload.Kids1Name},

		{"brand_1_price", payload.Brand1Price},
		{"brand_1_amount", payload.Brand1Amount},
		{"brand_1_pack", payload.Brand1Pack},
		{"brand_1_name", payload.Brand1Name},
	}
	if payload.IsComplaint != 1 {
		s = append(s, bson.E{Key: "updated_at", Value: helperSDK.GetTimeNowGMT()})
	} else {
		s = append(s, bson.E{Key: "complaint_at", Value: helperSDK.GetTimeNowGMT()})
	}
	update := &bson.D{{"$set", s}}
	res, err := collection.UpdateOne(context.TODO(), filter, update)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			ID:         _id,
			CodeReturn: -1,
			Message:    "Update Error",
		}
	}

	// Output Error
	if res.ModifiedCount <= 0 {
		return &typesSDK.SaveRepositoryResponse{
			ID:         _id,
			CodeReturn: 2,
			Message:    "Data Not Found",
		}
	}

	// Update Duplicate
	if payload.StatusID == 4 {
		res, _ = collection.UpdateMany(context.TODO(),
			&bson.D{
				{"bill_no", payload.BillNo},
				{"user_id", payload.UserID},
			}, &bson.D{{
				"$set", &bson.D{
					{"is_duplicate", 1},
					//{"updated_at", helperSDK.GetTimeNowGMT()},
				},
			}})
		res, _ = collection.UpdateMany(context.TODO(),
			&bson.D{
				{"bill_no_admin", payload.BillNoAdmin},
				{"user_id", payload.UserID},
			}, &bson.D{{
				"$set", &bson.D{
					{"is_duplicate", 1},
					//{"updated_at", helperSDK.GetTimeNowGMT()},
				},
			}})
	}

	// Output
	return &typesSDK.SaveRepositoryResponse{
		ID:         _id,
		CodeReturn: 1,
		Message:    "Update Success",
	}
}

func (d *billRepository) DeleteData(id string) *typesSDK.SaveRepositoryResponse {
	// Valid & Convert ID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	filter := bson.D{{"_id", _id}}
	res, err := collection.DeleteOne(context.TODO(), filter)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			ID:         _id,
			CodeReturn: -1,
			Message:    "Delete Error",
		}
	}

	// Output Error
	if res.DeletedCount <= 0 {
		return &typesSDK.SaveRepositoryResponse{
			ID:         _id,
			CodeReturn: 2,
			Message:    "Data Not Found",
		}
	}

	// Output Error
	return &typesSDK.SaveRepositoryResponse{
		ID:         _id,
		CodeReturn: 1,
		Message:    "Delete Success",
	}
}

func (d *billRepository) GetDataByUser(registerID string) *typesSDK.SaveRepositoryResponse {
	resultsEmpty := make([]*types.TableFactBill, 0)

	// Valid & Convert ID
	_registerID, err := primitive.ObjectIDFromHex(registerID)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
			Rows:       resultsEmpty,
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Rows:       resultsEmpty,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	findOptions := options.Find()

	var results []*types.TableFactBill

	filter := &bson.D{{"user_id", _registerID}}
	cur, err := collection.Find(context.TODO(), filter, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Rows:       resultsEmpty,
		}
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem types.TableFactBill
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Data Not Found",
			Rows:       resultsEmpty,
		}
	}

	_ = cur.Close(context.TODO())

	// Clear Data
	defer func() { results = nil }()

	// Output
	if results == nil {
		results = resultsEmpty
	}
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Rows:       results,
	}
}

func (d *billRepository) GetHistory(payload *types.RequestBillHistory) *typesSDK.SaveRepositoryResponse {
	resultsEmpty := make([]*types.ResponseAppBillDataByFilter, 0)

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Rows:       resultsEmpty,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	findOptions := options.Find()
	findOptions.SetSort(bson.D{{"updated_at", -1}})
	findOptions.SetProjection(&bson.M{
		"bill_no":      1,
		"img_url":      1,
		"is_used":      1,
		"point":        1,
		"status_id":    1,
		"status_title": 1,
		"created_at":   1,
	})

	var results []*types.ResponseAppBillDataByFilter

	filter := bson.D{{"user_id", payload.UserID}}
	if !payload.StartDate.IsZero() && !payload.EndDate.IsZero() {
		filter = append(filter, bson.E{
			Key: "created_at",
			Value: bson.M{
				"$gte": payload.StartDate,
				"$lte": payload.EndDate,
			}})
	}
	if payload.StatusID != -1 {
		if payload.StatusID == 1 {
			// Success All
			filter = append(filter, bson.E{
				Key:   "status_id",
				Value: 1,
			})
		} else {
			// Fail All
			filter = append(filter, bson.E{
				Key:   "status_id",
				Value: bson.D{{"$ne", 1}},
			})
		}
	}
	cur, err := collection.Find(context.TODO(), filter, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Rows:       resultsEmpty,
		}
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem types.ResponseAppBillDataByFilter

		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		elem.DateGroup = elem.CreatedAt.Format("2006-01-02")
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Data Not Found",
			Rows:       resultsEmpty,
		}
	}

	_ = cur.Close(context.TODO())

	// Clear Data
	defer func() { results = nil }()

	// Output
	if results == nil {
		results = resultsEmpty
	}

	allGroup := make([]string, 0)
	for _, v := range results {
		isFound := false
		for i := range allGroup {
			if allGroup[i] == v.DateGroup {
				isFound = true
			}
		}
		if !isFound {
			allGroup = append(allGroup, v.DateGroup)
		}
	}

	s0 := make([]interface{}, 0)
	for i := range allGroup {
		s2 := make([]*types.ResponseAppBillDataByFilter, 0)
		for _, v := range results {
			if v.DateGroup == allGroup[i] {
				s2 = append(s2, v)
			}
		}
		s0 = append(s0, map[string]interface{}{
			allGroup[i]: s2,
		})
	}

	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "",
		Rows:       s0,
	}
}

func (d *billRepository) ReportUploadBill(payload *types.FilterReportBillUploadRequest) *typesSDK.SaveRepositoryResponse {
	resultsEmpty := make([]*types.TableFactBill, 0)
	var results []*types.TableFactBill
	var total int64
	filter := bson.D{}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	findOptions := options.Find()
	// No Projection When Export
	if !payload.IsExport {
		findOptions.SetProjection(&bson.M{
			"line_user_id":      1,
			"line_display_name": 1,
			"first_name":        1,
			"last_name":         1,
			"phone_number":      1,
			"img_url":           1,
			"bill_no":           1,
			"bill_no_admin":     1,
			"status_id":         1,
			"is_duplicate":      1,
			"created_at":        1,
			"user_id":           1,
			"shop_type":         1,
			"shop_type_2":       1,
		})
	}
	orderBy := 1
	sort := "updated_at"
	if payload.Sort != "" {
		sort = payload.Sort
	}
	if payload.Order == "desc" {
		orderBy = -1
	}
	findOptions.SetSort(&bson.D{{sort, orderBy}})

	// No Limit When Export
	if !payload.IsExport {
		findOptions.SetSkip(payload.Offset)
		findOptions.SetLimit(payload.Limit)
	}

	filter = append(filter, bson.E{Key: "status_id", Value: 2})
	filter = append(filter, bson.E{Key: "is_complaint", Value: &bson.D{{"$ne", 1}}})
	if payload.Filter.BillNo != "" {
		filter = append(filter, bson.E{
			Key:   "bill_no",
			Value: bson.D{{"$regex", fmt.Sprintf(".*%s.*", payload.Filter.BillNo)}},
		})
	}
	if payload.Filter.PhoneNumber != "" {
		filter = append(filter, bson.E{
			Key:   "phone_number",
			Value: bson.D{{"$regex", fmt.Sprintf(".*%s.*", payload.Filter.PhoneNumber)}},
		})
	}
	if payload.Filter.ShopType != "" {
		filter = append(filter, bson.E{
			Key:   "shop_type",
			Value: payload.Filter.ShopType,
		})
	}
	if payload.Filter.ShopType2 != "" {
		filter = append(filter, bson.E{
			Key:   "shop_type_2",
			Value: payload.Filter.ShopType2,
		})
	}
	if !payload.Filter.StartDate.IsZero() && !payload.Filter.EndDate.IsZero() {
		filter = append(filter, bson.E{
			Key: "created_at",
			Value: bson.M{
				"$gte": payload.Filter.StartDate,
				"$lte": payload.Filter.EndDate,
			}})
	}

	cur, err := collection.Find(context.TODO(), filter, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem types.TableFactBill
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		//elem["id"] = elem["_id"]
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Data Not Found",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	_ = cur.Close(context.TODO())

	// No Total
	if !payload.IsExport {
		total, _ = collection.CountDocuments(context.TODO(), filter)
	}

	// Clear Data
	defer func() {
		results = nil
		total = 0
	}()

	// Output
	if results == nil {
		results = resultsEmpty
	}
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Rows:       results,
		Total:      int32(total),
	}
}

func (d *billRepository) ReportUploadBillSuccess(payload *types.FilterReportBillUploadSuccessRequest) *typesSDK.SaveRepositoryResponse {
	resultsEmpty := make([]*types.TableFactBill, 0)
	var results []*types.TableFactBill
	var total int64
	filter := bson.D{}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	findOptions := options.Find()
	// No Projection When Export
	if !payload.IsExport {
		findOptions.SetProjection(&bson.M{
			"line_user_id":      1,
			"line_display_name": 1,
			"first_name":        1,
			"last_name":         1,
			"phone_number":      1,
			"img_url":           1,
			"bill_no":           1,
			"bill_no_admin":     1,
			"status_id":         1,
			"status_title":      1,
			"is_duplicate":      1,
			"created_at":        1,
			"user_id":           1,
			"shop_type":         1,
			"shop_type_2":       1,
			"point":             1,
			"bill_summary":      1,
			"updated_at":        1,
		})
	}
	orderBy := 1
	sort := "updated_at"
	if payload.Sort != "" {
		sort = payload.Sort
	}
	if payload.Order == "desc" {
		orderBy = -1
	}
	findOptions.SetSort(&bson.D{{sort, orderBy}})

	// No Limit When Export
	if !payload.IsExport {
		findOptions.SetSkip(payload.Offset)
		findOptions.SetLimit(payload.Limit)
	}

	if payload.Filter.BillNo != "" {
		filter = append(filter, bson.E{
			Key:   "bill_no",
			Value: bson.D{{"$regex", fmt.Sprintf(".*%s.*", payload.Filter.BillNo)}},
		})
	}
	if payload.Filter.PhoneNumber != "" {
		filter = append(filter, bson.E{
			Key:   "phone_number",
			Value: bson.D{{"$regex", fmt.Sprintf(".*%s.*", payload.Filter.PhoneNumber)}},
		})
	}
	if payload.Filter.ShopType != "" {
		filter = append(filter, bson.E{
			Key:   "shop_type",
			Value: payload.Filter.ShopType,
		})
	}
	if payload.Filter.ShopType2 != "" {
		filter = append(filter, bson.E{
			Key:   "shop_type_2",
			Value: payload.Filter.ShopType2,
		})
	}
	if !payload.Filter.StartDate.IsZero() && !payload.Filter.EndDate.IsZero() {
		filter = append(filter, bson.E{
			Key: "updated_at",
			Value: bson.M{
				"$gte": payload.Filter.StartDate,
				"$lte": payload.Filter.EndDate,
			}})
	}
	if payload.Filter.StatusID != -1 {
		filter = append(filter, bson.E{
			Key:   "status_id",
			Value: payload.Filter.StatusID,
		})
	} else {
		filter = append(filter, bson.E{Key: "status_id", Value: bson.D{{"$ne", 2}}})
	}

	cur, err := collection.Find(context.TODO(), filter, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem types.TableFactBill
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		//elem["id"] = elem["_id"]
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Data Not Found",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	_ = cur.Close(context.TODO())

	// No Total
	if !payload.IsExport {
		total, _ = collection.CountDocuments(context.TODO(), filter)
	}

	// Clear Data
	defer func() {
		results = nil
		total = 0
	}()

	// Output
	if results == nil {
		results = resultsEmpty
	}
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Rows:       results,
		Total:      int32(total),
	}
}

func (d *billRepository) ReportUploadBillSuccessV2(payload *types.FilterReportBillUploadSuccessRequest) *typesSDK.SaveRepositoryResponse {
	resultsEmpty := make([]*types.TableFactBill, 0)
	var results []*types.TableFactBill
	var total int64
	filter := bson.D{}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	findOptions := options.Find()
	// No Projection When Export
	if !payload.IsExport {
		findOptions.SetProjection(&bson.M{
			"line_user_id":      1,
			"line_display_name": 1,
			"first_name":        1,
			"last_name":         1,
			"phone_number":      1,
			"img_url":           1,
			"bill_no":           1,
			"bill_no_admin":     1,
			"status_id":         1,
			"status_title":      1,
			"is_duplicate":      1,
			"created_at":        1,
			"user_id":           1,
			"shop_type":         1,
			"shop_type_2":       1,
			"point":             1,
			"bill_summary":      1,
			"updated_at":        1,
		})
	}
	orderBy := 1
	sort := "updated_at"
	if payload.Sort != "" {
		sort = payload.Sort
	}
	if payload.Order == "desc" {
		orderBy = -1
	}
	findOptions.SetSort(&bson.D{{sort, orderBy}})

	// No Limit When Export
	if !payload.IsExport {
		findOptions.SetSkip(payload.Offset)
		findOptions.SetLimit(payload.Limit)
	}

	if payload.Filter.BillNo != "" {
		filter = append(filter, bson.E{
			Key:   "bill_no",
			Value: bson.D{{"$regex", fmt.Sprintf(".*%s.*", payload.Filter.BillNo)}},
		})
	}
	if payload.Filter.PhoneNumber != "" {
		filter = append(filter, bson.E{
			Key:   "phone_number",
			Value: bson.D{{"$regex", fmt.Sprintf(".*%s.*", payload.Filter.PhoneNumber)}},
		})
	}
	if payload.Filter.ShopType != "" {
		filter = append(filter, bson.E{
			Key:   "shop_type",
			Value: payload.Filter.ShopType,
		})
	}
	if payload.Filter.ShopType2 != "" {
		filter = append(filter, bson.E{
			Key:   "shop_type_2",
			Value: payload.Filter.ShopType2,
		})
	}
	if !payload.Filter.StartDate.IsZero() && !payload.Filter.EndDate.IsZero() {
		filter = append(filter, bson.E{
			Key: "created_at",
			Value: bson.M{
				"$gte": payload.Filter.StartDate,
				"$lte": payload.Filter.EndDate,
			}})
	}
	if payload.Filter.StatusID != -1 {
		filter = append(filter, bson.E{
			Key:   "status_id",
			Value: payload.Filter.StatusID,
		})
	} else {
		filter = append(filter, bson.E{Key: "status_id", Value: bson.D{{"$ne", 2}}})
	}

	cur, err := collection.Find(context.TODO(), filter, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem types.TableFactBill
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		//elem["id"] = elem["_id"]
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Data Not Found",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	_ = cur.Close(context.TODO())

	// No Total
	if !payload.IsExport {
		total, _ = collection.CountDocuments(context.TODO(), filter)
	}

	// Clear Data
	defer func() {
		results = nil
		total = 0
	}()

	// Output
	if results == nil {
		results = resultsEmpty
	}
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Rows:       results,
		Total:      int32(total),
	}
}

func (d *billRepository) ReportUploadBillAll(payload *types.FilterReportBillUploadRequest) *typesSDK.SaveRepositoryResponse {
	resultsEmpty := make([]*map[string]interface{}, 0)

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	findOptions := options.Find()
	findOptions.SetProjection(&bson.M{
		"first_name":    1,
		"last_name":     1,
		"phone_number":  1,
		"img_url":       1,
		"bill_no":       1,
		"bill_no_admin": 1,
		"status_id":     1,
		"is_duplicate":  1,
		"created_at":    1,
		"user_id":       1,
		"shop_type":     1,
		"shop_type_2":   1,
	})
	orderBy := 1
	sort := "updated_at"
	if payload.Sort != "" {
		sort = payload.Sort
	}
	if payload.Order == "desc" {
		orderBy = -1
	}
	findOptions.SetSort(&bson.D{{sort, orderBy}})
	findOptions.SetSkip(payload.Offset)
	findOptions.SetLimit(payload.Limit)

	var results []*map[string]interface{}

	filter := bson.D{}
	if payload.Filter.BillNo != "" {
		filter = append(filter, bson.E{
			Key:   "bill_no",
			Value: bson.D{{"$regex", fmt.Sprintf(".*%s.*", payload.Filter.BillNo)}},
		})
	}
	cur, err := collection.Find(context.TODO(), filter, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem map[string]interface{}
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Data Not Found",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	_ = cur.Close(context.TODO())

	total, _ := collection.CountDocuments(context.TODO(), filter)

	// Clear Data
	defer func() {
		results = nil
		total = 0
	}()

	// Output
	if results == nil {
		results = resultsEmpty
	}
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Rows:       results,
		Total:      int32(total),
	}
}

func (d *billRepository) ReportBillCustomer(payload *types.FilterReportRegisterRequest) *typesSDK.SaveRepositoryResponse {
	resultsEmpty := make([]*types.ResponseReportBillCustomer, 0)
	var results []*types.ResponseReportBillCustomer
	var results2 []*types.AggregateCount
	var total int32

	orderBy := 1
	sort := "updated_at"
	if payload.Sort != "" {
		sort = payload.Sort
	}
	if payload.Order == "desc" {
		orderBy = -1
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	filter := bson.D{}
	if payload.Filter.PhoneNumber != "" {
		filter = append(filter, bson.E{
			Key:   "phone_number",
			Value: bson.D{{"$regex", fmt.Sprintf(".*%s.*", payload.Filter.PhoneNumber)}},
		})
	}
	if !payload.Filter.StartDate.IsZero() && !payload.Filter.EndDate.IsZero() {
		filter = append(filter, bson.E{
			Key: "updated_at",
			Value: bson.M{
				"$gte": payload.Filter.StartDate,
				"$lte": payload.Filter.EndDate,
			}})
	}
	matchStage := bson.D{{"$match", filter}}
	var groupStage bson.D
	if payload.IsExport {
		groupStage = bson.D{{"$group", &bson.D{
			{"_id", &bson.D{
				{"line_user_id", "$line_user_id"},
				{"line_display_name", "$line_display_name"},
				{"user_id", "$user_id"},
				{"first_name", "$first_name"},
				{"last_name", "$last_name"},
				{"email", "$email"},
				{"phone_number", "$phone_number"},
				{"sex", "$sex"},
				{"birthday", "$birthday"},
				{"address", "$address"},
				{"province_id", "$province_id"},
				{"zip_code", "$zip_code"},
			}},
			{"total_point", &bson.D{{"$sum", "$point"}}},
			{"current_point", &bson.D{{"$max", "$current_score"}}},
			{"bill_summary", &bson.D{{"$sum", "$bill_summary"}}},
			{"total_bill", &bson.D{{"$sum", 1}}},

			{"dutchie_bill_summary", &bson.D{{"$sum", "$dutchie_bill_summary"}}},
			{"regular_1_amount", &bson.D{{"$sum", "$regular_1_amount"}}},
			{"regular_1_price", &bson.D{{"$sum", "$regular_1_price"}}},
			{"regular_2_amount", &bson.D{{"$sum", "$regular_2_amount"}}},
			{"regular_2_price", &bson.D{{"$sum", "$regular_2_price"}}},
			{"df_1_amount", &bson.D{{"$sum", "$df_1_amount"}}},
			{"df_1_price", &bson.D{{"$sum", "$df_1_price"}}},
			{"df_2_amount", &bson.D{{"$sum", "$df_2_amount"}}},
			{"df_2_price", &bson.D{{"$sum", "$df_2_price"}}},
			{"bio_1_amount", &bson.D{{"$sum", "$bio_1_amount"}}},
			{"bio_1_price", &bson.D{{"$sum", "$bio_1_price"}}},
			{"bio_2_amount", &bson.D{{"$sum", "$bio_2_amount"}}},
			{"bio_2_price", &bson.D{{"$sum", "$bio_2_price"}}},
			{"greek_1_amount", &bson.D{{"$sum", "$greek_1_amount"}}},
			{"greek_1_price", &bson.D{{"$sum", "$greek_1_price"}}},
			{"greek_2_amount", &bson.D{{"$sum", "$greek_2_amount"}}},
			{"greek_2_price", &bson.D{{"$sum", "$greek_2_price"}}},
			{"kids_1_amount", &bson.D{{"$sum", "$kids_1_amount"}}},
			{"kids_1_price", &bson.D{{"$sum", "$kids_1_price"}}},
			{"kids_2_amount", &bson.D{{"$sum", "$kids_2_amount"}}},
			{"kids_2_price", &bson.D{{"$sum", "$kids_2_price"}}},
			{"brand_1_amount", &bson.D{{"$sum", "$brand_1_amount"}}},
			{"brand_1_price", &bson.D{{"$sum", "$brand_1_price"}}},
		}}}
	} else {
		groupStage = bson.D{{"$group", &bson.D{
			{"_id", &bson.D{
				{"line_user_id", "$line_user_id"},
				{"line_display_name", "$line_display_name"},
				{"user_id", "$user_id"},
				{"first_name", "$first_name"},
				{"last_name", "$last_name"},
				{"email", "$email"},
				{"phone_number", "$phone_number"},
			}},
			{"total_point", &bson.D{{"$sum", "$point"}}},
			{"current_point", &bson.D{{"$max", "$current_score"}}},
			{"bill_summary", &bson.D{{"$sum", "$dutchie_bill_summary"}}},
			{"total_bill", &bson.D{{"$sum", 1}}},
			{"updated_at", &bson.D{{"$max", "$updated_at"}}},
		}}}
	}

	// ================================================================ //
	// Pagination
	// ================================================================ //
	var vPagination []*types.TableFactBill

	curPage, _ := collection.Aggregate(context.TODO(),
		mongo.Pipeline{
			matchStage,
			bson.D{{"$sort", &bson.D{
				{sort, orderBy},
			}}},
			bson.D{{"$group", &bson.D{
				{"_id", "$user_id"},
			},
			}},
			bson.D{{"$skip", payload.Offset}},
			bson.D{{"$limit", payload.Limit}},
		},
	)

	if err = curPage.All(context.TODO(), &vPagination); err != nil {
		fmt.Println(err)
	}

	_ = curPage.Close(context.TODO())

	var arr []primitive.ObjectID

	for _, v := range vPagination {
		arr = append(arr, v.ID)
	}

	// ================================================================ //

	cur, err := collection.Aggregate(context.TODO(),
		mongo.Pipeline{
			bson.D{{"$match", &bson.D{
				{"user_id", bson.M{"$in": arr}},
			}}},
			matchStage,
			bson.D{{"$lookup", &bson.D{
				{"from", "dim_register_point"},
				{"localField", "user_id"},
				{"foreignField", "register_id"},
				{"as", "register_point"},
			}}},
			bson.D{{"$unwind", "$register_point"}},
			bson.D{{"$addFields", &bson.D{
				{"total_redeem", &bson.D{
					{"$add", &bson.A{"$register_point.point_lucky_draw", "$register_point.point_premium"}},
				}},
			}}},
			bson.D{{"$addFields", &bson.D{
				{"current_score", &bson.D{
					{"$subtract", &bson.A{"$register_point.point", "$total_redeem"}},
				}},
			}}},
			groupStage,
			bson.D{{"$sort", &bson.D{{sort, orderBy}}}},
		},
	)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	// Assign Data Array
	if err = cur.All(context.TODO(), &results); err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	cur2, _ := collection.Aggregate(context.TODO(),
		mongo.Pipeline{
			//matchStage,
			//groupStage,
			bson.D{{"$sort", &bson.D{
				{sort, orderBy},
			}}},
			bson.D{{"$group", &bson.D{
				{"_id", "$user_id"},
			},
			}},
			bson.D{{"$count", "count"}},
		},
	)

	if err = cur2.All(context.TODO(), &results2); err != nil {
		fmt.Println(err)
	}

	_ = cur.Close(context.TODO())
	_ = cur2.Close(context.TODO())

	// Clear Data
	defer func() {
		results = nil
		results2 = nil
		total = 0
		vPagination = nil
		arr = nil
	}()

	// Output
	if results == nil {
		results = resultsEmpty
	}
	if results2 != nil {
		total = results2[0].Count
	}
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Rows:       results,
		Total:      total,
	}
}

func (d *billRepository) ReportUploadBillComplaint(payload *types.FilterReportBillUploadSuccessRequest) *typesSDK.SaveRepositoryResponse {
	resultsEmpty := make([]*types.TableFactBill, 0)
	var results []*types.TableFactBill
	var total int64
	filter := bson.D{}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	findOptions := options.Find()
	// No Projection When Export
	if !payload.IsExport {
		findOptions.SetProjection(&bson.M{
			"line_user_id":      1,
			"line_display_name": 1,
			"first_name":        1,
			"last_name":         1,
			"phone_number":      1,
			"img_url":           1,
			"bill_no":           1,
			"bill_no_admin":     1,
			"status_id":         1,
			"status_title":      1,
			"is_duplicate":      1,
			"created_at":        1,
			"user_id":           1,
			"shop_type":         1,
			"shop_type_2":       1,
			"point":             1,
			"bill_summary":      1,
			"updated_at":        1,
			"previous_point":    1,
			"previous_status":   1,
		})
	}
	orderBy := 1
	sort := "updated_at"
	if payload.Sort != "" {
		sort = payload.Sort
	}
	if payload.Order == "desc" {
		orderBy = -1
	}
	findOptions.SetSort(&bson.D{{sort, orderBy}})

	// No Limit When Export
	if !payload.IsExport {
		findOptions.SetSkip(payload.Offset)
		findOptions.SetLimit(payload.Limit)
	}

	filter = append(filter, bson.E{Key: "is_complaint", Value: 1})
	filter = append(filter, bson.E{Key: "is_checked", Value: 0})
	if payload.Filter.BillNo != "" {
		filter = append(filter, bson.E{
			Key:   "bill_no",
			Value: bson.D{{"$regex", fmt.Sprintf(".*%s.*", payload.Filter.BillNo)}},
		})
	}
	if payload.Filter.PhoneNumber != "" {
		filter = append(filter, bson.E{
			Key:   "phone_number",
			Value: bson.D{{"$regex", fmt.Sprintf(".*%s.*", payload.Filter.PhoneNumber)}},
		})
	}
	if payload.Filter.ShopType != "" {
		filter = append(filter, bson.E{
			Key:   "shop_type",
			Value: payload.Filter.ShopType,
		})
	}
	if payload.Filter.ShopType2 != "" {
		filter = append(filter, bson.E{
			Key:   "shop_type_2",
			Value: payload.Filter.ShopType2,
		})
	}
	if !payload.Filter.StartDate.IsZero() && !payload.Filter.EndDate.IsZero() {
		filter = append(filter, bson.E{
			Key: "updated_at",
			Value: bson.M{
				"$gte": payload.Filter.StartDate,
				"$lte": payload.Filter.EndDate,
			}})
	}
	if payload.Filter.StatusID != -1 {
		filter = append(filter, bson.E{
			Key:   "status_id",
			Value: payload.Filter.StatusID,
		})
	}

	cur, err := collection.Find(context.TODO(), filter, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem types.TableFactBill
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		//elem["id"] = elem["_id"]
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Data Not Found",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	_ = cur.Close(context.TODO())

	// No Total
	if !payload.IsExport {
		total, _ = collection.CountDocuments(context.TODO(), filter)
	}

	// Clear Data
	defer func() {
		results = nil
		total = 0
	}()

	// Output
	if results == nil {
		results = resultsEmpty
	}
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Rows:       results,
		Total:      int32(total),
	}
}

func (d *billRepository) ReportUploadBillDetail(id string) *typesSDK.SaveRepositoryResponse {
	data := map[string]interface{}{
		"bill_detail":    &types.TableFactBill{},
		"bill_duplicate": make([]*types.TableFactBill, 0),
		"user_bill_list": make([]interface{}, 0),
	}

	// Valid & Convert ID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
			Data:       data,
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Data:       data,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	var result *types.TableFactBill
	filter := &bson.D{{"_id", _id}}
	err = collection.FindOne(context.TODO(), filter).Decode(&result)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Data:       data,
		}
	}

	if result.BillProduct == nil {
		result.BillProduct = make([]interface{}, 0)
	}
	data["bill_detail"] = result

	findOptions := options.Find()
	findOptions.SetProjection(&bson.M{})
	findOptions.SetSort(&bson.D{{"updated_at", 1}})

	var billDuplicate []*types.TableFactBill
	var filter2 *interface{}
	if result.BillNoAdmin != "" {
		_ = bson.UnmarshalExtJSON([]byte(fmt.Sprintf(`{
		  "$or": [
			{
			  "$or": [ { "bill_no": "%s" } ]
			},
			{
			  "$or": [ { "bill_no_admin": "%s" } ]
			}
		  ] }`, result.BillNo, result.BillNoAdmin)), true, &filter2)
	} else {
		_ = bson.UnmarshalExtJSON([]byte(fmt.Sprintf(`{
		  "$or": [
			{
			  "$or": [ { "bill_no": "%s" } ]
			},
			{
			  "$or": [ { "bill_no_admin": "%s" } ]
			}
		  ] }`, result.BillNo, result.BillNo)), true, &filter2)
	}
	cur, _ := collection.Find(context.TODO(), filter2, findOptions)

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem types.TableFactBill
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		billDuplicate = append(billDuplicate, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
	}

	_ = cur.Close(context.TODO())

	if billDuplicate != nil {
		data["bill_duplicate"] = billDuplicate
	}

	// Clear Data
	defer func() {
		cur = nil
		data = nil
		result = nil
		billDuplicate = nil
		filter2 = nil
	}()

	// Output
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Data:       data,
	}
}

func (d *billRepository) ComplaintByUser(payload *types.TableFactBill) *typesSDK.SaveRepositoryResponse {
	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)

	var prevData *types.TableFactBill
	filter := &bson.D{{"_id", payload.ID}}
	err = collection.FindOne(context.TODO(), filter).Decode(&prevData)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
		}
	}

	// Clear Data
	defer func() { prevData = nil }()

	filter = &bson.D{{"_id", payload.ID}}
	s := &bson.D{
		{"point", 0},
		{"status_id", 2},
		{"status_title", constants.GetStatusTitleBill(2)},

		{"is_used", 1},
		{"is_complaint", 1},
		{"previous_point", prevData.Point},
		{"previous_status", prevData.StatusID},
		{"previous_status_title", prevData.StatusTitle},

		//{"updated_at", helperSDK.GetTimeNowGMT()},
	}
	update := &bson.D{{"$set", s}}
	res, err := collection.UpdateOne(context.TODO(), filter, update)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Update Error",
		}
	}

	// Output Error
	if res.ModifiedCount <= 0 {
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Data Not Found",
		}
	}

	// Output
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Update Success",
	}
}

func (d *billRepository) ComplaintAndReNew(payload *types.TableFactBill) *typesSDK.SaveRepositoryResponse {
	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)

	var prevData *types.TableFactBill
	filter := &bson.D{{"_id", payload.ID}}
	err = collection.FindOne(context.TODO(), filter).Decode(&prevData)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
		}
	}

	// Clear Data
	defer func() { prevData = nil }()

	filter = &bson.D{{"_id", payload.ID}}
	s := &bson.D{
		{"bill_no", payload.BillNo},
		{"point", 0},
		{"status_id", 2},
		{"status_title", constants.GetStatusTitleBill(2)},
		{"shop_type", payload.ShopType},
		{"shop_type_2", payload.ShopType2},
		{"img_url", payload.ImgUrl},

		{"is_used", 1},
		{"is_complaint", 1},
		{"previous_point", prevData.Point},
		{"previous_status", prevData.StatusID},
		{"previous_status_title", prevData.StatusTitle},

		//{"updated_at", helperSDK.GetTimeNowGMT()},
	}
	update := &bson.D{{"$set", s}}
	res, err := collection.UpdateOne(context.TODO(), filter, update)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Update Error",
		}
	}

	// Output Error
	if res.ModifiedCount <= 0 {
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Data Not Found",
		}
	}

	// Output
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Update Success xxx",
	}
}

func (d *billRepository) BillNoAdminCheckDuplicate(billNoAdmin string) bool {
	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return false
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	var result *types.TableFactBill
	filter := &bson.D{{"bill_no_admin", billNoAdmin}}
	err = collection.FindOne(context.TODO(), filter).Decode(&result)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return false
	}

	// Clear Data
	defer func() { result = nil }()

	if result != nil {
		return true
	} else {
		return false
	}
}
