package repository

import (
	"context"
	"fmt"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/helper"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchie_v2/constants"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchie_v2/types"
	driverMongoSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/drivers/mongodb"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
	"go.mongodb.org/mongo-driver/bson"
)

type SettingRepository interface {
	SaveData(payload *types.TableSettingMain) *typesSDK.SaveRepositoryResponse
	ShowData() *typesSDK.SaveRepositoryResponse
}

type settingRepository struct {
	MongoDetail *typesSDK.SecretMongoDetail
	MongoClient driverMongoSDK.MongoClientType
	DBName      string
}

func NewSettingRepository() SettingRepository {
	return &settingRepository{
		MongoDetail: helper.SecretGetMongoDetail(constants.MongoServerType),
		MongoClient: driverMongoSDK.MongoClient,
		DBName:      constants.MongoDatabaseName,
	}
}

func (d *settingRepository) SaveData(payload *types.TableSettingMain) *typesSDK.SaveRepositoryResponse {
	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}

	defer client.Disconnect(context.TODO())

	collection := db.Collection(constants.DimSetting)
	count, err := collection.CountDocuments(context.TODO(), &bson.D{})

	if err != nil {
		fmt.Println(err)
		// Output Error
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Save Error",
		}
	}

	// Has Old Data
	if count > 0 {
		// Delete All Data
		_, err = collection.DeleteMany(context.TODO(), &bson.D{})

		// Delete Error
		if err != nil {
			fmt.Println(err)
		}
	}

	_, err = collection.InsertOne(context.TODO(), payload)

	// Insert Error
	if err != nil {
		fmt.Println(err)
		// Output Error
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Save Error",
		}
	}

	// Output Success
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Save Success",
	}
}

func (d *settingRepository) ShowData() *typesSDK.SaveRepositoryResponse {
	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}

	defer client.Disconnect(context.TODO())

	collection := db.Collection(constants.DimSetting)

	var result *types.TableSettingMain
	err = collection.FindOne(context.TODO(), &bson.D{}).Decode(&result)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Data:       nil,
		}
	}

	// Clear Data
	defer func() { result = nil }()

	// Output
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Data:       result,
	}
}
