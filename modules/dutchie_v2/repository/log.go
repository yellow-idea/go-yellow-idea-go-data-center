package repository

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/helper"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchie_v2/constants"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchie_v2/types"
	driverMongoSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/drivers/mongodb"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type LogRepository interface {
	SavePushMessageBill(billId string, msg string)
}

type logRepository struct {
	BillRepository BillRepository
	MongoDetail    *typesSDK.SecretMongoDetail
	MongoClient    driverMongoSDK.MongoClientType
	DBName         string
	CollectionName string
}

func NewLogRepository() LogRepository {
	return &logRepository{
		BillRepository: NewBillRepository(),
		MongoDetail:    helper.SecretGetMongoDetail(constants.MongoServerType),
		MongoClient:    driverMongoSDK.MongoClient,
		DBName:         constants.MongoDatabaseName,
		CollectionName: constants.LogPushMessageBill,
	}
}

func (d *logRepository) SavePushMessageBill(billId string, msg string) {
	// Valid & Convert ID
	_billId, err := primitive.ObjectIDFromHex(billId)
	if err != nil {
		fmt.Println(err)
	}

	res := d.BillRepository.ShowData(billId)

	billData := res.Data.(*types.TableFactBill)

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
	}
	defer client.Disconnect(context.TODO())

	var message []*map[string]interface{}
	_ = json.Unmarshal([]byte(msg), &message)

	// Execute
	collection := db.Collection(d.CollectionName)
	_, _ = collection.InsertOne(context.TODO(), &map[string]interface{}{
		"bill_id":      _billId,
		"line_user_id": billData.LineUserID,
		"first_name":   billData.FirstName,
		"last_name":    billData.LastName,
		"phone_number": billData.PhoneNumber,
		"point":        billData.Point,
		"message":      message,
		"created_at":   billData.UpdatedAt,
	})
}
