package constants

import (
	"fmt"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
	"os"
)

// Chanel Detail
func GetDetailLineChanel() *typesSDK.LineChanelDetail {
	env := os.Getenv("ENV_STAGING")
	if env == "" {
		env = "local"
	}
	if env == "local" {
		return &typesSDK.LineChanelDetail{
			ChannelID:          "1654184099",
			ChannelSecret:      "bf97f17f151b3a43dcf3a3ea1e3955cc",
			ChannelAccessToken: "+HrmHlVXKs9i6n0eaLP/0nJhtoXvlONheWbrNyL0mJmhaitJUjpElOvM4qKzlbaIQvbW3q6LHuezSH8xWTQr6ljjxhROvK3A2Olu6VhqaGIFwfLQSKWbUmLMXsc9K2Rk30OETW6WbuKyWn0HESc6VgdB04t89/1O/w1cDnyilFU=",
		}
	} else if env == "dev" {
		return &typesSDK.LineChanelDetail{
			ChannelID:          "1654184099",
			ChannelSecret:      "bf97f17f151b3a43dcf3a3ea1e3955cc",
			ChannelAccessToken: "+HrmHlVXKs9i6n0eaLP/0nJhtoXvlONheWbrNyL0mJmhaitJUjpElOvM4qKzlbaIQvbW3q6LHuezSH8xWTQr6ljjxhROvK3A2Olu6VhqaGIFwfLQSKWbUmLMXsc9K2Rk30OETW6WbuKyWn0HESc6VgdB04t89/1O/w1cDnyilFU=",
		}
	} else if env == "prod" {
		return &typesSDK.LineChanelDetail{
			ChannelID:          "1610341662",
			ChannelSecret:      "cd4f0a3ba351b4d7965e97ff8d6ee8d3",
			ChannelAccessToken: "atPSJg8qSHBK4LnEhJvWpHJbri1SlwL7PJXO6deq9THuirxVLM3axQSA1Z7xtrWn+1Su9YsfHcDVVKUZ9MkmMNutt5OQgK+HInrLsQbCumv5sUCbg3IYyV/G2qh2w1AZVIMmJvvu/pIvwnjyJxh82FGUYhWQfeY8sLGRXgo3xvw=",
		}
	}
	return &typesSDK.LineChanelDetail{}
}

// Message
func GetMessageAfterAdminSaveBill(vDate string, vPoint int, vStatus int8) string {
	return fmt.Sprintf(`{
  "type": "flex",
  "altText": "แจ้งอัพเดทสถานะ",
  "contents": {
    "type": "bubble",
    "hero": {
      "type": "image",
      "url": "https://sv1.picz.in.th/images/2019/07/26/KPKXMJ.png",
      "size": "md",
      "aspectRatio": "2:1",
      "aspectMode": "fit"
    },
    "body": {
      "type": "box",
      "layout": "vertical",
      "spacing": "md",
      "contents": [
        {
          "type": "text",
          "text": "แจ้งอัพเดทสถานะใบเสร็จ",
          "wrap": true,
          "size": "md",
          "weight": "bold"
        },
        {
          "type": "separator"
        },
        {
          "type": "box",
          "layout": "vertical",
          "spacing": "sm",
          "contents": [
            {
              "type": "box",
              "layout": "baseline",
              "contents": [
                {
                  "type": "text",
                  "text": "วันที่/เวลา:",
                  "margin": "sm",
                  "color": "#555555",
                  "flex": 0
                },
                {
                  "type": "text",
                  "text": "%s",
                  "size": "sm",
                  "align": "end"
                }
              ]
            },
            {
              "type": "box",
              "layout": "baseline",
              "contents": [
                {
                  "type": "text",
                  "text": "แต้มที่ได้รับ:",
                  "margin": "sm",
                  "color": "#555555",
                  "flex": 0
                },
                {
                  "type": "text",
                  "text": "%d",
                  "size": "sm",
                  "align": "end"
                },
                {
                  "type": "icon",
                  "size": "xl",
                  "url": "https://cdn.yellow-idea.com/dutchie/flex-icon.png"
                }
              ]
            },
            {
              "type": "box",
              "layout": "baseline",
              "contents": [
                {
                  "type": "text",
                  "text": "สถานะ:",
                  "margin": "sm",
                  "color": "#555555",
                  "flex": 0
                },
                {
                  "type": "text",
                  "text": "%s",
                  "size": "sm",
                  "align": "end",
                  "color": "#ff0000"
                }
              ]
            }
          ]
        }
      ]
    }
  }
}`, vDate, vPoint, GetStatusTitleBill(vStatus))
}
