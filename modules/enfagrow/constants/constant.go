package constants

const TokenKey = "U/SuzQtLwD1slpuAqCFPzYtHp2Gq9wfSEhQxK7AjT30=" // [ enfagrow : yellowidea ]
const MongoDatabaseName = "Enfagrow"
const MongoServerType = 1
const UploadFolder = "/enfagrow"

const DimRegister = "dim_register"
const DimLuckyDraw = "dim_lucky_draw"
const DimPremium = "dim_premium"
const DimSetting = "dim_setting"

const DimRegisterPoint = "dim_register_point"
const DimRegisterAddress = "dim_register_address"
const FactBill = "fact_bill"
const FactPremiumRedeem = "fact_premium_redeem"
const FactLuckyDrawRedeem = "fact_lucky_draw_redeem"

const LogPushMessageBill = "log_push_message_bill"

// Bill Status
func GetStatusTitleBill(s int8) string {
	if s == 1 {
		return "ข้อมูลถูกต้อง"
	} else if s == 2 {
		return "รอการตรวจสอบ"
	} else if s == 3 {
		return "รูปภาพไม่ชัดเจน"
	} else if s == 4 {
		return "ใบเสร็จซ้ำ"
	} else if s == 5 {
		return "วันที่ไม่ถูกต้อง"
	} else if s == 6 {
		return "ไม่พบสินค้า"
	} else if s == 7 {
		return "ไม่มีวันที่"
	} else if s == 8 {
		return "ไม่มีเลขที่ใบเสร็จ"
	} else if s == 9 {
		return "ใบเสร็จไม่ร่วมรายการ"
	} else if s == 10 {
		return "ไม่มีเลขที่ใบเสร็จ"
	} else if s == 11 {
		return "ใบเสร็จไม่ร่วมรายการ"
	} else if s == 12 {
		return "ใบเสร็จไม่สมบูรณ์"
	} else if s == 13 {
		return "ใบเสร็จไม่ถูกกติกา"
	}
	return ""
}

// Tracking Status
func GetStatusTitleTracking(s int) string {
	if s == 1 {
		return "จัดส่งเรียบร้อย"
	} else if s == 2 {
		return "ดำเนินการจัดส่งภายใน 7 วัน"
	}
	return ""
}
