package constants

import (
	"fmt"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
	"os"
)

// Chanel Detail
func GetDetailLineChanel() *typesSDK.LineChanelDetail {
	env := os.Getenv("ENV_STAGING")
	if env == "" {
		env = "local"
	}
	if env == "local" {
		return &typesSDK.LineChanelDetail{
			ChannelID:          "1655081165",
			ChannelSecret:      "778ccbf12421c72788b63261e2f745c3",
			ChannelAccessToken: "EqhSJXKZTqCR6E/gbhSQuua5os9DjBd1F3KQfDizHDBqiGJfCeSi2b5Uv3iXiR+W/mSgXVY30yrns6bslrT7UShDL6Yu2WDzt0RukQjrwBbw+JgPwfHtHVSEhFFL6Ok+hjDTOCRGpi4Lxx34xyVYZwdB04t89/1O/w1cDnyilFU=",
			RichMenuID:         "richmenu-53884d2ae093f992a9f9be373b74b1b1",
		}
	} else if env == "dev" {
		return &typesSDK.LineChanelDetail{
			ChannelID:          "1655081165",
			ChannelSecret:      "778ccbf12421c72788b63261e2f745c3",
			ChannelAccessToken: "EqhSJXKZTqCR6E/gbhSQuua5os9DjBd1F3KQfDizHDBqiGJfCeSi2b5Uv3iXiR+W/mSgXVY30yrns6bslrT7UShDL6Yu2WDzt0RukQjrwBbw+JgPwfHtHVSEhFFL6Ok+hjDTOCRGpi4Lxx34xyVYZwdB04t89/1O/w1cDnyilFU=",
			RichMenuID:         "richmenu-53884d2ae093f992a9f9be373b74b1b1",
		}
	} else if env == "prod" {
		return &typesSDK.LineChanelDetail{
			ChannelID:          "1613106275",
			ChannelSecret:      "ab027cf8e4cc9424dab99707dbb737d5",
			ChannelAccessToken: "6P4evsp9Y0OplELZ8HJCX4ofakZgeyXtw0i9ufrV+/tYOm4dmDqjtg1ZnQCFhk/uI3uiC7ZwzdBpdv/pM2QmumHNHrh2Me/nf8qHockqr6NxOl9kGpbJNaj9YJuGg/tqzOWotCfhVI5pwIDGX5NLGWbZkCQsS1A9FX3FSRp/ceE=",
			RichMenuID:         "richmenu-1ad3c1ae014f4b841e256799b0ccfec7",
		}
	}
	return &typesSDK.LineChanelDetail{}
}

// Message
func GetMessageAfterAdminSaveBill(vDate string, billNoAdmin string, BillSummary float32, vStatus int8) string {
	return fmt.Sprintf(`{
    "type":"flex",
    "altText":"แจ้งอัพเดทสถานะ",
    "contents":{
        "type":"bubble",
        "body":{
            "type":"box",
            "layout":"vertical",
            "contents":[
                {
                    "type":"box",
                    "layout":"vertical",
                    "contents":[
                        {
                            "type":"text",
                            "text":"แจ้งอัพเดทสถานะใบเสร็จ",
                            "size":"lg",
                            "weight":"bold",
                            "wrap":true,
                            "color":"#4E4E4E"
                        },
                        {
                            "type":"separator",
                            "margin":"lg"
                        }
                    ]
                },
                {
                    "type":"box",
                    "layout":"horizontal",
                    "contents":[
                        {
                            "type":"text",
                            "text":"วันที่ / เวลา",
                            "align":"start"
                        },
                        {
                            "type":"text",
                            "text":"%s",
                            "flex":2,
                            "size":"sm",
                            "align":"end"
                        }
                    ],
                    "margin":"lg"
                },
                {
                    "type":"box",
                    "layout":"horizontal",
                    "contents":[
                        {
                            "type":"text",
                            "text":"เลขที่ใบเสร็จ",
                            "align":"start"
                        },
                        {
                            "type":"text",
                            "text":"%s",
                            "flex":2,
                            "size":"sm",
                            "align":"end"
                        }
                    ],
                    "margin":"lg"
                },
                {
                    "type":"box",
                    "layout":"horizontal",
                    "contents":[
                        {
                            "type":"text",
                            "text":"ยอดเงิน",
                            "align":"start"
                        },
                        {
                            "type":"box",
                            "layout":"vertical",
                            "contents":[
                                {
                                    "type":"text",
                                    "text":"%s บาท",
                                    "weight":"bold",
                                    "align":"end",
                                    "flex":0,
                                    "color":"#FFFFFF"
                                }
                            ],
                            "flex":1,
                            "cornerRadius":"md",
                            "borderColor":"#01B901",
                            "borderWidth":"normal",
                            "backgroundColor":"#01B901",
                            "offsetStart":"none",
                            "paddingAll":"xs",
                            "alignItems":"center",
                            "offsetEnd":"none",
                            "offsetTop":"none",
                            "offsetBottom":"none"
                        }
                    ],
                    "margin":"lg"
                },
                {
                    "type":"box",
                    "layout":"horizontal",
                    "contents":[
                        {
                            "type":"text",
                            "text":"สถานะ",
                            "align":"start"
                        },
                        {
                            "type":"text",
                            "text":"%s",
                            "flex":2,
                            "size":"md",
                            "align":"end",
                            "color":"#01B901",
                            "weight":"bold"
                        }
                    ],
                    "margin":"lg"
                }
            ]
        }
    }
}`, vDate, billNoAdmin, fmt.Sprintf("%.2f", BillSummary), GetStatusTitleBill(vStatus))
}

func GetMessageAfterAdminSaveBillFail(vDate string, billNoAdmin string, vStatus int8) string {
	return fmt.Sprintf(`{
    "type":"flex",
    "altText":"แจ้งอัพเดทสถานะ",
    "contents":{
        "type":"bubble",
        "body":{
            "type":"box",
            "layout":"vertical",
            "contents":[
                {
                    "type":"box",
                    "layout":"vertical",
                    "contents":[
                        {
                            "type":"text",
                            "text":"แจ้งอัพเดทสถานะใบเสร็จ",
                            "size":"lg",
                            "weight":"bold",
                            "wrap":true,
                            "color":"#4E4E4E"
                        },
                        {
                            "type":"separator",
                            "margin":"lg"
                        }
                    ]
                },
                {
                    "type":"box",
                    "layout":"horizontal",
                    "contents":[
                        {
                            "type":"text",
                            "text":"วันที่ / เวลา",
                            "align":"start"
                        },
                        {
                            "type":"text",
                            "text":"%s",
                            "flex":2,
                            "size":"sm",
                            "align":"end"
                        }
                    ],
                    "margin":"lg"
                },
                {
                    "type":"box",
                    "layout":"horizontal",
                    "contents":[
                        {
                            "type":"text",
                            "text":"เลขที่ใบเสร็จ",
                            "align":"start"
                        },
                        {
                            "type":"text",
                            "text":"%s",
                            "flex":2,
                            "size":"sm",
                            "align":"end"
                        }
                    ],
                    "margin":"lg"
                },
                {
                    "type":"box",
                    "layout":"horizontal",
                    "contents":[
                        {
                            "type":"text",
                            "text":"สถานะ",
                            "align":"start"
                        },
                        {
                            "type":"text",
                            "text":"%s",
                            "flex":2,
                            "size":"md",
                            "align":"end",
                            "color":"#FF3939",
                            "weight":"bold"
                        }
                    ],
                    "margin":"lg"
                }
            ]
        }
    }
}`, vDate, billNoAdmin, GetStatusTitleBill(vStatus))
}
