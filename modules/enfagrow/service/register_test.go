package service

import (
	"fmt"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/enfagrow/types"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	"testing"
)

func TestRegisterService_CreateData(t *testing.T) {
	payload := &types.TableDimRegister{
		//Name: "Koko-oook",
	}

	a := NewRegisterService()
	res := a.CreateData(payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}

func TestRegisterService_ListData(t *testing.T) {
	payload := &types.FilterRegisterList{
		Sort:   "",
		Order:  "",
		Offset: 0,
		Limit:  10,
		//IsExport: False,
		Filter: &types.FilterRegisterListItems{
			Name: "",
		},
	}
	a := NewRegisterService()
	res := a.ListData(payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(helperSDK.InterfaceToJson(&res.Rows))
}

func TestRegisterService_ShowData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewRegisterService()
	res := a.ShowData(id)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(helperSDK.InterfaceToJson(&res.Data))
}

func TestRegisterService_UpdateData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewRegisterService()
	payload := &types.TableDimRegister{
		//Name: "Koko",
	}
	res := a.UpdateData(id, payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}

func TestRegisterService_DeleteData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewRegisterService()
	res := a.DeleteData(id)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}
