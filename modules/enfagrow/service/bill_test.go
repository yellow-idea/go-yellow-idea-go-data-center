package service

import (
	"fmt"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/enfagrow/types"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	"testing"
)

func TestBillService_CreateData(t *testing.T) {
	payload := &types.TableFactBill{
		//Name: "Koko-oook",
	}

	a := NewBillService()
	res := a.CreateData(payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}

func TestBillService_ListData(t *testing.T) {
	payload := &types.FilterBillList{
		Sort:   "",
		Order:  "",
		Offset: 0,
		Limit:  10,
		//IsExport: False,
		Filter: &types.FilterBillListItems{
			Name: "",
		},
	}
	a := NewBillService()
	res := a.ListData(payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(helperSDK.InterfaceToJson(&res.Rows))
}

func TestBillService_ShowData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewBillService()
	res := a.ShowData(id)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(helperSDK.InterfaceToJson(&res.Data))
}

func TestBillService_UpdateData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewBillService()
	payload := &types.TableFactBill{
		//Name: "Koko",
	}
	res := a.UpdateData(id, payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}

func TestBillService_DeleteData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewBillService()
	res := a.DeleteData(id)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}
