package types

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type TableDimLuckyDrawMessage struct {
	StartDate    time.Time `json:"start_date" bson:"start_date"`
	EndDate      time.Time `json:"end_date" bson:"end_date"`
	MessageTime  string    `json:"message_time" bson:"message_time"`
	MessageDawn  string    `json:"message_dawn" bson:"message_dawn"`
	MessageThank string    `json:"message_thank" bson:"message_thank"`
}

type TableDimLuckyDraw struct {
	ID          primitive.ObjectID          `json:"id" bson:"_id"`
	Name        string                      `json:"name" bson:"name"`
	Description string                      `json:"description" bson:"description"`
	Point       int                         `json:"point" bson:"point"`
	Sequence    int                         `json:"sequence" bson:"sequence"`
	ImgURL      string                      `json:"img_url" bson:"img_url"`
	EndDate     time.Time                   `json:"end_date" bson:"end_date"`
	StartDate   time.Time                   `json:"start_date" bson:"start_date"`
	IsActive    int                         `json:"is_active" bson:"is_active"`
	Messages    []*TableDimLuckyDrawMessage `json:"messages" bson:"messages"`
	CreatedAt   time.Time                   `json:"created_at" bson:"created_at"`
	UpdatedAt   time.Time                   `json:"updated_at" bson:"updated_at"`
}

type RequestLuckyDrawRedeem struct {
	UserID primitive.ObjectID `json:"user_id" bson:"user_id"`
	ItemID primitive.ObjectID `json:"item_id" bson:"item_id"`
	Name   string             `json:"name" bson:"name"`
}

type TableFactLuckyDrawRedeem struct {
	ID               primitive.ObjectID `json:"id" bson:"_id"`
	UserID           primitive.ObjectID `json:"user_id" bson:"user_id"`
	ItemID           primitive.ObjectID `json:"item_id" bson:"item_id"`
	Name             string             `json:"name" bson:"name"`
	Point            int                `json:"point" bson:"point"`
	LineUserID       string             `json:"line_user_id" bson:"line_user_id"`
	LineDisplayName  string             `json:"line_display_name" bson:"line_display_name"`
	LineDisplayImage string             `json:"line_display_image" bson:"line_display_image"`
	FirstName        string             `json:"first_name" bson:"first_name"`
	LastName         string             `json:"last_name" bson:"last_name"`
	Email            string             `json:"email" bson:"email"`
	PhoneNumber      string             `json:"phone_number" bson:"phone_number"`
	Birthday         string             `json:"birthday" bson:"birthday"`
	Sex              string             `json:"sex" bson:"sex"`
	Address          string             `json:"address" bson:"address"`
	ZipCode          string             `json:"zip_code" bson:"zip_code"`
	ProvinceID       string             `json:"province_id" bson:"province_id"`
	CreatedAt        time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt        time.Time          `json:"updated_at" bson:"updated_at"`
}

type FilterLuckyDrawListItems struct {
	Name string `json:"name" bson:"name"`
}

type FilterLuckyDrawList struct {
	Sort     string `json:"sort" bson:"sort"`
	Order    string `json:"order" bson:"order"`
	Offset   int64  `json:"offset" bson:"offset"`
	Limit    int64  `json:"limit" bson:"limit"`
	IsExport bool   `json:"is_export" bson:"is_export"`
	Filter   *FilterLuckyDrawListItems
}
