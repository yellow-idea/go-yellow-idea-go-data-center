package types

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type TableFactBill struct {
	ID primitive.ObjectID `json:"id" bson:"_id"`

	ShopType            string  `json:"shop_type" bson:"shop_type"`
	ShopType2           string  `json:"shop_type_2" bson:"shop_type_2"`
	StatusID            int8    `json:"status_id" bson:"status_id"`
	StatusTitle         string  `json:"status_title" bson:"status_title"`
	Point               int     `json:"point" bson:"point"`
	BillNo              string  `json:"bill_no" bson:"bill_no"`
	BillNoAdmin         string  `json:"bill_no_admin" bson:"bill_no_admin"`
	BillSummary         float32 `json:"bill_summary" bson:"bill_summary"`
	ByAdminNo           int8    `json:"by_admin_no" bson:"by_admin_no"`
	ImgUrl              string  `json:"img_url" bson:"img_url"`
	IsDuplicate         int8    `json:"is_duplicate" bson:"is_duplicate"`
	IsRetry             int8    `json:"is_retry" bson:"is_retry"`
	IsUsed              int8    `json:"is_used" bson:"is_used"`
	IsChecked           int8    `json:"is_checked" bson:"is_checked"`
	IsComplaint         int     `json:"is_complaint" bson:"is_complaint"`
	PreviousPoint       int     `json:"previous_point,omitempty" bson:"previous_point"`
	PreviousStatus      int     `json:"previous_status,omitempty" bson:"previous_status"`
	PreviousStatusTitle string  `json:"previous_status_title,omitempty" bson:"previous_status_title"`

	UserID           primitive.ObjectID `json:"user_id" bson:"user_id"`
	LineUserID       string             `json:"line_user_id" bson:"line_user_id"`
	LineDisplayName  string             `json:"line_display_name" bson:"line_display_name"`
	LineDisplayImage string             `json:"line_display_image" bson:"line_display_image"`
	FirstName        string             `json:"first_name" bson:"first_name"`
	LastName         string             `json:"last_name" bson:"last_name"`
	Email            string             `json:"email" bson:"email"`
	PhoneNumber      string             `json:"phone_number" bson:"phone_number"`
	Birthday         string             `json:"birthday" bson:"birthday"`
	Sex              string             `json:"sex" bson:"sex"`
	Address          string             `json:"address" bson:"address"`
	ZipCode          string             `json:"zip_code" bson:"zip_code"`
	Province         string             `json:"province_id" bson:"province_id"`

	EnFaGrowBillSummary float32       `json:"enfagrow_bill_summary" bson:"enfagrow_bill_summary"`
	ProductGroup        string        `json:"product_group" bson:"product_group"` // New 2020-10-08
	BillProduct         []interface{} `json:"bill_product" bson:"bill_product"`

	ShopByAdmin     string                   `json:"shop_by_admin" bson:"shop_by_admin"`
	BillDateByAdmin string                   `json:"bill_date_by_admin" bson:"bill_date_by_admin"`
	Regular1Name    string                   `json:"regular_1_name" bson:"regular_1_name"`
	Regular1Pack    string                   `json:"regular_1_pack" bson:"regular_1_pack"`
	Regular1Amount  int32                    `json:"regular_1_amount" bson:"regular_1_amount"`
	Regular1Price   float32                  `json:"regular_1_price" bson:"regular_1_price"`
	Regular2Name    string                   `json:"regular_2_name" bson:"regular_2_name"`
	Regular2Pack    string                   `json:"regular_2_pack" bson:"regular_2_pack"`
	Regular2Amount  int32                    `json:"regular_2_amount" bson:"regular_2_amount"`
	Regular2Price   float32                  `json:"regular_2_price" bson:"regular_2_price"`
	Df1Name         string                   `json:"df_1_name" bson:"df_1_name"`
	Df1Pack         string                   `json:"df_1_pack" bson:"df_1_pack"`
	Df1Amount       int32                    `json:"df_1_amount" bson:"df_1_amount"`
	Df1Price        float32                  `json:"df_1_price" bson:"df_1_price"`
	Df2Name         string                   `json:"df_2_name" bson:"df_2_name"`
	Df2Pack         string                   `json:"df_2_pack" bson:"df_2_pack"`
	Df2Amount       int32                    `json:"df_2_amount" bson:"df_2_amount"`
	Df2Price        float32                  `json:"df_2_price" bson:"df_2_price"`
	Bio1Name        string                   `json:"bio_1_name" bson:"bio_1_name"`
	Bio1Pack        string                   `json:"bio_1_pack" bson:"bio_1_pack"`
	Bio1Amount      int32                    `json:"bio_1_amount" bson:"bio_1_amount"`
	Bio1Price       float32                  `json:"bio_1_price" bson:"bio_1_price"`
	Bio2Name        string                   `json:"bio_2_name" bson:"bio_2_name"`
	Bio2Pack        string                   `json:"bio_2_pack" bson:"bio_2_pack"`
	Bio2Amount      int32                    `json:"bio_2_amount" bson:"bio_2_amount"`
	Bio2Price       float32                  `json:"bio_2_price" bson:"bio_2_price"`
	Greek1Name      string                   `json:"greek_1_name" bson:"greek_1_name"`
	Greek1Pack      string                   `json:"greek_1_pack" bson:"greek_1_pack"`
	Greek1Amount    int32                    `json:"greek_1_amount" bson:"greek_1_amount"`
	Greek1Price     float32                  `json:"greek_1_price" bson:"greek_1_price"`
	Greek2Name      string                   `json:"greek_2_name" bson:"greek_2_name"`
	Greek2Pack      string                   `json:"greek_2_pack" bson:"greek_2_pack"`
	Greek2Amount    int32                    `json:"greek_2_amount" bson:"greek_2_amount"`
	Greek2Price     float32                  `json:"greek_2_price" bson:"greek_2_price"`
	Kids1Name       string                   `json:"kids_1_name" bson:"kids_1_name"`
	Kids1Pack       string                   `json:"kids_1_pack" bson:"kids_1_pack"`
	Kids1Amount     int32                    `json:"kids_1_amount" bson:"kids_1_amount"`
	Kids1Price      float32                  `json:"kids_1_price" bson:"kids_1_price"`
	Kids2Name       string                   `json:"kids_2_name" bson:"kids_2_name"`
	Kids2Pack       string                   `json:"kids_2_pack" bson:"kids_2_pack"`
	Kids2Amount     int32                    `json:"kids_2_amount" bson:"kids_2_amount"`
	Kids2Price      float32                  `json:"kids_2_price" bson:"kids_2_price"`
	Brand1Name      string                   `json:"brand_1_name" bson:"brand_1_name"`
	Brand1Pack      string                   `json:"brand_1_pack" bson:"brand_1_pack"`
	Brand1Amount    int32                    `json:"brand_1_amount" bson:"brand_1_amount"`
	Brand1Price     float32                  `json:"brand_1_price" bson:"brand_1_price"`
	CreatedAt       time.Time                `json:"created_at" bson:"created_at"`
	UpdatedAt       time.Time                `json:"updated_at" bson:"updated_at"`
	IsComplaintAt   time.Time                `json:"complaint_at" bson:"complaint_at"`
	ProductSKU      []map[string]interface{} `json:"product_sku" bson:"product_sku"`
}

type RequestBillUpload struct {
	ID          string `json:"id" bson:"_id"`
	BillNo      string `json:"bill_no" bson:"bill_no"`
	ImgURL      string `json:"img_url" bson:"img_url"`
	IsComplaint int    `json:"is_complaint" bson:"is_complaint"`
	ShopType    string `json:"shop_type" bson:"shop_type"`
	ShopType2   string `json:"shop_type_2" bson:"shop_type_2"`
	UserID      string `json:"user_id" bson:"user_id"`
}

type RequestBillHistory struct {
	UserID    primitive.ObjectID `json:"user_id" bson:"user_id"`
	StartDate time.Time          `json:"start_date" bson:"start_date"`
	EndDate   time.Time          `json:"end_date" bson:"end_date"`
	StatusID  int8               `json:"status_id" bson:"status_id"`
}

type FilterBillListItems struct {
	Name string `json:"name" bson:"name"`
}

type FilterBillList struct {
	Sort     string `json:"sort" bson:"sort"`
	Order    string `json:"order" bson:"order"`
	Offset   int64  `json:"offset" bson:"offset"`
	Limit    int64  `json:"limit" bson:"limit"`
	IsExport bool   `json:"is_export" bson:"is_export"`
	Filter   *FilterBillListItems
}

type ResponseAppBillDataByFilter struct {
	ID          primitive.ObjectID `json:"id" bson:"_id"`
	StatusID    int8               `json:"status_id" bson:"status_id"`
	StatusTitle string             `json:"status_title" bson:"status_title"`
	Point       int                `json:"point" bson:"point"`
	BillNo      string             `json:"bill_no" bson:"bill_no"`
	ImgUrl      string             `json:"img_url" bson:"img_url"`
	IsUsed      int8               `json:"is_used" bson:"is_used"`
	DateGroup   string             `json:"date_group" bson:"date_group"`
	CreatedAt   time.Time          `json:"created_at" bson:"created_at"`
}
