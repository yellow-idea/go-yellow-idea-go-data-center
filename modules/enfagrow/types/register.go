package types

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type TableDimRegister struct {
	ID               primitive.ObjectID `json:"id" bson:"_id"`
	LineUserID       string             `json:"line_user_id" bson:"line_user_id"`
	LineDisplayName  string             `json:"line_display_name" bson:"line_display_name"`
	LineDisplayImage string             `json:"line_display_image" bson:"line_display_image"`
	FirstName        string             `json:"first_name" bson:"first_name"`
	LastName         string             `json:"last_name" bson:"last_name"`
	Email            string             `json:"email" bson:"email"`
	PhoneNumber      string             `json:"phone_number" bson:"phone_number"`
	Birthday         string             `json:"birthday" bson:"birthday"`
	Sex              string             `json:"sex" bson:"sex"`
	Address          string             `json:"address" bson:"address"`
	ZipCode          string             `json:"zip_code" bson:"zip_code"`
	ProvinceID       string             `json:"province_id" bson:"province_id"`
	CreatedAt        time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt        time.Time          `json:"updated_at" bson:"updated_at"`
}

type TableDimRegisterRafflePoint struct {
	RegisterId     primitive.ObjectID `json:"register_id" bson:"register_id"`
	Point          int                `json:"point" bson:"point"`
	PointLuckyDraw int                `json:"point_lucky_draw" bson:"point_lucky_draw"`
	PointPremium   int                `json:"point_premium" bson:"point_premium"`
	TotalPoint     int                `json:"total_point,omitempty" bson:"total_point"`
	BillSummary    float32            `json:"bill_summary" bson:"bill_summary"`
}

type TableDimRegisterAddress struct {
	ID          primitive.ObjectID `json:"id" bson:"_id"`
	UserID      primitive.ObjectID `json:"user_id" bson:"user_id"`
	FirstName   string             `json:"first_name" bson:"first_name"`
	LastName    string             `json:"last_name" bson:"last_name"`
	PhoneNumber string             `json:"phone_number" bson:"phone_number"`
	Address     string             `json:"address" bson:"address"`
	ProvinceID  string             `json:"province_id" bson:"province_id"`
	ZipCode     string             `json:"zip_code" bson:"zip_code"`
	Default     string             `json:"default" bson:"default"`
	CreatedAt   time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt   time.Time          `json:"updated_at" bson:"updated_at"`
}

type ResponseRegisterPoint struct {
	DataLuckyDrawItems []struct {
		Count  int    `json:"count" bson:"count"`
		ItemID string `json:"item_id" bson:"item_id"`
	} `json:"data_lucky_draw_items" bson:"data_lucky_draw_items"`
	DataPoint struct {
		Point          int `json:"point" bson:"point"`
		PointLuckyDraw int `json:"point_lucky_draw" bson:"point_lucky_draw"`
		PointPremium   int `json:"point_premium" bson:"point_premium"`
	} `json:"data_point" bson:"data_point"`
}

type FilterRegisterListItems struct {
	Name string `json:"name" bson:"name"`
}

type FilterRegisterList struct {
	Sort     string `json:"sort" bson:"sort"`
	Order    string `json:"order" bson:"order"`
	Offset   int64  `json:"offset" bson:"offset"`
	Limit    int64  `json:"limit" bson:"limit"`
	IsExport bool   `json:"is_export" bson:"is_export"`
	Filter   *FilterRegisterListItems
}
