package repository

import (
	"context"
	"fmt"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/helper"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/enfagrow/constants"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/enfagrow/types"
	driverMongoSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/drivers/mongodb"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

type LuckyDrawRepository interface {
	ListData(payload *types.FilterLuckyDrawList) *typesSDK.SaveRepositoryResponse
	ShowData(id string) *typesSDK.SaveRepositoryResponse
	CreateData(payload *types.TableDimLuckyDraw) *typesSDK.SaveRepositoryResponse
	UpdateData(id string, payload *types.TableDimLuckyDraw) *typesSDK.SaveRepositoryResponse
	DeleteData(id string) *typesSDK.SaveRepositoryResponse
	ReportRedeemHistory(payload *types.FilterReportLuckyDrawRedeemHistory) *typesSDK.SaveRepositoryResponse // TODO:
	// App
	ListAll() []*types.TableDimLuckyDraw
	Redeem(payload *types.RequestLuckyDrawRedeem, point int) *typesSDK.SaveRepositoryResponse
	RedeemHistory(registerID string) *typesSDK.SaveRepositoryResponse
}

type luckyDrawRepository struct {
	MongoDetail      *typesSDK.SecretMongoDetail
	MongoClient      driverMongoSDK.MongoClientType
	DBName           string
	CollectionName   string
	CollectionRedeem string
}

func NewLuckyDrawRepository() LuckyDrawRepository {
	return &luckyDrawRepository{
		MongoDetail:      helper.SecretGetMongoDetail(constants.MongoServerType),
		MongoClient:      driverMongoSDK.MongoClient,
		DBName:           constants.MongoDatabaseName,
		CollectionName:   constants.DimLuckyDraw,
		CollectionRedeem: constants.FactLuckyDrawRedeem,
	}
}

func (d *luckyDrawRepository) ListData(payload *types.FilterLuckyDrawList) *typesSDK.SaveRepositoryResponse {
	var total int64
	resultsEmpty := make([]*types.TableDimLuckyDraw, 0)

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	findOptions := options.Find()
	findOptions.SetSort(bson.D{{"updated_at", -1}})
	findOptions.SetSkip(payload.Offset)
	findOptions.SetLimit(payload.Limit)

	var results []*types.TableDimLuckyDraw

	filter := bson.D{}
	if payload.Filter.Name != "" {
		filter = append(filter, bson.E{
			Key:   "name",
			Value: bson.D{{"$regex", fmt.Sprintf(".*%s.*", payload.Filter.Name)}},
		})
	}
	cur, err := collection.Find(context.TODO(), filter, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem types.TableDimLuckyDraw
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Data Not Found",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	_ = cur.Close(context.TODO())

	// Clear Data
	defer func() { results = nil }()

	// Output
	if results == nil {
		results = resultsEmpty
		total = 0
	} else {
		total, _ = collection.CountDocuments(context.TODO(), filter)
	}

	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Rows:       results,
		Total:      int32(total),
	}
}

func (d *luckyDrawRepository) ShowData(id string) *typesSDK.SaveRepositoryResponse {
	// Valid & Convert ID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
			Data:       nil,
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Data:       nil,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	var result *types.TableDimLuckyDraw
	filter := &bson.D{{"_id", _id}}
	err = collection.FindOne(context.TODO(), filter).Decode(&result)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Data:       nil,
		}
	}

	// Clear Data
	defer func() { result = nil }()

	if result.Messages == nil {
		result.Messages = make([]*types.TableDimLuckyDrawMessage, 0)
	}
	// Output
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Data:       result,
	}
}

func (d *luckyDrawRepository) CreateData(payload *types.TableDimLuckyDraw) *typesSDK.SaveRepositoryResponse {
	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	payload.CreatedAt = helperSDK.GetTimeNowGMT()
	s := &bson.D{
		{"_id", primitive.NewObjectID()},
		{"name", payload.Name},
		{"description", payload.Description},
		{"point", payload.Point},
		{"sequence", payload.Sequence},
		{"img_url", payload.ImgURL},
		{"start_date", payload.StartDate},
		{"end_date", payload.EndDate},
		{"is_active", payload.IsActive},
		{"messages", payload.Messages},
		{"created_at", payload.CreatedAt},
		{"updated_at", payload.CreatedAt},
	}
	res, err := collection.InsertOne(context.TODO(), s)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Create Error",
		}
	}

	var result *types.TableDimLuckyDraw
	filter := &bson.D{{"_id", res.InsertedID}}
	_ = collection.FindOne(context.TODO(), filter).Decode(&result)

	// Output
	return &typesSDK.SaveRepositoryResponse{
		ID:         res.InsertedID,
		CodeReturn: 1,
		Message:    "Create Success",
		Data:       result,
	}
}

func (d *luckyDrawRepository) UpdateData(id string, payload *types.TableDimLuckyDraw) *typesSDK.SaveRepositoryResponse {
	// Valid & Convert ID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	filter := &bson.D{{"_id", _id}}
	s := &bson.D{
		{"name", payload.Name},
		{"description", payload.Description},
		{"point", payload.Point},
		{"sequence", payload.Sequence},
		{"img_url", payload.ImgURL},
		{"start_date", payload.StartDate},
		{"end_date", payload.EndDate},
		{"is_active", payload.IsActive},
		{"messages", payload.Messages},
		{"updated_at", helperSDK.GetTimeNowGMT()},
	}
	update := &bson.D{{"$set", s}}
	res, err := collection.UpdateOne(context.TODO(), filter, update)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			ID:         _id,
			CodeReturn: -1,
			Message:    "Update Error",
		}
	}

	// Output Error
	if res.ModifiedCount <= 0 {
		return &typesSDK.SaveRepositoryResponse{
			ID:         _id,
			CodeReturn: 2,
			Message:    "Data Not Found",
		}
	}

	var result *types.TableDimLuckyDraw
	filter = &bson.D{{"_id", _id}}
	_ = collection.FindOne(context.TODO(), filter).Decode(&result)

	// Output
	return &typesSDK.SaveRepositoryResponse{
		ID:         _id,
		CodeReturn: 1,
		Message:    "Update Success",
		Data:       result,
	}
}

func (d *luckyDrawRepository) DeleteData(id string) *typesSDK.SaveRepositoryResponse {
	// Valid & Convert ID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	filter := bson.D{{"_id", _id}}
	res, err := collection.DeleteOne(context.TODO(), filter)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			ID:         _id,
			CodeReturn: -1,
			Message:    "Delete Error",
		}
	}

	// Output Error
	if res.DeletedCount <= 0 {
		return &typesSDK.SaveRepositoryResponse{
			ID:         _id,
			CodeReturn: 2,
			Message:    "Data Not Found",
		}
	}

	// Output Error
	return &typesSDK.SaveRepositoryResponse{
		ID:         _id,
		CodeReturn: 1,
		Message:    "Delete Success",
	}
}

func (d *luckyDrawRepository) ListAll() []*types.TableDimLuckyDraw {
	resultsEmpty := make([]*types.TableDimLuckyDraw, 0)

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
		return resultsEmpty
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	findOptions := options.Find()
	findOptions.SetSort(&bson.D{{"sequence", 1}})
	var results []*types.TableDimLuckyDraw
	t1 := time.Now()
	locationTime, errTime := time.LoadLocation("Asia/Bangkok")
	if errTime != nil {
		fmt.Println(errTime)
	} else {
		t1 = time.Now().In(locationTime)
	}

	cur, err := collection.Find(context.TODO(), bson.D{
		bson.E{
			Key:   "start_date",
			Value: bson.M{"$lte": t1}},
	}, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return resultsEmpty
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem types.TableDimLuckyDraw
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
		return resultsEmpty
	}

	_ = cur.Close(context.TODO())

	// Clear Data
	defer func() { results = nil }()

	// Output
	if results == nil {
		return resultsEmpty
	}
	return results
}

func (d *luckyDrawRepository) Redeem(payload *types.RequestLuckyDrawRedeem, point int) *typesSDK.SaveRepositoryResponse {
	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	// Get Register Data
	collection := db.Collection(constants.DimRegister)
	var registerData *types.TableDimRegister
	err = collection.FindOne(context.TODO(), &bson.D{{"_id", payload.UserID}}).Decode(&registerData)

	// Get Register Address
	collection = db.Collection(constants.DimRegisterAddress)
	var vAdd *types.TableDimRegisterAddress
	err = collection.FindOne(context.TODO(), &bson.D{
		{"user_id", payload.UserID},
		{"default", "1"},
	}).Decode(&vAdd)

	// Execute
	collection = db.Collection(d.CollectionRedeem)
	createdAt := helperSDK.GetTimeNowGMT()
	_, err = collection.InsertOne(context.TODO(), &types.TableFactLuckyDrawRedeem{
		ID:               primitive.NewObjectID(),
		UserID:           payload.UserID,
		ItemID:           payload.ItemID,
		Name:             payload.Name,
		Point:            point,
		LineUserID:       registerData.LineUserID,
		LineDisplayName:  registerData.LineDisplayName,
		LineDisplayImage: registerData.LineDisplayImage,
		Email:            registerData.Email,
		Birthday:         registerData.Birthday,
		Sex:              registerData.Sex,
		FirstName:        vAdd.FirstName,
		LastName:         vAdd.LastName,
		PhoneNumber:      vAdd.PhoneNumber,
		Address:          vAdd.Address,
		ProvinceID:       vAdd.ProvinceID,
		ZipCode:          vAdd.ZipCode,
		CreatedAt:        createdAt,
		UpdatedAt:        createdAt,
	})

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Redeem Error",
		}
	}

	// Update IsUsed All Bill
	collection = db.Collection(constants.FactBill)
	_, _ = collection.UpdateMany(context.TODO(),
		&bson.D{{"user_id", payload.UserID}},
		&bson.D{{"$set", &bson.D{
			{"is_used", 1},
		}}})

	// Clear Data
	defer func() {
		registerData = nil
		vAdd = nil
	}()

	// Output
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Redeem Success",
	}
}

func (d *luckyDrawRepository) RedeemHistory(registerID string) *typesSDK.SaveRepositoryResponse {
	resultsEmpty := make([]*types.TableFactLuckyDrawRedeem, 0)

	// Valid & Convert ID
	_registerID, err := primitive.ObjectIDFromHex(registerID)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
			Data:       resultsEmpty,
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connect Fail",
			Data:       resultsEmpty,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionRedeem)
	findOptions := options.Find()

	var results []*types.TableFactLuckyDrawRedeem
	filter := &bson.D{{"user_id", _registerID}}
	cur, err := collection.Find(context.TODO(), filter, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Data:       resultsEmpty,
		}
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem types.TableFactLuckyDrawRedeem
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Data:       resultsEmpty,
		}
	}

	_ = cur.Close(context.TODO())

	matchStage := bson.D{{"$match", filter}}
	groupStage := bson.D{{"$group", bson.D{{"_id", bson.D{{"item_id", "$item_id"}, {"name", "$name"}}}, {"count", bson.D{{"$sum", 1}}}}}}

	cur, err = collection.Aggregate(context.TODO(),
		mongo.Pipeline{
			matchStage,
			groupStage,
		},
	)

	// Assign Data Array
	var showsLoaded []bson.M
	if err = cur.All(context.TODO(), &showsLoaded); err != nil {
		fmt.Println(err)
	}

	_ = cur.Close(context.TODO())

	// Clear Data
	defer func() { results = nil }()

	// Output
	if results == nil {
		results = resultsEmpty
	}
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Data:       results,
		Rows:       showsLoaded,
	}
}

func (d *luckyDrawRepository) ReportRedeemHistory(payload *types.FilterReportLuckyDrawRedeemHistory) *typesSDK.SaveRepositoryResponse {
	resultsEmpty := make([]*types.TableFactLuckyDrawRedeem, 0)
	var results []*types.TableFactLuckyDrawRedeem
	var total int64
	filter := bson.D{}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(constants.FactLuckyDrawRedeem)
	findOptions := options.Find()
	// No Projection When Export
	if !payload.IsExport {
		findOptions.SetProjection(&bson.M{
			"last_name":    1,
			"first_name":   1,
			"phone_number": 1,
			"address":      1,
			"zip_code":     1,
			"name":         1,
			"created_at":   1,
		})
	}
	findOptions.SetSort(&bson.D{{"updated_at", 1}})

	// No Limit When Export
	if !payload.IsExport {
		findOptions.SetSkip(payload.Offset)
		findOptions.SetLimit(payload.Limit)
	}

	if payload.Filter.PhoneNumber != "" {
		filter = append(filter, bson.E{
			Key:   "phone_number",
			Value: bson.D{{"$regex", fmt.Sprintf(".*%s.*", payload.Filter.PhoneNumber)}},
		})
	}
	if !payload.Filter.StartDate.IsZero() && !payload.Filter.EndDate.IsZero() {
		filter = append(filter, bson.E{
			Key: "created_at",
			Value: bson.M{
				"$gte": payload.Filter.StartDate,
				"$lte": payload.Filter.EndDate,
			}})
	}
	cur, err := collection.Find(context.TODO(), filter, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem types.TableFactLuckyDrawRedeem
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Data Not Found",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	_ = cur.Close(context.TODO())

	// No Total
	if !payload.IsExport {
		total, _ = collection.CountDocuments(context.TODO(), filter)
	}

	// Clear Data
	defer func() {
		results = nil
		total = 0
	}()

	// Output
	if results == nil {
		results = resultsEmpty
	}
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Rows:       results,
		Total:      int32(total),
	}
}
