package repository

import (
	"context"
	"fmt"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/helper"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/ntl/constants"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/ntl/types"
	driverMongoSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/drivers/mongodb"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type RegisterRepository interface {
	ListData(payload *types.FilterRegisterList) *typesSDK.SaveRepositoryResponse
}

type registerRepository struct {
	MongoDetail           *typesSDK.SecretMongoDetail
	MongoClient           driverMongoSDK.MongoClientType
	DBName                string
	CollectionName        string
	CollectionNamePoint   string
	CollectionNameAddress string
}

func NewRegisterRepository() RegisterRepository {
	return &registerRepository{
		MongoDetail:           helper.SecretGetMongoDetail(constants.MongoServerType),
		MongoClient:           driverMongoSDK.MongoClient,
		DBName:                constants.MongoDatabaseName,
		CollectionName:        constants.DimRegister,
	}
}

func (d *registerRepository) ListData(payload *types.FilterRegisterList) *typesSDK.SaveRepositoryResponse {
	resultsEmpty := make([]*types.TableDimRegister, 0)

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	findOptions := options.Find()
	findOptions.SetSort(bson.D{{"updated_at", -1}})
	findOptions.SetSkip(payload.Offset)
	findOptions.SetLimit(payload.Limit)

	var results []*types.TableDimRegister

	filter := bson.D{}
	if payload.Filter.Name != "" {
		filter = append(filter, bson.E{
			Key:   "name",
			Value: bson.D{{"$regex", fmt.Sprintf(".*%s.*", payload.Filter.Name)}},
		})
	}
	cur, err := collection.Find(context.TODO(), filter, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem types.TableDimRegister
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Data Not Found",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	_ = cur.Close(context.TODO())

	// Clear Data
	defer func() { results = nil }()

	// Output
	if results == nil {
		results = resultsEmpty
	}
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Rows:       results,
		Total:      0,
	}
}
