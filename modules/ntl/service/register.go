package service

import (
	"fmt"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/ntl/repository"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/ntl/types"
	"math"
	"net/http"
	"strings"

	//helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
	//"sync"
	//"time"
)

type RegisterService interface {
	ListData(payload *types.FilterRegisterList) *typesSDK.SaveRepositoryResponse
	LoopLink(payload *types.FilterRegisterList)
}

type registerService struct {
	RegisterRepository repository.RegisterRepository
}

func NewRegisterService() RegisterService {
	return &registerService{
		RegisterRepository: repository.NewRegisterRepository(),
	}
}

func (x *registerService) ListData(payload *types.FilterRegisterList) *typesSDK.SaveRepositoryResponse {
	res := x.RegisterRepository.ListData(payload)
	a := res.Rows.([]*types.TableDimRegister)
	for _, v := range a {
		fmt.Println(v.MID)
	}
	return res
}

//func addRichMenu(mid string, menuId string) int {
//	url := fmt.Sprintf("https://api.line.me/v2/bot/user/%s/richmenu/%s", mid, menuId)
//	method := "POST"
//	client := &http.Client{
//	}
//	req, err := http.NewRequest(method, url, nil)
//	if err != nil {
//		return 500
//	}
//	req.Header.Add("Authorization", "Bearer 5daLoMcePQxO8anU/5gp7rg/MaJZF3GjL0T3UfPwcD+Mg3yWT7x3V/8uMUFnt6765lh8hBYzU/HVrXw6aSqzOUAePr5npy7ZiGfU3IV4jjr9jTZ6Bj4e8xcSvUNDCqqKGBhTsz68LmdR6IFsFj2F9gFIS9xybk1bpjJUhI9NTk0=")
//	res, err := client.Do(req)
//	defer res.Body.Close()
//	return res.StatusCode
//}
//
//func removeRichMenu(mid string, menuId string) int {
//	client := &http.Client{
//	}
//	req, err := http.NewRequest("DELETE", fmt.Sprintf("https://api.line.me/v2/bot/user/%s/richmenu", mid), nil)
//	if err != nil {
//		return 500
//	}
//	req.Header.Add("Authorization", "Bearer 5daLoMcePQxO8anU/5gp7rg/MaJZF3GjL0T3UfPwcD+Mg3yWT7x3V/8uMUFnt6765lh8hBYzU/HVrXw6aSqzOUAePr5npy7ZiGfU3IV4jjr9jTZ6Bj4e8xcSvUNDCqqKGBhTsz68LmdR6IFsFj2F9gFIS9xybk1bpjJUhI9NTk0=")
//	res, _ := client.Do(req)
//	req, err = http.NewRequest("POST", fmt.Sprintf("https://api.line.me/v2/bot/user/%s/richmenu/%s", mid, menuId), nil)
//	if err != nil {
//		return 500
//	}
//	req.Header.Add("Authorization", "Bearer 5daLoMcePQxO8anU/5gp7rg/MaJZF3GjL0T3UfPwcD+Mg3yWT7x3V/8uMUFnt6765lh8hBYzU/HVrXw6aSqzOUAePr5npy7ZiGfU3IV4jjr9jTZ6Bj4e8xcSvUNDCqqKGBhTsz68LmdR6IFsFj2F9gFIS9xybk1bpjJUhI9NTk0=")
//	res, _ = client.Do(req)
//	defer res.Body.Close()
//	return res.StatusCode
//}

func bulkRichMenu(mid string, menuId string) int {
	url := "https://api.line.me/v2/bot/richmenu/bulk/link"
	method := "POST"
	payload := strings.NewReader(fmt.Sprintf(`{"richMenuId": "%s", "userIds": [%s]}`, menuId, mid))
	client := &http.Client{}
	req, err := http.NewRequest(method, url, payload)
	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Authorization", "Bearer 5daLoMcePQxO8anU/5gp7rg/MaJZF3GjL0T3UfPwcD+Mg3yWT7x3V/8uMUFnt6765lh8hBYzU/HVrXw6aSqzOUAePr5npy7ZiGfU3IV4jjr9jTZ6Bj4e8xcSvUNDCqqKGBhTsz68LmdR6IFsFj2F9gFIS9xybk1bpjJUhI9NTk0=")
	req.Header.Add("Content-Type", "application/json")
	res, _ := client.Do(req)
	defer res.Body.Close()
	return res.StatusCode
}

func (x *registerService) LoopLink(payload *types.FilterRegisterList) {
	limit := 150
	maxRows := 60536.00 //float64(len(a))
	payload.Limit = int64(limit)
	b := maxRows / float64(limit)
	offset := 0
	for i := 1; i <= int(math.Ceil(b)); i++ {
		payload.Offset = int64(offset)
		// Query
		res := x.RegisterRepository.ListData(payload)
		a := res.Rows.([]*types.TableDimRegister)
		strArr := make([]string, 0)
		for _, v := range a {
			strArr = append(strArr, fmt.Sprintf(`"%s"`, v.MID))
		}
		// Last Row Update Me
		if i == int(math.Ceil(b)) { // int(math.Ceil(b))
			strArr = append(strArr, fmt.Sprintf(`"%s"`, "U2513603d1b4a11c7e4a0440c6265aa7d"))
		}
		// Run Bulk
		statusCode := bulkRichMenu(strings.Join(strArr, ","), "richmenu-691b3c36fc01fb69c427d2eb357916c4")
		// Print New Process
		fmt.Println(a[0].MID)
		fmt.Printf("Max : %d I : %d Code : %d\n", int(b), i, statusCode)

		offset = offset + limit
	}

	//var wg sync.WaitGroup
	//for i, v := range a {
	//	// Increment the WaitGroup counter.
	//	wg.Add(1)
	//	go func(v *types.TableDimRegister, i int) {
	//		// Decrement the counter when the goroutine completes.
	//		defer wg.Done()
	//		a1 := removeRichMenu(v.MID, "richmenu-691b3c36fc01fb69c427d2eb357916c4")
	//		//a2 := addRichMenu(v.MID, "richmenu-691b3c36fc01fb69c427d2eb357916c4")
	//		if i%5 == 0 {
	//			fmt.Printf("No. %d time : %s\n", i, helperSDK.GetTimeFormat1(helperSDK.GetTimeNowGMT()))
	//		} else {
	//			fmt.Printf("Delete %d\n", a1)
	//		}
	//	}(v, i)
	//	if i%500 == 0 {
	//		time.Sleep(1 * time.Second)
	//	}
	//}
	//// Wait for all HTTP fetches to complete.
	//wg.Wait()
	//res = nil
	//removeRichMenu("U2513603d1b4a11c7e4a0440c6265aa7d")
	//addRichMenu("U2513603d1b4a11c7e4a0440c6265aa7d", "richmenu-691b3c36fc01fb69c427d2eb357916c4")
}
