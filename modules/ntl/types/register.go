package types

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type TableDimRegister struct {
	ID  primitive.ObjectID `json:"id" bson:"_id"`
	MID string             `json:"mId" bson:"mId"`
}

type FilterRegisterListItems struct {
	Name string `json:"name" bson:"name"`
}

type FilterRegisterList struct {
	Sort     string `json:"sort" bson:"sort"`
	Order    string `json:"order" bson:"order"`
	Offset   int64  `json:"offset" bson:"offset"`
	Limit    int64  `json:"limit" bson:"limit"`
	IsExport bool   `json:"is_export" bson:"is_export"`
	Filter   *FilterRegisterListItems
}
