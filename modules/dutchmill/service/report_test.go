package service

import (
	"fmt"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchmill/types"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	"testing"
	"time"
)

func TestGetSurveyReport(t *testing.T) {
	a := NewReportService()

	var payload types.RequestGetSurveyReport
	payload.Limit = 10
	stDate, _ := time.Parse(time.RFC3339, "0001-01-01T00:00:00.000Z")
	edDate, _ := time.Parse(time.RFC3339, "0001-01-01T00:00:00.000Z")
	payload.Filter.StartDate = stDate
	payload.Filter.EndDate = edDate
	payload.Filter.UserType = "mt"
	res := a.GetSurveyReport(&payload)
	fmt.Println(helperSDK.InterfaceToJson(&res.Rows))
}

func TestGetPCMReport(t *testing.T) {
	a := NewReportService()
	res := a.GetPCMReport()
	fmt.Println(helperSDK.InterfaceToJson(&res.Rows))
}

func TestGetAuditReport(t *testing.T) {
	a := NewReportService()
	res := a.GetAuditReport()
	fmt.Println(helperSDK.InterfaceToJson(&res.Rows))
}
