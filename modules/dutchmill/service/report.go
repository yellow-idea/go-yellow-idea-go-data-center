package service

import (
	"context"
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/helper"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchmill/constants"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchmill/interfaces"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchmill/repository"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchmill/types"
	driverMongoSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/drivers/mongodb"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"sort"
	"sync"
	"time"
)

type reportService struct {
	MongoDetail      *typesSDK.SecretMongoDetail
	MongoClient      driverMongoSDK.MongoClientType
	DBName           string
	ReportRepository interfaces.ReportRepository
}

func NewReportService() interfaces.ReportService {
	return &reportService{
		MongoDetail:      helper.SecretGetMongoDetail(constants.MongoServerType),
		MongoClient:      driverMongoSDK.MongoClient,
		DBName:           constants.MongoDatabaseName,
		ReportRepository: repository.NewReportRepository(),
	}
}

func (x *reportService) GetSurveyReport(payload *types.RequestGetSurveyReport) *typesSDK.SaveRepositoryResponse {
	// Connect DB
	client, db, err := x.MongoClient(x.MongoDetail, x.DBName)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	data := x.ReportRepository.GetSurveyReport(db, payload)

	if len(data) <= 0 {
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Not Found",
		}
	}

	// Export Excel
	f := excelize.NewFile()
	sheet1 := "Sheet1"
	fileName := helperSDK.GetTimeNowGMT().Format("200601021504") + "-SurveyReport.xlsx"

	// Render Header
	_ = f.SetCellValue(sheet1, "A1", "User Name")
	_ = f.SetCellValue(sheet1, "B1", "Name")
	_ = f.SetCellValue(sheet1, "C1", "Company")
	_ = f.SetCellValue(sheet1, "D1", "Store Code")
	_ = f.SetCellValue(sheet1, "E1", "Store Name")
	_ = f.SetCellValue(sheet1, "F1", "Survey Date")
	_ = f.SetCellValue(sheet1, "G1", "Check In")
	_ = f.SetCellValue(sheet1, "H1", "Check Out")
	_ = f.SetCellValue(sheet1, "I1", "Check In Latitude")
	_ = f.SetCellValue(sheet1, "J1", "Check In Longitude")
	_ = f.SetCellValue(sheet1, "K1", "Check Out Latitude")
	_ = f.SetCellValue(sheet1, "L1", "Check Out Longitude")

	// Render Body
	var wg sync.WaitGroup
	for i, v := range data {
		wg.Add(1)
		go func(index int, v *types.DataGetSurveyReport, wg *sync.WaitGroup) {
			defer wg.Done()
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`A%d`, index), v.UserName)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`B%d`, index), v.Name)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`C%d`, index), v.Company)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`D%d`, index), v.StoreCode)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`E%d`, index), v.StoreName)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`F%d`, index), v.SurveyDate)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`G%d`, index), v.CheckIn)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`H%d`, index), v.CheckOut)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`I%d`, index), v.CheckInLatitude)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`J%d`, index), v.CheckInLongitude)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`K%d`, index), v.CheckOutLatitude)
			_ = f.SetCellValue(sheet1, fmt.Sprintf(`L%d`, index), v.CheckOutLongitude)
		}(i+2, v, &wg)
		time.Sleep(100 * time.Microsecond)
	}

	// Wait Group
	wg.Wait()

	if err := f.SaveAs("/tmp/" + fileName); err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Error!",
		}
	}

	// Clear Data
	defer func() { data = nil }()

	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Data:       helperSDK.S3FileToCDN("/tmp/"+fileName, fmt.Sprintf("%s/%s", constants.UploadFolder, fileName)),
	}
}

func (x *reportService) GetPCMReport() *typesSDK.SaveRepositoryResponse {
	resultsEmpty := make([]*map[string]interface{}, 0)

	// Connect DB
	client, db, err := x.MongoClient(x.MongoDetail, x.DBName)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}
	defer client.Disconnect(context.TODO())

	data := x.ReportRepository.GetPCMReport(db)

	if len(data) <= 0 {
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Not Found",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: -1,
		Message:    "Data Not Found",
		Rows:       data,
		Total:      0,
	}
}

func (x *reportService) GetAuditReport() *typesSDK.SaveRepositoryResponse {
	//type resultAuditReport struct {
	//	AuditName    string `json:"audit_name"`
	//	Date         string `json:"date"`
	//	StoreCode    string `json:"store_code"`
	//	StoreName    string `json:"store_name"`
	//	Username     string `json:"username"`
	//	MaterialType string `json:"material_type"`
	//	MaterialName string `json:"material_name"`
	//	Location     string `json:"location"`
	//	Pcm          int    `json:"pcm"`
	//	Audit        string `json:"audit"`
	//	Data         string `json:"data"`
	//}
	//var results []*resultAuditReport
	var tmpActivities []*types.FactStoreActivities
	var tmpStorePlace []*types.DimStoresPlaces
	var tmpUser []*types.DimUsers
	resultsEmpty := make([]*map[string]interface{}, 0)

	// =================================================== //
	// Connect DB
	// =================================================== //
	client, db, err := x.MongoClient(x.MongoDetail, x.DBName)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}
	defer client.Disconnect(context.TODO())

	// Export Excel
	f := excelize.NewFile()
	sheet1 := "Sheet1"
	fileName := helperSDK.GetTimeNowGMT().Format("200601021504") + "-AuditReport.xlsx"

	// Render Header
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(1, 1), "Audit Name")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(2, 1), "Date")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(3, 1), "Store Code")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(4, 1), "Store Name")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(5, 1), "User Name")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(6, 1), "Material Type")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(7, 1), "Material Name")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(8, 1), "Location")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(9, 1), "Pcm")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(10, 1), "Audit")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(11, 1), "data")

	start := time.Now()
	// =================================================== //
	// Get Data
	// =================================================== //
	i := 2
	x.ReportRepository.GetAuditReport(db, func(cur *mongo.Cursor) {
		var storeData *types.DimStoresPlaces
		var userData *types.DimUsers
		var elem types.FactStoreActivities
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}

		// Store
		storePlaceID, err := primitive.ObjectIDFromHex(elem.StoreID)
		if err != nil {
			fmt.Println(err)
		}

		findStore := sort.Search(len(tmpStorePlace), func(i int) bool { return storePlaceID == tmpStorePlace[i].ID })
		if findStore <= 0 {
			storeData = x.ReportRepository.FindOneStorePlace(db, bson.D{{"_id", storePlaceID}})
			tmpStorePlace = append(tmpStorePlace, storeData)
		} else {
			storeData = tmpStorePlace[findStore-1]
		}
		elem.StorePlaceData = storeData

		// User
		userID, err := primitive.ObjectIDFromHex(elem.UserID)
		if err != nil {
			fmt.Println(err)
		}
		findUser := sort.Search(len(tmpUser), func(i int) bool { return userID == tmpUser[i].ID })
		if findUser <= 0 {
			userData = x.ReportRepository.FindOneUser(db, &bson.D{{"_id", userID}})
			tmpUser = append(tmpUser, userData)
		} else {
			userData = tmpUser[findUser-1]
		}
		elem.UserData = userData

		tmpActivities = append(tmpActivities, &elem)

		// =================================================== //
		// Gen Excel
		// =================================================== //
		for _, v2 := range elem.Products {
			//	"date": `${arrDate[2]}/${arrDate[1]}/${arrDate[0]}`,
			//	"pcm": parseInt(product.pcm.is_has_product),
			//	"audit": parseInt(product.is_has_product),
			//	"data": parseInt(product.pcm.is_has_product) === parseInt(product.is_has_product) ? "ตรง" : "ไม่ตรง",
			if elem.UserData != nil && elem.StorePlaceData != nil {
				_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(1, i), elem.UserData.Name)            // "Audit Name"
				_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(2, i), "")                            // TODO: Date
				_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(3, i), elem.StorePlaceData.StoreCode) // "Store Code"
				_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(4, i), elem.StorePlaceData.StoreName) // "Store Name"
				_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(5, i), elem.UserData.Username)        // "User Name"
				_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(6, i), v2.Type)                       // "Material Type"
				_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(7, i), v2.MaterialName)               // "Material Name"
				_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(8, i), v2.Location)                   // "Location"
				_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(9, i), 0)                             // "Pcm"
				_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(10, i), v2.IsHasProduct)              // "Audit"
				_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(11, i), "")                           // data
			}
			i++
		}
	})

	fmt.Println("Get Data Time", time.Since(start))
	// Output Not Found
	if len(tmpActivities) <= 0 {
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Not Found",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	if err := f.SaveAs("/tmp/" + fileName); err != nil {
		fmt.Println(err)
	}

	// Clear Data
	defer func() {
		tmpActivities = nil
		tmpStorePlace = nil
		tmpUser = nil
	}()

	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Data:       helperSDK.S3FileToCDN("/tmp/"+fileName, fmt.Sprintf("%s/%s", constants.UploadFolder, fileName)),
	}
}
