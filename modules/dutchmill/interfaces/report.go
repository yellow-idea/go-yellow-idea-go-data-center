package interfaces

import (
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchmill/types"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
	"go.mongodb.org/mongo-driver/mongo"
)

type ReportRepository interface {
	GetSurveyReport(db *mongo.Database, payload *types.RequestGetSurveyReport) []*types.DataGetSurveyReport
	GetPCMReport(db *mongo.Database) []*map[string]interface{}
	GetAuditReport(db *mongo.Database, fn1 func(cur *mongo.Cursor))
	FindOneUser(db *mongo.Database, filter interface{}) *types.DimUsers
	FindOneStorePlace(db *mongo.Database, filter interface{}) *types.DimStoresPlaces
}

type ReportService interface {
	GetSurveyReport(payload *types.RequestGetSurveyReport) *typesSDK.SaveRepositoryResponse
	GetPCMReport() *typesSDK.SaveRepositoryResponse
	GetAuditReport() *typesSDK.SaveRepositoryResponse
}
