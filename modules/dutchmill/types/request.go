package types

import "time"

type RequestGetSurveyReport struct {
	Sort     string `json:"sort" bson:"sort"`
	Order    string `json:"order" bson:"order"`
	Offset   int64  `json:"offset" bson:"offset"`
	Limit    int64  `json:"limit" bson:"limit"`
	IsExport bool   `json:"is_export" bson:"is_export"`
	Filter   struct {
		StartDate time.Time `json:"start_date" bson:"start_date"`
		EndDate   time.Time `json:"end_date" bson:"end_date"`
		UserType  string    `json:"user_type" bson:"user_type"`
	}
}
