package types

type DataGetSurveyReport struct {
	CheckIn           string  `json:"check_in" bson:"check_in"`
	CheckInLatitude   string  `json:"check_in_latitude" bson:"check_in_latitude"`
	CheckInLongitude  string  `json:"check_in_longitude" bson:"check_in_longitude"`
	CheckOut          string  `json:"check_out" bson:"check_out"`
	CheckOutLatitude  string  `json:"check_out_latitude" bson:"check_out_latitude"`
	CheckOutLongitude string  `json:"check_out_longitude" bson:"check_out_longitude"`
	Company           string  `json:"company" bson:"company"`
	Duration          float64 `json:"duration" bson:"duration"`
	Name              string  `json:"name" bson:"name"`
	StoreCode         string  `json:"store_code" bson:"store_code"`
	StoreName         string  `json:"store_name" bson:"store_name"`
	SurveyDate        string  `json:"survey_date" bson:"survey_date"`
	UserName          string  `json:"user_name" bson:"user_name"`
}
