package types

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type DimUsers struct {
	ID          primitive.ObjectID `json:"id" bson:"_id"`
	Number      int                `json:"number" bson:"number"`
	Area        string             `json:"area" bson:"area"`
	Username    string             `json:"username" bson:"username"`
	CustomerID  string             `json:"customer_id" bson:"customer_id"`
	Name        string             `json:"name" bson:"name"`
	Nickname    string             `json:"nickname" bson:"nickname"`
	PhoneNumber string             `json:"phone_number" bson:"phone_number"`
	Contact     string             `json:"contact" bson:"contact"`
	StartDate   string             `json:"start_date" bson:"start_date"`
	Store       string             `json:"store" bson:"store"`
	Holiday     string             `json:"holiday" bson:"holiday"`
	UserRole    string             `json:"user_role" bson:"user_role"`
	Role        string             `json:"role" bson:"role"`
	Remark      string             `json:"remark" bson:"remark"`
	CreatedAt   time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt   time.Time          `json:"updated_at" bson:"updated_at"`
}

type DimStores struct {
	ID                primitive.ObjectID `json:"id" bson:"_id"`
	DepartmentID      string             `json:"department_id" bson:"department_id"`
	TemplateSystem    string             `json:"template_system" bson:"template_system"`
	Name              string             `json:"name" bson:"name"`
	ImgURL            string             `json:"img_url" bson:"img_url"`
	PpMustHave        []interface{}      `json:"pp_must_have" bson:"pp_must_have"`
	UhtSingleMustHave []interface{}      `json:"uht_single_must_have" bson:"uht_single_must_have"`
	UhtPackMustHave   []interface{}      `json:"uht_pack_must_have" bson:"uht_pack_must_have"`
	CreatedAt         time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt         time.Time          `json:"updated_at" bson:"updated_at"`
}

type DimStoresPlaces struct {
	ID                        primitive.ObjectID `json:"id" bson:"_id"`
	Template                  string             `json:"template" bson:"template"`
	StoreCode                 string             `json:"store_code" bson:"store_code"`
	StoreLane                 string             `json:"store_lane" bson:"store_lane"`
	StoreName                 string             `json:"store_name" bson:"store_name"`
	StoreType                 string             `json:"store_type" bson:"store_type"`
	Location                  string             `json:"location" bson:"location"`
	Latitude                  string             `json:"latitude" bson:"latitude"`
	Longitude                 string             `json:"longitude" bson:"longitude"`
	PhoneNumber               string             `json:"phone_number" bson:"phone_number"`
	PpOpenBayAll              int                `json:"pp_open_bay_all" bson:"pp_open_bay_all"`
	PpOpenAllShelves          int                `json:"pp_open_all_shelves" bson:"pp_open_all_shelves"`
	PpOpenBayMilk             int                `json:"pp_open_bay_milk" bson:"pp_open_bay_milk"`
	PpOpenMilkShelves         int                `json:"pp_open_milk_shelves" bson:"pp_open_milk_shelves"`
	PpCloseBayAll             int                `json:"pp_close_bay_all" bson:"pp_close_bay_all"`
	PpCloseAllShelves         int                `json:"pp_close_all_shelves" bson:"pp_close_all_shelves"`
	PpCloseBayMilk            int                `json:"pp_close_bay_milk" bson:"pp_close_bay_milk"`
	PpCloseMilkShelves        int                `json:"pp_close_milk_shelves" bson:"pp_close_milk_shelves"`
	PpClose                   int                `json:"pp_close" bson:"pp_close"`
	PpClassAmount             int                `json:"pp_class_amount" bson:"pp_class_amount"`
	UhtSingleOpenBayAll       int                `json:"uht_single_open_bay_all" bson:"uht_single_open_bay_all"`
	UhtSingleOpenAllShelves   int                `json:"uht_single_open_all_shelves" bson:"uht_single_open_all_shelves"`
	UhtSingleOpenBayMilk      int                `json:"uht_single_open_bay_milk" bson:"uht_single_open_bay_milk"`
	UhtSingleOpenMilkShelves  int                `json:"uht_single_open_milk_shelves" bson:"uht_single_open_milk_shelves"`
	UhtSingleCloseBayAll      int                `json:"uht_single_close_bay_all" bson:"uht_single_close_bay_all"`
	UhtSingleCloseAllShelves  int                `json:"uht_single_close_all_shelves" bson:"uht_single_close_all_shelves"`
	UhtSingleCloseBayMilk     int                `json:"uht_single_close_bay_milk" bson:"uht_single_close_bay_milk"`
	UhtSingleCloseMilkShelves int                `json:"uht_single_close_milk_shelves" bson:"uht_single_close_milk_shelves"`
	UhtSingleClassAmount      int                `json:"uht_single_class_amount" bson:"uht_single_class_amount"`
	UhtPackBayMilk            int                `json:"uht_pack_bay_milk" bson:"uht_pack_bay_milk"`
	UhtPackMilkShelves        int                `json:"uht_pack_milk_shelves" bson:"uht_pack_milk_shelves"`
	UhtPackClassAmount        int                `json:"uht_pack_class_amount" bson:"uht_pack_class_amount"`
	IsApprove                 int                `json:"is_approve" bson:"is_approve"`
	IsCreateByApp             int                `json:"is_create_by_app" bson:"is_create_by_app"`
	ShelfPromotion            int                `json:"shelf_promotion" bson:"shelf_promotion"`
	ImgURL                    string             `json:"img_url" bson:"img_url"`
	Amount                    int                `json:"amount" bson:"amount"`
	AppCreateBy               string             `json:"app_create_by" bson:"app_create_by"`
	AppCreateLatitude         string             `json:"app_create_latitude" bson:"app_create_latitude"`
	AppCreateLongitude        string             `json:"app_create_longitude" bson:"app_create_longitude"`
	ApproveBy                 string             `json:"approve_by" bson:"approve_by"`
	ApproveDate               time.Time          `json:"approve_date" bson:"approve_date"`
	CreatedAt                 time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt                 time.Time          `json:"updated_at" bson:"updated_at"`
	TemplateID                primitive.ObjectID `json:"template_id" bson:"template_id"`
}

type FactStoreActivities struct {
	ID             primitive.ObjectID `json:"id" bson:"_id"`
	UserType       string             `json:"user_type" bson:"user_type"`
	UpdateType     string             `json:"update_type" bson:"update_type"`
	Date           time.Time          `json:"date" bson:"date"`
	UserID         string             `json:"user_id" bson:"user_id"`
	UserData       *DimUsers          `json:"user_data,omitempty" bson:"user_data"`
	StoreID        string             `json:"store_id" bson:"store_id"`
	StorePlaceData *DimStoresPlaces   `json:"store_place_data,omitempty" bson:"store_place_data"`
	IsUpdate       int                `json:"is_update" bson:"is_update"`
	Latitude       string             `json:"latitude" bson:"latitude"`
	Longitude      string             `json:"longitude" bson:"longitude"`
	Images         []interface{}      `json:"images" bson:"images"`
	Datas          []interface{}      `json:"datas" bson:"datas"`
	Products       []struct {
		ID           string `json:"id" bson:"id"`
		SeqNo        int    `json:"seq_no" bson:"seq_no"`
		MaterialName string `json:"material_name" bson:"material_name"`
		Type         string `json:"type" bson:"type"`
		Group        string `json:"group" bson:"group"`
		Size         string `json:"size" bson:"size"`
		Location     string `json:"location" bson:"location"`
		Promotion    string `json:"promotion" bson:"promotion"`
		Highlight    string `json:"highlight" bson:"highlight"`
		IsHasProduct string `json:"is_has_product" bson:"is_has_product"`
		Leg          string `json:"leg" bson:"leg"`
		Top          string `json:"top" bson:"top"`
		ReasonID     string `json:"reason_id" bson:"reason_id"`
	} `json:"products" bson:""`
	Task             string             `json:"task" bson:"task"`
	StoreCode        string             `json:"store_code" bson:"store_code"`
	StoreName        string             `json:"store_name" bson:"store_name"`
	SeqNo            int                `json:"seq_no" bson:"seq_no"`
	AreaName         string             `json:"area_name" bson:"area_name"`
	SubAreaName      string             `json:"sub_area_name" bson:"sub_area_name"`
	TemplateSystem   string             `json:"template_system" bson:"template_system"`
	TemplateImgURL   string             `json:"template_img_url" bson:"template_img_url"`
	TemplateName     string             `json:"template_name" bson:"template_name"`
	MaterialLocation string             `json:"material_location" bson:"material_location"`
	Type             string             `json:"type" bson:"type"`
	AreaID           primitive.ObjectID `json:"area_id" bson:"area_id"`
	SubAreaID        primitive.ObjectID `json:"sub_area_id" bson:"sub_area_id"`
	TemplateID       primitive.ObjectID `json:"template_id" bson:"template_id"`
	CreatedAt        time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt        time.Time          `json:"updated_at" bson:"updated_at"`
}
