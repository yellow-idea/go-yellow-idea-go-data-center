package repository

import (
	"context"
	"fmt"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchmill/interfaces"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchmill/types"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type reportRepository struct {
}

func NewReportRepository() interfaces.ReportRepository {
	return &reportRepository{}
}

func (x *reportRepository) GetSurveyReport(db *mongo.Database, payload *types.RequestGetSurveyReport) []*types.DataGetSurveyReport {
	var results []*types.DataGetSurveyReport
	filter := bson.D{}

	if payload.Filter.UserType != "" {
		filter = append(filter, bson.E{
			Key:   "user_type",
			Value: payload.Filter.UserType,
		})
	}
	if !payload.Filter.StartDate.IsZero() && !payload.Filter.EndDate.IsZero() {
		filter = append(filter, bson.E{
			Key: "date",
			Value: bson.M{
				"$gte": payload.Filter.StartDate,
				"$lte": payload.Filter.EndDate,
			}})
	}

	// Execute
	findOptions := options.Find()
	findOptions.SetProjection(&bson.M{
		"_id":         0,
		"date":        0,
		"store_id":    0,
		"user_id":     0,
		"area_id":     0,
		"sub_area_id": 0,
		"area_name":   0,
		"user_type":   0,
	})
	findOptions.SetSort(&bson.D{{"user_name", 1}})
	//findOptions.SetSkip(payload.Offset)
	//findOptions.SetLimit(payload.Limit)

	cur, err := db.Collection("fact_report_surveys").Find(context.TODO(), filter, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return results
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem types.DataGetSurveyReport
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		//elem["id"] = elem["_id"]
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
		return results
	}

	_ = cur.Close(context.TODO())

	defer func() { results = nil }()

	return results
}

func (x *reportRepository) GetPCMReport(db *mongo.Database) []*map[string]interface{} {
	var result []*map[string]interface{}
	result = append(result, &map[string]interface{}{
		"id": "11234",
	})
	defer func() { result = nil }()
	return result
}

func (x *reportRepository) GetAuditReport(db *mongo.Database, fn1 func(cur *mongo.Cursor)) {
	filter := bson.D{
		{"update_type", "product"},
		{"user_type", "audit"},
	}

	// Execute
	findOptions := options.Find()
	findOptions.SetProjection(&bson.M{
		"_id":                         0,
		"store_id":                    1,
		"user_id":                     1,
		"date":                        1,
		"products.material_name":      1,
		"products.type":               1,
		"products.location":           1,
		"products.is_has_product":     1,
		"products.pcm.is_has_product": 1,
		"products.pcm.date":           1,
	})
	//findOptions.SetSort(&bson.D{
	//})
	findOptions.SetLimit(100)

	cur, err := db.Collection("fact_store_activities").Find(context.TODO(), filter, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
	}

	// Assign Data
	for cur.Next(context.TODO()) {
		fn1(cur)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
	}

	// Close Cursor
	_ = cur.Close(context.TODO())
}

func (x *reportRepository) FindOneUser(db *mongo.Database, filter interface{}) (result *types.DimUsers) {
	err := db.Collection("dim_users").FindOne(context.TODO(), filter).Decode(&result)
	if err != nil {
		fmt.Println(err)
	}
	return result
}

func (x *reportRepository) FindOneStorePlace(db *mongo.Database, filter interface{}) (result *types.DimStoresPlaces) {
	err := db.Collection("dim_store_places").FindOne(context.TODO(), filter).Decode(&result)
	if err != nil {
		fmt.Println(err)
	}
	return result
}
