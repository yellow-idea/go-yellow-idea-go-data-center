package router

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchmill/service"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/dutchmill/types"
	httpSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/http"
	"net/http"
)

func RouteCms(r *gin.Engine) {
	rep := r.Group("/cms/dutchmill/report")
	{
		sr := service.NewReportService()
		// Report Survey
		rep.POST("/survey", func(c *gin.Context) {
			var payload *types.RequestGetSurveyReport
			defer func() { payload = nil }()
			err := c.BindJSON(&payload)
			if err != nil {
				fmt.Println(err)
				httpSDK.ResponseBadRequest(c)
				return
			}
			res := sr.GetSurveyReport(payload)
			c.JSON(http.StatusOK, res)
		})
		// Report Audit
		rep.POST("/audit", func(c *gin.Context) {
			//var payload *types.RequestGetSurveyReport // TODO: Change
			//defer func() { payload = nil }()
			//err := c.BindJSON(&payload)
			//if err != nil {
			//	fmt.Println(err)
			//	httpSDK.ResponseBadRequest(c)
			//	return
			//}
			res := sr.GetAuditReport()
			c.JSON(http.StatusOK, res.Data)
		})
	}
}
