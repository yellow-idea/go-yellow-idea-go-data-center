package repository

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/unilever-makro/types"
)

type ReportRepository interface {
	ReportNewDashboardBill(db *sql.DB, payload *types.FilterReportNewDashboardBill) *sql.Rows
	ReportNewDashboardSummary(db *sql.DB) *sql.Rows
}

type reportRepository struct{}

func NewReportRepository() ReportRepository {
	return &reportRepository{}
}

func reportNewDashboardBillGetQuery(prefix string) string {
	return fmt.Sprintf(`( SELECT
	a4.line_user_id,
	IFNULL(a1.bill_id,'') as bill_id,
	a2.created_at,
	IFNULL( a2.bill_date_by_admin, '' ) AS bill_date_by_admin,
	a3.first_name,
	a3.last_name,
	IFNULL( a3.customer_id , '' ) as customer_id,
	a3.address,
	a3.province_id,
	a3.zip_code,
	a2.product_1_all,
	a2.product_1_unilever,
	a2.product_1_competitor,
	a2.product_2_all,
	a2.product_2_unilever,
	a2.product_2_competitor,
	a2.product_3_all,
	a2.product_3_unilever,
	a2.product_3_competitor,
	IFNULL(a1.category_name,'') as category_name,
	IFNULL(a1.product_group,'') as product_group,
	IFNULL(a1.sku,'') as sku,
	IFNULL(a1.price,0) as price,
	IFNULL(a1.size,0) as size,
	IFNULL(a1.amount1,0) as amount1,
	IFNULL(a1.amount2,0) as amount2,
	IFNULL(a1.total,0) as total,
	"" AS competitor_product_group,
	"" AS competitor_sku,
	0 AS competitor_price,
	"" AS competitor_size,
	0 AS competitor_amount1,
	0 AS competitor_amount2,
	0 As competitor_total,
	IFNULL( a2.item_favourite , '' ) as item_favourite,
	a2.bill_summary,
	a2.unilever_bill,
	IFNULL( a3.store , '' ) as shop_name
	FROM
		loyalty_1901_makro_transaction_bill_upload_product AS a1
		INNER JOIN loyalty_1901_makro_transaction_bill_upload%s AS a2 ON a1.bill_id = a2.id
		INNER JOIN master_register AS a3 ON a2.user_id = a3.id
		INNER JOIN master_user_line AS a4 ON a3.line_user_id = a4.id 
	) UNION ALL
	(
	SELECT
		a4.line_user_id,
		IFNULL(a1.bill_id,'') as bill_id,
		a2.created_at,
		IFNULL( a2.bill_date_by_admin, '' ),
		a3.first_name,
		a3.last_name,
		IFNULL( a3.customer_id , '' ) as customer_id,
		a3.address,
		a3.province_id,
		a3.zip_code,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		IFNULL(a1.category_name,'') as category_name,
		"",
		"",
		0,
		"",
		0,
		0,
		0,
		IFNULL(a1.product_group,'') as product_group,
		IFNULL(a1.sku,'') as sku,
		IFNULL(a1.price,0) as price,
		IFNULL(a1.size,0) as size,
		IFNULL(a1.amount1,0) as amount1,
		IFNULL(a1.amount2,0) as amount2,
		IFNULL(a1.total,0) as total,
		IFNULL( a2.item_favourite , '' ) as item_favourite,
		a2.bill_summary,
		a2.unilever_bill,
		IFNULL( a3.store , '' ) as shop_name
	FROM
		loyalty_1901_makro_transaction_bill_upload_product_competitor AS a1
		INNER JOIN loyalty_1901_makro_transaction_bill_upload%s AS a2 ON a1.bill_id = a2.id
		INNER JOIN master_register AS a3 ON a2.user_id = a3.id
		INNER JOIN master_user_line AS a4 ON a3.line_user_id = a4.id
	)`, prefix, prefix)
}

func (d *reportRepository) ReportNewDashboardBill(db *sql.DB, _ *types.FilterReportNewDashboardBill) *sql.Rows {
	rows, err := db.Query(fmt.Sprintf(`%s UNION %s UNION %s %s`,
		reportNewDashboardBillGetQuery(""),
		reportNewDashboardBillGetQuery("_bk"),
		reportNewDashboardBillGetQuery("_bk2"),
		"ORDER BY line_user_id, bill_id, category_name, product_group DESC"))
	if err != nil {
		fmt.Println(err)
		return nil
	}
	return rows
}

func (d *reportRepository) ReportNewDashboardSummary(db *sql.DB) *sql.Rows {
	rows, err := db.Query(fmt.Sprintf(`SELECT
	a3.line_user_id,
	a2.first_name,
	a2.last_name,
	a2.phone_number,
	SUM( a1.bill_summary ) AS bill_summary,
	SUM( a1.unilever_bill ) AS unilever_bill,
	COUNT( a1.id ) AS count_bill,
	SUM(( a1.product_1_all + a1.product_1_unilever + a1.product_1_all  )) AS product_1,
	SUM(( a1.product_2_all + a1.product_2_unilever + a1.product_2_all  )) AS product_2,
	SUM(( a1.product_3_all + a1.product_3_unilever + a1.product_3_all  )) AS product_3 
FROM
	loyalty_1901_makro_transaction_bill_upload AS a1
	INNER JOIN master_register AS a2 ON a1.user_id = a2.id
	INNER JOIN master_user_line AS a3 ON a2.line_user_id = a3.id 
GROUP BY
	a3.line_user_id,
	a2.first_name,
	a2.last_name,
	a2.phone_number
ORDER BY bill_summary DESC`))
	if err != nil {
		fmt.Println(err)
		return nil
	}
	return rows
}
