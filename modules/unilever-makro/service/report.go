package service

import (
	"database/sql"
	"fmt"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/helper"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/unilever-makro/constants"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/unilever-makro/repository"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/unilever-makro/types"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	"os"

	//helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
	//"sync"
	//"time"
)

type ReportService interface {
	ReportNewDashboardBill(payload *types.FilterReportNewDashboardBill) *typesSDK.SaveRepositoryResponse
	ReportNewDashboardSummary() *typesSDK.SaveRepositoryResponse
}

type reportService struct {
	MySqlDataSource  string
	ReportRepository repository.ReportRepository
}

func NewReportService() ReportService {
	d := helper.SecretGetMySqlDetail()
	env := os.Getenv("ENV_STAGING")
	dbName := constants.MySqlDatabaseName
	if env == "prod" {
		dbName = "PROD_G2_UNILEVER_MAKRO"
	}
	return &reportService{
		MySqlDataSource:  fmt.Sprintf("%s:%s@tcp(%s:3306)/%s", d.Username, d.Password, d.IPAddress, dbName),
		ReportRepository: repository.NewReportRepository(),
	}
}

func (x *reportService) ReportNewDashboardBill(payload *types.FilterReportNewDashboardBill) *typesSDK.SaveRepositoryResponse {
	db, err := sql.Open("mysql", x.MySqlDataSource)
	if err != nil {
		fmt.Println("Connection Fail")
	}
	defer db.Close()

	rows := x.ReportRepository.ReportNewDashboardBill(db, payload)
	fileName := helperSDK.GetTimeNowGMT().Format("200601021504") + "-Report-Makro-DashboardBill.xlsx"
	helper.ReportUNDashboardBill(fileName, rows)

	// Upload
	return &typesSDK.SaveRepositoryResponse{
		Data: helperSDK.S3FileToCDN("/tmp/"+fileName, fmt.Sprintf("%s/%s", constants.UploadFolder, fileName)),
	}
}

func (x *reportService) ReportNewDashboardSummary() *typesSDK.SaveRepositoryResponse {
	db, err := sql.Open("mysql", x.MySqlDataSource)
	if err != nil {
		fmt.Println("Connection Fail")
	}
	defer db.Close()

	rows := x.ReportRepository.ReportNewDashboardSummary(db)
	fileName := helperSDK.GetTimeNowGMT().Format("200601021504") + "-Report-Makro-DashboardSummary.xlsx"
	helper.ReportUNDashboardSummary(fileName, rows)

	// Upload
	return &typesSDK.SaveRepositoryResponse{
		Data: helperSDK.S3FileToCDN("/tmp/"+fileName, fmt.Sprintf("%s/%s", constants.UploadFolder, fileName)),
	}
}
