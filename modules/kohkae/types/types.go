package types

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type AggregateCount struct {
	Count int32 `json:"count" bson:"count"`
}

type ResponseAppInit struct {
	PremiumList   []*TableDimPremium   `json:"premium_list" bson:"premium_list"`
	LuckyDrawList []*TableDimLuckyDraw `json:"lucky_draw_list" bson:"lucky_draw_list"`
}

// [CMS] Report ข้อมูลลงทะเบียน [Listing]
type FilterReportRegisterRequest struct {
	Sort     string `json:"sort" bson:"sort"`
	Order    string `json:"order" bson:"order"`
	Offset   int64  `json:"offset" bson:"offset"`
	Limit    int64  `json:"limit" bson:"limit"`
	IsExport bool   `json:"is_export" bson:"is_export"`
	Filter   struct {
		StartDate   time.Time `json:"start_date" bson:"start_date"`
		EndDate     time.Time `json:"end_date" bson:"end_date"`
		PhoneNumber string    `json:"phone_number" bson:"phone_number"`
	}
}

// [CMS] Report ลูกค้าที่ร่วมกิจกรรม [Listing]
type ResponseReportBillCustomer struct {
	ID struct {
		LineUserID      string `json:"line_user_id" bson:"line_user_id"`
		LineDisplayName string `json:"line_display_name" bson:"line_display_name"`
		UserID          string `json:"user_id" bson:"user_id"`
		Email           string `json:"email" bson:"email"`
		FirstName       string `json:"first_name" bson:"first_name"`
		LastName        string `json:"last_name" bson:"last_name"`
		PhoneNumber     string `json:"phone_number" bson:"phone_number"`
		Sex             string `json:"sex,omitempty" bson:"sex"`
		Birthday        string `json:"birthday,omitempty" bson:"birthday"`
		Address         string `json:"address,omitempty" bson:"address"`
		ProvinceID      string `json:"province_id,omitempty" bson:"province_id"`
		ZipCode         string `json:"zip_code,omitempty" bson:"zip_code"`
	} `json:"id" bson:"_id"`
	BillSummary             float64   `json:"bill_summary" bson:"bill_summary"`
	CurrentPoint            int       `json:"current_point" bson:"current_point"`
	TotalBill               int       `json:"total_bill" bson:"total_bill"`
	TotalPoint              int       `json:"total_point" bson:"total_point"`
	TotalKohKaeBillSummary int       `json:"kohkae_bill_summary" bson:"kohkae_bill_summary"`
	Regular1Amount          int32     `json:"regular_1_amount" bson:"regular_1_amount"`
	Regular1Price           float32   `json:"regular_1_price" bson:"regular_1_price"`
	Regular2Amount          int32     `json:"regular_2_amount" bson:"regular_2_amount"`
	Regular2Price           float32   `json:"regular_2_price" bson:"regular_2_price"`
	Df1Amount               int32     `json:"df_1_amount" bson:"df_1_amount"`
	Df1Price                float32   `json:"df_1_price" bson:"df_1_price"`
	Df2Amount               int32     `json:"df_2_amount" bson:"df_2_amount"`
	Df2Price                float32   `json:"df_2_price" bson:"df_2_price"`
	Bio1Amount              int32     `json:"bio_1_amount" bson:"bio_1_amount"`
	Bio1Price               float32   `json:"bio_1_price" bson:"bio_1_price"`
	Bio2Amount              int32     `json:"bio_2_amount" bson:"bio_2_amount"`
	Bio2Price               float32   `json:"bio_2_price" bson:"bio_2_price"`
	Greek1Amount            int32     `json:"greek_1_amount" bson:"greek_1_amount"`
	Greek1Price             float32   `json:"greek_1_price" bson:"greek_1_price"`
	Greek2Amount            int32     `json:"greek_2_amount" bson:"greek_2_amount"`
	Greek2Price             float32   `json:"greek_2_price" bson:"greek_2_price"`
	Kids1Amount             int32     `json:"kids_1_amount" bson:"kids_1_amount"`
	Kids1Price              float32   `json:"kids_1_price" bson:"kids_1_price"`
	Kids2Amount             int32     `json:"kids_2_amount" bson:"kids_2_amount"`
	Kids2Price              float32   `json:"kids_2_price" bson:"kids_2_price"`
	Brand1Amount            int32     `json:"brand_1_amount" bson:"brand_1_amount"`
	Brand1Price             float32   `json:"brand_1_price" bson:"brand_1_price"`
	UpdatedAt               time.Time `json:"updated_at" bson:"updated_at"`
}

// [CMS] Report ตรวจสอบใบเสร็จ [Listing]
type FilterReportBillUploadRequest struct {
	Sort     string `json:"sort" bson:"sort"`
	Order    string `json:"order" bson:"order"`
	Offset   int64  `json:"offset" bson:"offset"`
	Limit    int64  `json:"limit" bson:"limit"`
	IsExport bool   `json:"is_export" bson:"is_export"`
	Filter   struct {
		StartDate   time.Time `json:"start_date,omitempty" bson:"start_date"`
		EndDate     time.Time `json:"end_date,omitempty" bson:"end_date"`
		PhoneNumber string    `json:"phone_number" bson:"phone_number"`
		BillNo      string    `json:"bill_no" bson:"bill_no"`
		ShopType    string    `json:"shop_type" bson:"shop_type"`
		ShopType2   string    `json:"shop_type_2" bson:"shop_type_2"`
	} `json:"filter" bson:"filter"`
}

// [CMS] Report ตรวจสอบใบเสร็จเรียบร้อย [Listing]
type FilterReportBillUploadSuccessRequest struct {
	Sort     string `json:"sort" bson:"sort"`
	Order    string `json:"order" bson:"order"`
	Offset   int64  `json:"offset" bson:"offset"`
	Limit    int64  `json:"limit" bson:"limit"`
	IsExport bool   `json:"is_export" bson:"is_export"`
	Filter   *struct {
		StartDate   time.Time `json:"start_date" bson:"start_date"`
		EndDate     time.Time `json:"end_date" bson:"end_date"`
		PhoneNumber string    `json:"phone_number" bson:"phone_number"`
		BillNo      string    `json:"bill_no" bson:"bill_no"`
		ShopType    string    `json:"shop_type" bson:"shop_type"`
		ShopType2   string    `json:"shop_type_2" bson:"shop_type_2"`
		StatusID    int8      `json:"status_id" bson:"status_id"`
	} `json:"filter" bson:"filter"`
}

// [CMS] Report ประวัติแลกของรางวัล [Listing]
type FilterReportPremiumRedeemHistory struct {
	PhoneNumber string    `json:"phone_number" bson:"phone_number"`
	StatusID    int8      `json:"status_id" bson:"status_id"`
	StartDate   time.Time `json:"start_date" bson:"start_date"`
	EndDate     time.Time `json:"end_date" bson:"end_date"`
	IsExport    bool      `json:"is_export" bson:"is_export"`
}

// [CMS] Report ประวัติแลกของรางวัล [Save]
type FilterReportPremiumRedeemHistorySave struct {
	ID               primitive.ObjectID `json:"id" bson:"_id"`
	TrackingStatusID int                `json:"tracking_status_id" bson:"tracking_status_id"`
	TrackingNo       string             `json:"tracking_no" bson:"tracking_no"`
}

// [CMS] Report ประวัติลุ้นรางวัล [Listing]
type FilterReportLuckyDrawRedeemHistory struct {
	Sort     string `json:"sort" bson:"sort"`
	Order    string `json:"order" bson:"order"`
	Offset   int64  `json:"offset" bson:"offset"`
	Limit    int64  `json:"limit" bson:"limit"`
	IsExport bool   `json:"is_export" bson:"is_export"`
	Filter   *struct {
		PhoneNumber string    `json:"phone_number" bson:"phone_number"`
		StartDate   time.Time `json:"start_date" bson:"start_date"`
		EndDate     time.Time `json:"end_date" bson:"end_date"`
	} `json:"filter" bson:"filter"`
}
