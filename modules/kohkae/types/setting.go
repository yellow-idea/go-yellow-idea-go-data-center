package types

type TableSettingMain struct {
	HomeCondition      string `json:"home_condition" bson:"home_condition"`
	LuckyDawnCondition string `json:"lucky_dawn_condition" bson:"lucky_dawn_condition"`
}
