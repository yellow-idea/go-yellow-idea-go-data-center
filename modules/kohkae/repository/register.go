package repository

import (
	"context"
	"fmt"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/helper"
	constLine "gitlab.yellow-idea.com/yellow-idea-go-data-center/modules-standard/line/constants"
	typesLine "gitlab.yellow-idea.com/yellow-idea-go-data-center/modules-standard/line/types"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/kohkae/constants"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/kohkae/types"
	driverMongoSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/drivers/mongodb"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	//"strings"
)

type RegisterRepository interface {
	UpdateTransaction()
	ListData(payload *types.FilterRegisterList) *typesSDK.SaveRepositoryResponse
	ShowData(id string) *typesSDK.SaveRepositoryResponse
	CreateData(payload *types.TableDimRegister) *typesSDK.SaveRepositoryResponse
	UpdateData(id string, payload *types.TableDimRegister) *typesSDK.SaveRepositoryResponse
	DeleteData(id string) *typesSDK.SaveRepositoryResponse
	ReportRegister(payload *types.FilterReportRegisterRequest) *typesSDK.SaveRepositoryResponse // TODO:
	// App
	GetPoint(registerID string) *typesSDK.SaveRepositoryResponse
	GetPointByRegisterID(registerID string) *types.TableDimRegisterRafflePoint
	UpdatePointByRegisterID(registerID string) *typesSDK.SaveRepositoryResponse
	ShowDataByMid(mid string) *typesSDK.SaveRepositoryResponse
	GetAddress(registerID string) *typesSDK.SaveRepositoryResponse
	GetAddressDefaultByRegID(registerID string) *typesSDK.SaveRepositoryResponse
	GetAddressById(id string) *typesSDK.SaveRepositoryResponse
	CreateAddress(registerID string, payload *types.TableDimRegisterAddress) *typesSDK.SaveRepositoryResponse
	UpdateAddress(addressID string, payload *types.TableDimRegisterAddress) *typesSDK.SaveRepositoryResponse
	UpdateAddressToDefault(registerID string, addressID string) *typesSDK.SaveRepositoryResponse
}

type registerRepository struct {
	MongoDetail           *typesSDK.SecretMongoDetail
	MongoClient           driverMongoSDK.MongoClientType
	DBName                string
	CollectionName        string
	CollectionNamePoint   string
	CollectionNameAddress string
	BillRepository        BillRepository
	PremiumRepository     PremiumRepository
	LuckyDrawRepository   LuckyDrawRepository
}

func NewRegisterRepository() RegisterRepository {
	return &registerRepository{
		MongoDetail:           helper.SecretGetMongoDetail(constants.MongoServerType),
		MongoClient:           driverMongoSDK.MongoClient,
		DBName:                constants.MongoDatabaseName,
		CollectionName:        constants.DimRegister,
		CollectionNamePoint:   constants.DimRegisterPoint,
		CollectionNameAddress: constants.DimRegisterAddress,
		BillRepository:        NewBillRepository(),
		PremiumRepository:     NewPremiumRepository(),
		LuckyDrawRepository:   NewLuckyDrawRepository(),
	}
}

func (d *registerRepository) UpdateTransaction() {
	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
	}
	defer client.Disconnect(context.TODO())

	var results []*types.TableDimRegister
	// Execute
	collection := db.Collection(d.CollectionName)
	findOptions := options.Find()
	cur, err := collection.Find(context.TODO(), bson.D{}, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem types.TableDimRegister
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
	}

	_ = cur.Close(context.TODO())

	// Clear Data
	defer func() { results = nil }()

	// ================================================================ //
	// Update Point
	// ================================================================ //
	var vTotalPoint []*types.TableFactBill

	curPoint, _ := db.Collection(constants.FactBill).Aggregate(context.TODO(),
		mongo.Pipeline{
			bson.D{{"$group", bson.D{
				{"_id", "$user_id"},
				{"point", bson.D{{"$sum", "$point"}}},
			},
			}},
		},
	)

	if err = curPoint.All(context.TODO(), &vTotalPoint); err != nil {
		fmt.Println(err)
	}

	_ = curPoint.Close(context.TODO())

	for i, v := range vTotalPoint {
		fmt.Println(i, v.ID, v.Point)

		_, _ = db.Collection(constants.DimRegisterPoint).UpdateMany(context.TODO(),
			&bson.D{{"register_id", v.ID}},
			&bson.D{{"$set", &bson.D{
				{"point", v.Point},
			}}})
	}

	// ================================================================ //

	// ================================================================ //
	// Update Transaction
	// ================================================================ //
	for i, v := range results {
		_ = fmt.Sprintf(`%d, %v`, i, v)

		//// Get Register Address
		//var vAdd *types.TableDimRegisterAddress
		//err = db.Collection(constants.DimRegisterAddress).FindOne(context.TODO(), &bson.D{
		//	{"user_id", v.ID},
		//	{"default", "1"},
		//}).Decode(&vAdd)
		//
		//// Update Redeem Lucky Draw
		//_, _ = db.Collection(constants.FactBill).UpdateMany(context.TODO(),
		//	&bson.D{{"user_id", v.ID}},
		//	&bson.D{{"$set", &bson.D{
		//		{"line_user_id", v.LineUserID},
		//		{"line_display_name", v.LineDisplayName},
		//		{"line_display_image", v.LineDisplayImage},
		//		{"first_name", vAdd.FirstName},
		//		{"last_name", vAdd.LastName},
		//		{"email", v.Email},
		//		{"phone_number", vAdd.PhoneNumber},
		//		{"birthday", v.Birthday},
		//		{"sex", v.Sex},
		//		{"address", vAdd.Address},
		//		{"zip_code", vAdd.ZipCode},
		//		{"province_id", vAdd.ProvinceID},
		//	}}})
		//
		//// Update Redeem Premium
		//_, _ = db.Collection(constants.FactPremiumRedeem).UpdateMany(context.TODO(),
		//	&bson.D{{"user_id", v.ID}},
		//	&bson.D{{"$set", &bson.D{
		//		{"parent_name", "-"},
		//		{"line_user_id", v.LineUserID},
		//		{"line_display_name", v.LineDisplayName},
		//		{"line_display_image", v.LineDisplayImage},
		//		{"first_name", vAdd.FirstName},
		//		{"last_name", vAdd.LastName},
		//		{"email", v.Email},
		//		{"phone_number", vAdd.PhoneNumber},
		//		{"birthday", v.Birthday},
		//		{"sex", v.Sex},
		//		{"address", vAdd.Address},
		//		{"zip_code", vAdd.ZipCode},
		//		{"province_id", vAdd.ProvinceID},
		//	}}})
		//
		//// Update Redeem Lucky Draw
		//_, _ = db.Collection(constants.FactLuckyDrawRedeem).UpdateMany(context.TODO(),
		//	&bson.D{{"user_id", v.ID}},
		//	&bson.D{{"$set", &bson.D{
		//		{"line_user_id", v.LineUserID},
		//		{"line_display_name", v.LineDisplayName},
		//		{"line_display_image", v.LineDisplayImage},
		//		{"first_name", vAdd.FirstName},
		//		{"last_name", vAdd.LastName},
		//		{"email", v.Email},
		//		{"phone_number", vAdd.PhoneNumber},
		//		{"birthday", v.Birthday},
		//		{"sex", v.Sex},
		//		{"address", vAdd.Address},
		//		{"zip_code", vAdd.ZipCode},
		//		{"province_id", vAdd.ProvinceID},
		//	}}})
	}
}

func (d *registerRepository) ListData(payload *types.FilterRegisterList) *typesSDK.SaveRepositoryResponse {
	resultsEmpty := make([]*types.TableDimRegister, 0)

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	findOptions := options.Find()
	orderBy := -1
	sort := "updated_at"
	if payload.Sort != "" {
		sort = payload.Sort
	}
	if payload.Order == "asc" {
		orderBy = 1
	}
	findOptions.SetSort(&bson.D{{sort, orderBy}})
	findOptions.SetSkip(payload.Offset)
	findOptions.SetLimit(payload.Limit)

	var results []*types.TableDimRegister

	filter := bson.D{}
	if payload.Filter.Name != "" {
		filter = append(filter, bson.E{
			Key:   "name",
			Value: bson.D{{"$regex", fmt.Sprintf(".*%s.*", payload.Filter.Name)}},
		})
	}
	cur, err := collection.Find(context.TODO(), filter, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem types.TableDimRegister
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Data Not Found",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	_ = cur.Close(context.TODO())

	// Clear Data
	defer func() { results = nil }()

	// Output
	if results == nil {
		results = resultsEmpty
	}
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Rows:       results,
		Total:      0,
	}
}

func (d *registerRepository) ShowData(id string) *typesSDK.SaveRepositoryResponse {
	// Valid & Convert ID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
			Data:       nil,
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Data:       nil,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	var result *types.TableDimRegister
	filter := &bson.D{{"_id", _id}}
	err = collection.FindOne(context.TODO(), filter).Decode(&result)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Data:       nil,
		}
	}

	// Clear Data
	defer func() { result = nil }()

	// Output
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Data:       result,
	}
}

func (d *registerRepository) CreateData(payload *types.TableDimRegister) *typesSDK.SaveRepositoryResponse {
	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	// Add Update Line Profile
	collection := db.Collection(constLine.DimLineUser)
	var vLine *typesLine.TableDimLineUser
	filter := &bson.D{{"line_user_id", payload.LineUserID}}
	err = collection.FindOne(context.TODO(), filter).Decode(&vLine)
	if vLine != nil {
		payload.LineDisplayName = vLine.LineDisplayName
		payload.LineDisplayImage = vLine.LineDisplayImage
	}

	// Execute
	collection = db.Collection(d.CollectionName)
	payload.ID = primitive.NewObjectID()
	payload.CreatedAt = helperSDK.GetTimeNowGMT()
	payload.UpdatedAt = payload.CreatedAt
	res, err := collection.InsertOne(context.TODO(), payload)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Create Error",
		}
	}

	// Add Point
	collection = db.Collection(d.CollectionNamePoint)
	_, errPoint := collection.InsertOne(context.TODO(), &types.TableDimRegisterRafflePoint{
		RegisterId:     payload.ID,
		Point:          0,
		PointLuckyDraw: 0,
		PointPremium:   0,
	})

	// Output Point Error
	if errPoint != nil {
		fmt.Println(errPoint)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Create Error",
		}
	}

	// Add Address
	collection = db.Collection(constants.DimRegisterAddress)
	_, _ = collection.InsertOne(context.TODO(), &types.TableDimRegisterAddress{
		ID:          primitive.NewObjectID(),
		UserID:      payload.ID,
		FirstName:   payload.FirstName,
		LastName:    payload.LastName,
		PhoneNumber: payload.PhoneNumber,
		Address:     payload.Address,
		ProvinceID:  payload.ProvinceID,
		ZipCode:     payload.ZipCode,
		Default:     "1",
		CreatedAt:   payload.CreatedAt,
		UpdatedAt:   payload.CreatedAt,
	})

	// Output
	return &typesSDK.SaveRepositoryResponse{
		ID:         res.InsertedID,
		CodeReturn: 1,
		Message:    "Create Success",
	}
}

func (d *registerRepository) UpdateData(id string, payload *types.TableDimRegister) *typesSDK.SaveRepositoryResponse {
	// Valid & Convert ID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	filter := &bson.D{{"_id", _id}}
	s := &bson.D{
		{"first_name", payload.FirstName},
		{"last_name", payload.LastName},
		{"email", payload.Email},
		{"phone_number", payload.PhoneNumber},
		{"birthday", payload.Birthday},
		{"sex", payload.Sex},
		{"address", payload.Address},
		{"zip_code", payload.ZipCode},
		{"province_id", payload.ProvinceID},
		{"updated_at", helperSDK.GetTimeNowGMT()},
	}
	update := &bson.D{{"$set", s}}
	res, err := collection.UpdateOne(context.TODO(), filter, update)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			ID:         _id,
			CodeReturn: -1,
			Message:    "Update Error",
		}
	}

	// Output Error
	if res.ModifiedCount <= 0 {
		return &typesSDK.SaveRepositoryResponse{
			ID:         _id,
			CodeReturn: 2,
			Message:    "Data Not Found",
		}
	}

	// Output
	return &typesSDK.SaveRepositoryResponse{
		ID:         _id,
		CodeReturn: 1,
		Message:    "Update Success",
	}
}

func (d *registerRepository) DeleteData(id string) *typesSDK.SaveRepositoryResponse {
	// Valid & Convert ID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	filter := bson.D{{"_id", _id}}
	res, err := collection.DeleteOne(context.TODO(), filter)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			ID:         _id,
			CodeReturn: -1,
			Message:    "Delete Error",
		}
	}

	// Output Error
	if res.DeletedCount <= 0 {
		return &typesSDK.SaveRepositoryResponse{
			ID:         _id,
			CodeReturn: 2,
			Message:    "Data Not Found",
		}
	}

	// Output Error
	return &typesSDK.SaveRepositoryResponse{
		ID:         _id,
		CodeReturn: 1,
		Message:    "Delete Success",
	}
}

func (d *registerRepository) ReportRegister(payload *types.FilterReportRegisterRequest) *typesSDK.SaveRepositoryResponse {
	resultsEmpty := make([]*types.TableDimRegister, 0)
	var results []*types.TableDimRegister
	filter := bson.D{}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	findOptions := options.Find()
	// No Projection When Export
	if !payload.IsExport {
		findOptions.SetProjection(&bson.M{
			"address":            1,
			"created_at":         1,
			"first_name":         1,
			"last_name":          1,
			"phone_number":       1,
			"sex":                1,
			"line_display_image": 1,
			"line_display_name":  1,
		})
	}
	findOptions.SetSort(&bson.D{{"updated_at", 1}})

	// No Limit When Export
	if !payload.IsExport {
		findOptions.SetSkip(payload.Offset)
		findOptions.SetLimit(payload.Limit)
	}

	if payload.Filter.PhoneNumber != "" {
		filter = append(filter, bson.E{
			Key:   "phone_number",
			Value: bson.D{{"$regex", fmt.Sprintf(".*%s.*", payload.Filter.PhoneNumber)}},
		})
	}
	if !payload.Filter.StartDate.IsZero() && !payload.Filter.EndDate.IsZero() {
		filter = append(filter, bson.E{
			Key: "created_at",
			Value: bson.M{
				"$gte": payload.Filter.StartDate,
				"$lte": payload.Filter.EndDate,
			}})
	}
	cur, err := collection.Find(context.TODO(), filter, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem types.TableDimRegister
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Data Not Found",
			Rows:       resultsEmpty,
			Total:      0,
		}
	}

	_ = cur.Close(context.TODO())

	var total int64

	// No Total
	if !payload.IsExport {
		total, _ = collection.CountDocuments(context.TODO(), filter)
	}

	// Clear Data
	defer func() {
		filter = nil
		results = nil
		total = 0
	}()

	// Output
	if results == nil {
		results = resultsEmpty
	}
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Rows:       results,
		Total:      int32(total),
	}
}

func (d *registerRepository) GetPoint(registerID string) *typesSDK.SaveRepositoryResponse {
	// Valid & Convert ID
	_registerID, err := primitive.ObjectIDFromHex(registerID)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
			Data:       nil,
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Data:       nil,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionNamePoint)
	var result *types.TableDimRegisterRafflePoint
	filter := &bson.D{{"register_id", _registerID}}
	err = collection.FindOne(context.TODO(), filter).Decode(&result)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Data:       nil,
		}
	}

	// Clear Data
	defer func() { result = nil }()

	// Output
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Data:       result,
	}
}

func (d *registerRepository) GetPointByRegisterID(registerID string) *types.TableDimRegisterRafflePoint {
	result := &types.TableDimRegisterRafflePoint{}
	// Valid & Convert ID
	_registerID, err := primitive.ObjectIDFromHex(registerID)
	if err != nil {
		fmt.Println(err)
		return result
	}

	resBill := d.BillRepository.GetDataByUser(registerID)
	resPremium := d.PremiumRepository.RedeemHistory(registerID)
	resLucky := d.LuckyDrawRepository.RedeemHistory(registerID)
	billSummary := d.BillRepository.GetBillSummaryByUser(registerID)

	// Calc point_lucky_draw
	pointLuckyDraw := 0
	a1 := resLucky.Data.([]*types.TableFactLuckyDrawRedeem)
	for _, v2 := range a1 {
		pointLuckyDraw = pointLuckyDraw + v2.Point
	}

	// Calc point_premium
	pointPremium := 0
	a2 := resPremium.Data.([]*types.TableFactPremiumRedeem)
	for _, v2 := range a2 {
		pointPremium = pointPremium + v2.Point
	}

	// Calc point_premium
	point := 0
	a3 := resBill.Rows.([]*types.TableFactBill)
	for _, v3 := range a3 {
		point = point + v3.Point
	}

	result.RegisterId = _registerID
	result.Point = (point) - (pointLuckyDraw + pointPremium)
	result.PointLuckyDraw = pointLuckyDraw
	result.PointPremium = pointPremium
	result.TotalPoint = point
	result.BillSummary = billSummary
	return result
}

func (d *registerRepository) UpdatePointByRegisterID(registerID string) *typesSDK.SaveRepositoryResponse {
	// Valid & Convert ID
	_registerID, err := primitive.ObjectIDFromHex(registerID)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
		}
	}

	dp := d.GetPointByRegisterID(registerID)

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionNamePoint)
	res, err := collection.UpdateOne(context.TODO(),
		&bson.D{{"register_id", _registerID}},
		&bson.D{{"$set", &bson.D{
			{"point", dp.TotalPoint},
			{"point_lucky_draw", dp.PointLuckyDraw},
			{"point_premium", dp.PointPremium},
		}}})

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Update Error",
		}
	}

	// Output Error
	if res.ModifiedCount <= 0 {
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Data Not Found",
		}
	}

	// Output
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Update Success",
	}
}

func (d *registerRepository) ShowDataByMid(mid string) *typesSDK.SaveRepositoryResponse {
	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Data:       nil,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionName)
	var result *types.TableDimRegister
	filter := &bson.D{{"line_user_id", mid}}
	err = collection.FindOne(context.TODO(), filter).Decode(&result)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Data:       nil,
		}
	}

	collection = db.Collection(d.CollectionNameAddress)
	var resultAdd *types.TableDimRegisterAddress
	err = collection.FindOne(context.TODO(), &bson.D{
		{"user_id", result.ID},
		{"default", "1"}}).Decode(&resultAdd)

	if resultAdd != nil {
		result.Address = resultAdd.Address
		result.ZipCode = resultAdd.ZipCode
		result.ProvinceID = resultAdd.ProvinceID
		result.PhoneNumber = resultAdd.PhoneNumber
	}

	// Clear Data
	defer func() {
		result = nil
		resultAdd = nil
	}()

	// Output
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Data:       result,
	}
}

func (d *registerRepository) GetAddress(registerID string) *typesSDK.SaveRepositoryResponse {
	resultsEmpty := make([]*types.TableDimRegisterAddress, 0)

	// Valid & Convert ID
	_registerID, err := primitive.ObjectIDFromHex(registerID)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
			Rows:       resultsEmpty,
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Rows:       resultsEmpty,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionNameAddress)
	findOptions := options.Find()
	//findOptions.SetSort(bson.D{{"updated_at", -1}})

	var results []*types.TableDimRegisterAddress

	filter := &bson.D{{"user_id", _registerID}}
	cur, err := collection.Find(context.TODO(), filter, findOptions)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Rows:       resultsEmpty,
		}
	}

	// Assign Data Array
	for cur.Next(context.TODO()) {
		var elem types.TableDimRegisterAddress
		err := cur.Decode(&elem)
		if err != nil {
			fmt.Println(err)
		}
		results = append(results, &elem)
	}

	// Output Error
	if err := cur.Err(); err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Data Not Found",
			Rows:       resultsEmpty,
		}
	}

	_ = cur.Close(context.TODO())

	// Clear Data
	defer func() { results = nil }()

	// Output
	if results == nil {
		results = resultsEmpty
	}
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Rows:       results,
	}
}

func (d *registerRepository) GetAddressDefaultByRegID(registerID string) *typesSDK.SaveRepositoryResponse {
	// Valid & Convert ID
	_registerID, err := primitive.ObjectIDFromHex(registerID)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
			Data:       nil,
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Data:       nil,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionNameAddress)
	var result *types.TableDimRegisterAddress
	fmt.Println(_registerID)
	err = collection.FindOne(context.TODO(),
		&bson.D{
			{"user_id", _registerID},
			{"default", "1"}},
	).Decode(&result)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Data:       nil,
		}
	}

	// Clear Data
	defer func() { result = nil }()

	// Output
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Data:       result,
	}
}

func (d *registerRepository) GetAddressById(id string) *typesSDK.SaveRepositoryResponse {
	// Valid & Convert ID
	_id, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
			Data:       nil,
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Data:       nil,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionNameAddress)
	var result *types.TableDimRegisterAddress
	filter := &bson.D{{"_id", _id}}
	err = collection.FindOne(context.TODO(), filter).Decode(&result)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Data Error",
			Data:       nil,
		}
	}

	// Clear Data
	defer func() { result = nil }()

	// Output
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Success",
		Data:       result,
	}
}

func (d *registerRepository) CreateAddress(registerID string, payload *types.TableDimRegisterAddress) *typesSDK.SaveRepositoryResponse {
	// Valid & Convert ID
	_registerID, err := primitive.ObjectIDFromHex(registerID)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionNameAddress)
	payload.ID = primitive.NewObjectID()
	payload.UserID = _registerID
	payload.Default = "0"
	payload.CreatedAt = helperSDK.GetTimeNowGMT()
	payload.UpdatedAt = payload.CreatedAt
	res, err := collection.InsertOne(context.TODO(), payload)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Create Error",
		}
	}

	// Output
	return &typesSDK.SaveRepositoryResponse{
		ID:         res.InsertedID,
		CodeReturn: 1,
		Message:    "Create Success",
	}
}

func (d *registerRepository) UpdateAddress(addressID string, payload *types.TableDimRegisterAddress) *typesSDK.SaveRepositoryResponse {
	// Valid & Convert ID
	_id, err := primitive.ObjectIDFromHex(addressID)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionNameAddress)
	filter := &bson.D{{"_id", _id}}
	s := &bson.D{
		{"first_name", payload.FirstName},
		{"last_name", payload.LastName},
		{"phone_number", payload.PhoneNumber},
		{"address", payload.Address},
		{"province_id", payload.ProvinceID},
		{"zip_code", payload.ZipCode},
		{"updated_at", helperSDK.GetTimeNowGMT()},
	}
	update := &bson.D{{"$set", s}}
	res, err := collection.UpdateOne(context.TODO(), filter, update)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			ID:         _id,
			CodeReturn: -1,
			Message:    "Update Error",
		}
	}

	// Output Error
	if res.ModifiedCount <= 0 {
		return &typesSDK.SaveRepositoryResponse{
			ID:         _id,
			CodeReturn: 2,
			Message:    "Data Not Found",
		}
	}

	// Output
	return &typesSDK.SaveRepositoryResponse{
		ID:         _id,
		CodeReturn: 1,
		Message:    "Update Success",
	}
}

func (d *registerRepository) UpdateAddressToDefault(registerID string, addressID string) *typesSDK.SaveRepositoryResponse {
	// Valid & Convert ID
	_registerID, err := primitive.ObjectIDFromHex(registerID)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
			Data:       nil,
		}
	}

	_addressID, err := primitive.ObjectIDFromHex(addressID)
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Bad Request",
			Data:       nil,
		}
	}

	// Connect DB
	client, db, err := d.MongoClient(d.MongoDetail, d.DBName)

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Connection Fail",
			Data:       nil,
		}
	}
	defer client.Disconnect(context.TODO())

	// Execute
	collection := db.Collection(d.CollectionNameAddress)
	res, err := collection.UpdateOne(context.TODO(),
		&bson.D{{"_id", _addressID}},
		&bson.D{{"$set", &bson.D{
			{"default", "1"},
			{"updated_at", helperSDK.GetTimeNowGMT()},
		}}})

	// Output Error
	if err != nil {
		fmt.Println(err)
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: -1,
			Message:    "Update Error",
			Data:       nil,
		}
	}

	// Output Error
	if res.ModifiedCount <= 0 {
		return &typesSDK.SaveRepositoryResponse{
			CodeReturn: 2,
			Message:    "Data Not Found",
			Data:       nil,
		}
	}

	// Execute
	res, err = collection.UpdateMany(context.TODO(),
		&bson.D{
			{"user_id", _registerID},
			{"_id", &bson.D{{"$ne", _addressID}}},
		},
		&bson.D{{"$set", &bson.D{
			{"default", "0"},
			//{"updated_at", helperSDK.GetTimeNowGMT()},
		}}})

	// Output Error
	if err != nil {
		fmt.Println(err)
	}

	collection = db.Collection(d.CollectionNameAddress)
	var resultAdd *types.TableDimRegisterAddress
	err = collection.FindOne(context.TODO(), &bson.D{
		{"user_id", _registerID},
		{"default", "1"}}).Decode(&resultAdd)

	defer func() { resultAdd = nil }()

	// Output
	return &typesSDK.SaveRepositoryResponse{
		CodeReturn: 1,
		Message:    "Update Success",
		Data:       resultAdd,
	}
}
