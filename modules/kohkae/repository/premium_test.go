package repository

import (
	"fmt"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/kohkae/types"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	"os"
	"testing"
)

func TestPremiumRepository_CreateData(t *testing.T) {
	payload := &types.TableDimPremium{
		Name: "Koko",
	}

	a := NewPremiumRepository()
	res := a.CreateData(payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}

func TestPremiumRepository_ListData(t *testing.T) {
	payload := &types.FilterPremiumList{
		Sort:   "",
		Order:  "",
		Offset: 0,
		Limit:  10,
		//IsExport: False,
		Filter: &types.FilterPremiumListItems{
			Name: "",
		},
	}

	a := NewPremiumRepository()
	res := a.ListData(payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(helperSDK.InterfaceToJson(&res.Rows))
}

func TestPremiumRepository_ShowData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewPremiumRepository()
	res := a.ShowData(id)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(helperSDK.InterfaceToJson(&res.Data))
}

func TestPremiumRepository_UpdateData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewPremiumRepository()
	payload := &types.TableDimPremium{
		Name: "Koko",
	}
	res := a.UpdateData(id, payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}

func TestPremiumRepository_DeleteData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewPremiumRepository()
	res := a.DeleteData(id)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}

func TestPremiumRepository_GetDataItemByItemID(t *testing.T) {
	id := "5eb5dd85c9bff7f0b421d5d7"

	a := NewPremiumRepository()
	res := a.GetDataItemByItemID(id)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(helperSDK.InterfaceToJson(&res.Data))
}

func TestPremiumUpdateTransaction(t *testing.T) {
	_ = os.Setenv("ENV_STAGING", "prod")
	a := NewPremiumRepository()
	a.UpdateTransaction()
}
