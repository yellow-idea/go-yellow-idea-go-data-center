package repository

import (
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	"gitlab.yellow-idea.com/yellow-idea-go-data-center/modules/kohkae/types"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
	"go.mongodb.org/mongo-driver/bson"
	"testing"
)

func TestBillRepository_CreateData(t *testing.T) {
	payload := &types.TableFactBill{
		//Name: "Koko",
	}

	a := NewBillRepository()
	res := a.CreateData(payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}

func TestBillRepository_ListData(t *testing.T) {
	payload := &types.FilterBillList{
		Sort:   "",
		Order:  "",
		Offset: 0,
		Limit:  10,
		//IsExport: False,
		Filter: &types.FilterBillListItems{
			Name: "",
		},
	}

	a := NewBillRepository()
	res := a.ListData(payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(helperSDK.InterfaceToJson(&res.Rows))
}

func TestBillRepository_ShowData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewBillRepository()
	res := a.ShowData(id)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(helperSDK.InterfaceToJson(&res.Data))
}

func TestBillRepository_UpdateData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewBillRepository()
	payload := &types.TableFactBill{
		//Name: "Koko",
	}
	res := a.UpdateData(id, payload)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}

func TestBillRepository_DeleteData(t *testing.T) {
	id := "5eb3e51d5632533c72676747"

	a := NewBillRepository()
	res := a.DeleteData(id)

	if res.CodeReturn < 0 {
		t.Errorf("Code Return Fail = %d;", res.CodeReturn)
	}
	fmt.Println(res)
}

func TestExcel(t *testing.T) {
	f := excelize.NewFile()
	// Create a new sheet.
	index := f.NewSheet("Sheet2")
	// Set value of a cell.
	style, err := f.NewStyle(`{"fill":{"type":"pattern","color":["#E0EBF5"],"pattern":1}}`)
	if err != nil {
		fmt.Println(err)
	}
	_ = f.SetCellStyle("Sheet2", "A2", "A2", style)
	_ = f.SetCellValue("Sheet2", "A2", "Hello world.")
	_ = f.SetCellValue("Sheet1", "B2", 100)
	// Set active sheet of the workbook.
	f.SetActiveSheet(index)
	// Save xlsx file by the given path.
	if err := f.SaveAs("Book1.xlsx"); err != nil {
		fmt.Println(err)
	}
}

func TestReportBillCustomer2(t *testing.T) {
	a := NewBillRepository()
	res := a.ReportBillCustomer(&types.FilterReportRegisterRequest{

	})
	fmt.Println(helperSDK.InterfaceToJson(&res.Rows))
}

func TestBsonD(t *testing.T) {
	var filter2 *interface{}
	err := bson.UnmarshalExtJSON([]byte(`{"$or": [{"$or" : [{"bill_no":"1112"}]},{"$or" : [{"bill_no_admin":"1112"}]}] }`), true, &filter2)
	fmt.Println(err)
	fmt.Println(filter2)
}
