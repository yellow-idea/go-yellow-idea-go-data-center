package constants

import (
	"fmt"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
	"os"
)

// Chanel Detail
func GetDetailLineChanel() *typesSDK.LineChanelDetail {
	env := os.Getenv("ENV_STAGING")
	if env == "" {
		env = "local"
	}
	if env == "local" {
		return &typesSDK.LineChanelDetail{
			ChannelID:          "1655081165",
			ChannelSecret:      "743223c45f9f2638ade1807295de392f",
			ChannelAccessToken: "nwPGKA0K2/6zHDymTiW0CHtvIsvrZ2gFDVqCey86ySK1YXHrGMbM1xi+TuLjzDeAabkfg/2V+cZuEnk1pW8jsJNDGl0hjJU0+MlwHuanRKc5H0zGmqQmswURG9IMPgKrAKUD//mLSrbAjKs/8D0yDAdB04t89/1O/w1cDnyilFU=",
		}
	} else if env == "dev" {
		return &typesSDK.LineChanelDetail{
			ChannelID:          "1655081165",
			ChannelSecret:      "743223c45f9f2638ade1807295de392f",
			ChannelAccessToken: "nwPGKA0K2/6zHDymTiW0CHtvIsvrZ2gFDVqCey86ySK1YXHrGMbM1xi+TuLjzDeAabkfg/2V+cZuEnk1pW8jsJNDGl0hjJU0+MlwHuanRKc5H0zGmqQmswURG9IMPgKrAKUD//mLSrbAjKs/8D0yDAdB04t89/1O/w1cDnyilFU=",
		}
	} else if env == "prod" {
		return &typesSDK.LineChanelDetail{
			ChannelID:          "1655119353",
			ChannelSecret:      "20b0a8294e1a26474ee7ded34271f8b2",
			ChannelAccessToken: "f0FOubHUtthT5jZRqWwHXh8OLeLcI/hOco+aHzVj8N1N63jrpAvjlJNdZIrUKQ/E/a4DjW+iPQ+NA2B29Cpxp3TbiUoDqCjDgjmogPTl99+BdPaCeZzIAqtP3mb9D5MSweSG4dtjpzfAWXoSh9csOQdB04t89/1O/w1cDnyilFU=",
		}
	}
	return &typesSDK.LineChanelDetail{}
}

// Message
func GetMessageAfterAdminSaveBill(vDate string, vPoint int, BillSummary float32, vStatus int8) string {
	return fmt.Sprintf(`{
  "type": "flex",
  "altText": "แจ้งอัพเดทสถานะ",
  "contents": {
    "type": "bubble",
    "hero": {
      "type": "image",
      "url": "https://dev-app-yi-kohkae.s3-ap-southeast-1.amazonaws.com/assets/images/logo-top-right.png",
      "size": "xxl",
      "aspectRatio": "2:1",
      "aspectMode": "fit"
    },
    "body": {
      "type": "box",
      "layout": "vertical",
      "spacing": "md",
      "contents": [
        {
          "type": "text",
          "text": "แจ้งอัพเดทสถานะใบเสร็จ",
          "wrap": true,
          "size": "md",
          "weight": "bold"
        },
        {
          "type": "separator"
        },
        {
          "type": "box",
          "layout": "vertical",
          "spacing": "sm",
          "contents": [
            {
              "type": "box",
              "layout": "baseline",
              "contents": [
                {
                  "type": "text",
                  "text": "วันที่/เวลา:",
                  "margin": "sm",
                  "color": "#555555",
                  "flex": 0
                },
                {
                  "type": "text",
                  "text": "%s",
                  "size": "sm",
                  "align": "end"
                }
              ]
            },
            {
              "type": "box",
              "layout": "baseline",
              "contents": [
                {
                  "type": "text",
                  "text": "จำนวนสิทธิ์ที่ได้รับ:",
                  "margin": "sm",
                  "color": "#555555",
                  "flex": 0
                },
                {
                  "type": "text",
                  "text": "%d",
                  "size": "sm",
                  "align": "end"
                }
              ]
            },
            {
              "type": "box",
              "layout": "baseline",
              "contents": [
                {
                  "type": "text",
                  "text": "ยอดใบเสร็จ:",
                  "margin": "sm",
                  "color": "#555555",
                  "flex": 0
                },
                {
                  "type": "text",
                  "text": "%s",
                  "size": "sm",
                  "align": "end"
                }
              ]
            },
            {
              "type": "box",
              "layout": "baseline",
              "contents": [
                {
                  "type": "text",
                  "text": "สถานะ:",
                  "margin": "sm",
                  "color": "#555555",
                  "flex": 0
                },
                {
                  "type": "text",
                  "text": "%s",
                  "size": "sm",
                  "align": "end",
                  "color": "#ff0000"
                }
              ]
            }
          ]
        }
      ]
    }
  }
}`, vDate, vPoint, fmt.Sprintf("%.2f", BillSummary), GetStatusTitleBill(vStatus))
}
