## ประวัติส่งใบเสร็จ

###  Params
```json
{
  "user_id": "2607d660-8f70-11ea-b167-92c5b11b06cb",
  "start_date": "2020-05-09 00:00:00",
  "end_date": "2020-05-09 23:59:59",
  "status": -1
}
```

### Response
```json
{
  "data": [
    {
      "09/05/2020": [
        {
          "bill_no": "001",
          "id": "0238311c-91c8-11ea-a65e-32835c766fb3",
          "img_url": "https://s3-ap-southeast-1.amazonaws.com/dutchie-web-application-dev-s3-yellow.dutchie.upload/images/bill-upload/2607d660-8f70-11ea-b167-92c5b11b06cb-023836f8-91c8-11ea-a65e-32835c766fb3.png",
          "is_used": 0,
          "point": 0,
          "status_id": 2
        },
        {
          "bill_no": "003",
          "id": "09fa0cd6-91c8-11ea-a65e-32835c766fb3",
          "img_url": "https://s3-ap-southeast-1.amazonaws.com/dutchie-web-application-dev-s3-yellow.dutchie.upload/images/bill-upload/2607d660-8f70-11ea-b167-92c5b11b06cb-09fa1000-91c8-11ea-a65e-32835c766fb3.png",
          "is_used": 0,
          "point": 0,
          "status_id": 2
        }
      ]
    }
  ]
}
```

## ร้องเรียนใบเสร็จ

###  Params
```json
{
  "bill_id": "09fa0cd6-91c8-11ea-a65e-32835c766fb3"
}
```

### Response
```json
{
  "code_return" : 1,
  "message" : "Success"
}
```

## ร้องเรียนสถานะ ใบเสร็จไม่ถูกต้อง

###  Params
```json
{
  "bill_no": "001",
  "id": "0238311c-91c8-11ea-a65e-32835c766fb3",
  "img_url": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYA",
  "is_complaint": 1,
  "shop_type": "Name",
  "shop_type_2": "Name",
  "user_id": "2607d660-8f70-11ea-b167-92c5b11b06cb"
}
```

### Response
```json
{
  "code_return" : 1,
  "message" : "Success"
}
```