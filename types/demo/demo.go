package demo

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type TableDemo struct {
	ID        primitive.ObjectID `json:"id" bson:"_id"`
	Name      string             `json:"name" bson:"name"`
	LastName  string             `json:"last_name" bson:"last_name"`
	Age       int                `json:"age" bson:"age"`
	City      string             `json:"city" bson:"city"`
	CreatedAt time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time          `json:"updated_at" bson:"updated_at"`
	DeletedAt time.Time          `json:"deleted_at" bson:"deleted_at"`
}

type ListFilterDataRequest struct {
	Name string `json:"name" bson:"name"`
}

type ListDataRequest struct {
	Sort     string `json:"sort" bson:"sort"`
	Order    string `json:"order" bson:"order"`
	Offset   int64  `json:"offset" bson:"offset"`
	Limit    int64  `json:"limit" bson:"limit"`
	IsExport bool   `json:"is_export" bson:"is_export"`
	Filter   *ListFilterDataRequest
}
