package helper

import (
	"database/sql"
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	helperSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/helper"
)

type DataReportUNDashboardBill struct {
	LineUserID             string `json:"line_user_id"`
	BillId                 string `json:"bill_id"`
	CreatedAt              string `json:"created_at"`
	BillDateByAdmin        string `json:"bill_date_by_admin"`
	FirstName              string `json:"first_name"`
	LastName               string `json:"last_name"`
	CustomerID             string `json:"customer_id"`
	Address                string `json:"address"`
	ProvinceId             string `json:"province_id"`
	ZipCode                string `json:"zip_code"`
	Product1All            int    `json:"product_1_all"`
	Product1Unilever       int    `json:"product_1_unilever"`
	Product1Competitor     int    `json:"product_1_competitor"`
	Product2All            int    `json:"product_2_all"`
	Product2Unilever       int    `json:"product_2_unilever"`
	Product2Competitor     int    `json:"product_2_competitor"`
	Product3All            int    `json:"product_3_all"`
	Product3Unilever       int    `json:"product_3_unilever"`
	Product3Competitor     int    `json:"product_3_competitor"`
	CategoryName           string `json:"category_name"`
	ProductGroup           string `json:"product_group"`
	Sku                    string `json:"sku"`
	Price                  int    `json:"price"`
	Size                   string `json:"size"`
	Amount1                int    `json:"amount1"`
	Amount2                int    `json:"amount2"`
	Total                  int    `json:"total"`
	CompetitorProductGroup string `json:"competitor_product_group"`
	CompetitorSku          string `json:"competitor_sku"`
	CompetitorPrice        int    `json:"competitor_price"`
	CompetitorSize         string `json:"competitor_size"`
	CompetitorAmount1      int    `json:"competitor_amount1"`
	CompetitorAmount2      int    `json:"competitor_amount2"`
	CompetitorTotal        int    `json:"competitor_total"`
	ItemFavourite          string `json:"item_favourite"`
	BillSummary            string `json:"bill_summary"`
	UnileverBill           string `json:"unilever_bill"`
	ShopName               string `json:"shop_name"`
	BillNo                 string `json:"bill_no"`
}

type DataReportUNDashboardSummary struct {
	LineUserID   string `json:"line_user_id"`
	FirstName    string `json:"first_name"`
	LastName     string `json:"last_name"`
	PhoneNumber  string `json:"phone_number"`
	BillSummary  int    `json:"bill_summary"`
	UnileverBill int    `json:"unilever_bill"`
	CountBill    int    `json:"count_bill"`
	Product1     int    `json:"product_1"`
	Product2     int    `json:"product_2"`
	Product3     int    `json:"product_3"`
}

func ReportUNDashboardBill(fileName string, rows *sql.Rows) {
	f := excelize.NewFile()
	sheet1 := "Sheet1"
	setStyle := func(color string) string {
		return fmt.Sprintf(`{"fill":{"type":"pattern","color":["%s"],"pattern":1},"alignment":{"horizontal":"center","ident":1,"justify_last_line":true,"reading_order":0,"relative_indent":1,"shrink_to_fit":true,"text_rotation":45,"vertical":"","wrap_text":true}}`, color)
	}
	// Header
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(12, 1), "FW")
	style1, _ := f.NewStyle(setStyle("#ff6090"))
	_ = f.SetCellStyle(sheet1, helperSDK.ToXlsColumn(12, 1), helperSDK.ToXlsColumn(12, 1), style1)
	_ = f.MergeCell(sheet1, helperSDK.ToXlsColumn(12, 1), helperSDK.ToXlsColumn(27, 1))

	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(28, 1), "FS")
	_ = f.MergeCell(sheet1, helperSDK.ToXlsColumn(28, 1), helperSDK.ToXlsColumn(40, 1))
	style2, _ := f.NewStyle(setStyle("#d05ce3"))
	_ = f.SetCellStyle(sheet1, helperSDK.ToXlsColumn(28, 1), helperSDK.ToXlsColumn(44, 1), style2)

	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(45, 1), "Hair")
	_ = f.MergeCell(sheet1, helperSDK.ToXlsColumn(45, 1), helperSDK.ToXlsColumn(61, 1))
	style3, _ := f.NewStyle(setStyle("#6ec6ff"))
	_ = f.SetCellStyle(sheet1, helperSDK.ToXlsColumn(45, 1), helperSDK.ToXlsColumn(61, 1), style3)

	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(1, 2), "User ID")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(2, 2), "Customer ID")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(3, 2), "Address")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(4, 2), "Province")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(5, 2), "Post")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(6, 2), "Bill received")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(7, 2), "Time Visit")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(8, 2), "FirstName")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(9, 2), "LastName")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(10, 2), "Account")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(11, 2), "Category")

	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(12, 2), "Total FW Amount")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(13, 2), "UTT FW Amount")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(14, 2), "UTT FW Item brand")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(15, 2), "UTT FW Item segment")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(16, 2), "UTT FW Item size")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(17, 2), "UTT FW unit")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(18, 2), "UTT FW box")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(19, 2), "UTT FW Total")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(20, 2), "Competitor FW Amount")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(21, 2), "Competitor FW Item")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(22, 2), "UTT FW Competitor Item brand")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(23, 2), "UTT FW Competitor Item segment")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(24, 2), "UTT FW Competitor Item size")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(25, 2), "Competitor FW unit")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(26, 2), "Competitor FW box")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(27, 2), "Competitor FW Total")

	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(28, 2), "Total FS Amount")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(29, 2), "UTT FS Amount")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(30, 2), "UTT FS Item")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(31, 2), "UTT FS Item brand")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(32, 2), "UTT FS Item segment")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(33, 2), "UTT FS Item size")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(34, 2), "UTT FS unit")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(35, 2), "UTT FS box")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(36, 2), "UTT FW Total")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(37, 2), "Competitor FS Amount")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(38, 2), "Competitor FS Item")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(39, 2), "UTT FS Competitor Item brand")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(40, 2), "UTT FS Competitor Item segment")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(41, 2), "UTT FS Competitor Item size")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(42, 2), "Competitor FS unit")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(43, 2), "Competitor FS box")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(44, 2), "Competitor FS Total")

	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(45, 2), "Total Hair Amount")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(46, 2), "UTT Hair Amount")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(47, 2), "UTT Hair Item")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(48, 2), "UTT Hair Item brand")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(49, 2), "UTT Hair Item segment")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(50, 2), "UTT Hair Item size")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(51, 2), "UTT Hair unit")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(52, 2), "UTT Hair box")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(53, 2), "UTT Hair Total")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(54, 2), "Competitor Amount")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(55, 2), "Competitor Item")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(56, 2), "UTT Hair Competitor Item brand")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(57, 2), "UTT Hair Competitor Item segment")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(58, 2), "UTT Hair Competitor Item size")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(59, 2), "Competitor Hair unit")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(60, 2), "Competitor Hair box")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(61, 2), "Competitor Hair Total")

	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(62, 2), "Favourite")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(63, 2), "Total Spend")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(64, 2), "Total UTT Spend")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(65, 2), "Shop Name")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(66, 2), "Bill ID")

	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(1, 3), "เลขที่สมาชิก")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(2, 3), "รหัสลูกค้า")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(3, 3), "ที่อยู่")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(4, 3), "จังหวัด")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(5, 3), "รหัสไปรษณีย์")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(6, 3), "วันที่ส่งใบเสร็จ")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(7, 3), "เวลาออกใบเสร็จ")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(8, 3), "ชื่อ")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(9, 3), "นามสกุล")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(10, 3), "ชื่อร้าน")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(11, 3), "")

	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(12, 3), "ยอดซื้อผลิตภัณฑ์ซักผ้าทั้งหมด")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(13, 3), "ยอดซื้อผลิตภัณฑ์ซักผ้าของยูนิลีเวอร์")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(14, 3), "รายชื่อสินค้าผลิตภัณฑ์ซักผ้า")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(15, 3), "ประเภทของสินค้าผลิตภัณฑ์ซักผ้า")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(16, 3), "ขนาดของสินค้าผลิตภัณฑ์ซักผ้า")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(17, 3), "จำนวนชิ้น")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(18, 3), "จำนวนลัง")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(19, 3), "Total")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(20, 3), "ยอดซื้อผลิตภัณฑ์ซักผ้าคู่แข่ง")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(21, 3), "รายชื่อสินค้าผลิตภัณฑ์ซักผ้าคู่แข่ง")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(22, 3), "รายชื่อสินค้าผลิตภัณฑ์ซักผ้าคู่แข่ง")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(23, 3), "ประเภทของสินค้าผลิตภัณฑ์ซักผ้าคู่แข่ง")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(24, 3), "ขนาดของสินค้าผลิตภัณฑ์ซักผ้าคู่แข่ง")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(25, 3), "จำนวนชิ้น (คู่แข่ง)")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(26, 3), "จำนวนลัง (คู่แข่ง)")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(27, 3), "Total (คู่แข่ง)")

	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(28, 3), "ยอดซื้อผลิตภัณฑ์ปรับผ้านุ่มทั้งหมด")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(29, 3), "ยอดซื้อผลิตภัณฑ์ปรับผ้านุ่มยูนิลีเวอร์")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(30, 3), "รายชื่อสินค้าผลิตภัณฑ์ปรับผ้านุ่ม")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(31, 3), "รายชื่อสินค้าผลิตภัณฑ์ปรับผ้านุ่ม")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(32, 3), "ประเภทของสินค้าผลิตภัณฑ์ปรับผ้านุ่ม")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(33, 3), "ขนาดของสินค้าผลิตภัณฑ์ปรับผ้านุ่ม")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(34, 3), "จำนวนชิ้น")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(35, 3), "จำนวนลัง")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(36, 3), "Total")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(37, 3), "ยอดซื้อผลิตภัณฑ์ปรับผ้านุ่มคู่แข่ง")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(38, 3), "รายชื่อสินค้าผลิตภัณฑ์ปรับผ้านุ่มคู่แข่ง")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(39, 3), "รายชื่อสินค้าผลิตภัณฑ์ปรับผ้านุ่มคู่แข่ง")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(40, 3), "ประเภทของสินค้าผลิตภัณฑ์ปรับผ้านุ่มคู่แข่ง")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(41, 3), "ขนาดของสินค้าผลิตภัณฑ์ปรับผ้านุ่มคู่แข่ง")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(42, 3), "จำนวนชิ้น (คู่แข่ง)")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(43, 3), "จำนวนลัง (คู่แข่ง)")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(44, 3), "Total (คู่แข่ง)")

	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(45, 3), "ยอดซื้อผลิตภัณฑ์ดูแลเส้นผมทั้งหมด")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(46, 3), "ยอดซื้อผลิตภัณฑ์ดูแลเส้นผมยูนิลีเวอร์")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(47, 3), "รายชื่อสินค้าผลิตภัณฑ์ดูแลเส้นผม")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(48, 3), "รายชื่อสินค้าผลิตภัณฑ์ดูแลผม")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(49, 3), "ประเภทของสินค้าผลิตภัณฑ์ดูแลผม")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(50, 3), "ขนาดของสินค้าผลิตภัณฑ์ดูแลผม")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(51, 3), "จำนวนชิ้น")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(52, 3), "จำนวนลัง")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(53, 3), "Total")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(54, 3), "ยอดซื้อผลิตภัณฑ์ดูแลเส้นผมคู่แข่ง")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(55, 3), "รายชื่อสินค้าผลิตภัณฑ์ดูแลเส้นผมคู่แข่ง")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(56, 3), "รายชื่อสินค้าผลิตภัณฑ์ดูแลผมคู่แข่ง")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(57, 3), "ประเภทของสินค้าผลิตภัณฑ์ดูแลผมคู่แข่ง")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(58, 3), "ขนาดของสินค้าผลิตภัณฑ์ดูแลผมคู่แข่ง")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(59, 3), "จำนวนชิ้น (คู่แข่ง)")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(60, 3), "จำนวนลัง (คู่แข่ง)")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(61, 3), "Total (คู่แข่ง)")

	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(62, 3), "")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(63, 3), "ยอดซื้อทั้งหมดต่อใบเสร็จ")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(64, 3), "ยอดซื้อสินค้ายูนิลีเวอร์ต่อใบเสร็จ")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(65, 3), "")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(66, 3), "")

	// Body
	var i int
	//var lastLineID string
	var lastBillID string
	for rows.Next() {
		if i == 0 {
			i = i + 4
		}
		var v DataReportUNDashboardBill
		if err := rows.Scan(
			&v.LineUserID,
			&v.BillId,
			&v.CreatedAt,
			&v.BillDateByAdmin,
			&v.FirstName,
			&v.LastName,
			&v.CustomerID,
			&v.Address,
			&v.ProvinceId,
			&v.ZipCode,
			&v.Product1All,
			&v.Product1Unilever,
			&v.Product1Competitor,
			&v.Product2All,
			&v.Product2Unilever,
			&v.Product2Competitor,
			&v.Product3All,
			&v.Product3Unilever,
			&v.Product3Competitor,
			&v.CategoryName,
			&v.ProductGroup,
			&v.Sku,
			&v.Price,
			&v.Size,
			&v.Amount1,
			&v.Amount2,
			&v.Total,

			&v.CompetitorProductGroup,
			&v.CompetitorSku,
			&v.CompetitorPrice,
			&v.CompetitorSize,
			&v.CompetitorAmount1,
			&v.CompetitorAmount2,
			&v.CompetitorTotal,
			&v.ItemFavourite,
			&v.BillSummary,
			&v.UnileverBill,
			&v.ShopName,
		); err != nil {
			fmt.Println(err)
		}

		//// User 1:1
		//if lastLineID != v.LineUserID {
		//	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(1, i), v.LineUserID)
		//	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(4, i), v.FirstName)
		//	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(5, i), v.LastName)
		//	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(6, i), "") // Account
		//
		//	lastLineID = v.LineUserID
		//}

		//// Bill 1:1
		//if lastBillID != v.BillId {
		//	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(2, i), v.CreatedAt)       // Bill received
		//	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(3, i), v.BillDateByAdmin) // Time Visit
		//
		//	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(8, i), v.Product1All)         // Total FW Amount
		//	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(9, i), v.Product1Unilever)    // UTT FW Amount
		//	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(16, i), v.Product1Competitor) // Competitor FW Amount
		//	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(17, i), "")                   // Competitor FW Item
		//
		//	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(24, i), v.Product2All)        //Total FS Amount
		//	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(25, i), v.Product2Unilever)   // UTT FS Amount
		//	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(26, i), "")                   // UTT FS Item
		//	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(33, i), v.Product2Competitor) // Competitor FS Amount
		//	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(34, i), "")                   // Competitor FS Item
		//
		//	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(41, i), v.Product3All)        // Total Hair Amount
		//	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(42, i), v.Product3Unilever)   // UTT Hair Amount
		//	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(43, i), "")                   // UTT Hair Item
		//	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(50, i), v.Product3Competitor) // Competitor Amount
		//	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(51, i), "")                   // Competitor Item
		//
		//	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(58, i), v.ItemFavourite)
		//	lastBillID = v.BillId
		//}

		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(1, i), v.LineUserID)
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(2, i), v.CustomerID)
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(3, i), v.Address)
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(4, i), v.ProvinceId)
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(5, i), v.ZipCode)
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(6, i), v.CreatedAt)       // Bill received
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(7, i), v.BillDateByAdmin) // Time Visit
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(8, i), v.FirstName)
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(9, i), v.LastName)
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(10, i), "") // Account
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(11, i), v.CategoryName)

		// Unilever ซักผ้า
		if v.CategoryName == "ซักผ้า" && v.ProductGroup != "" {
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(12, i), v.Product1All)      // Total FW Amount
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(13, i), v.Product1Unilever) // UTT FW Amount
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(14, i), v.ProductGroup)     // UTT FW Item brand
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(15, i), v.Sku)              // UTT FW Item segment
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(16, i), v.Size)             // UTT FW Item size
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(17, i), v.Amount1)          // UTT FW unit
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(18, i), v.Amount2)          // UTT FW box
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(19, i), v.Total)
		} else {
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(12, i), "") // Total FW Amount
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(13, i), "") // UTT FW Amount
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(14, i), "") // UTT FW Item brand
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(15, i), "") // UTT FW Item segment
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(16, i), "") // UTT FW Item size
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(17, i), "") // UTT FW unit
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(18, i), "") // UTT FW box
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(19, i), "")
		}

		// Competitor ซักผ้า
		if v.CategoryName == "ซักผ้า" && v.ProductGroup == "" {
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(20, i), v.Product1Competitor)     // Competitor FW Amount
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(21, i), "")                       // Competitor FW Item
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(22, i), v.CompetitorProductGroup) // UTT FW Competitor Item brand
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(23, i), v.CompetitorSku)          // UTT FW Competitor Item segment
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(24, i), v.CompetitorSize)         // UTT FW Competitor Item size
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(25, i), v.CompetitorAmount1)      // Competitor FW unit
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(26, i), v.CompetitorAmount2)      // Competitor FW box
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(27, i), v.CompetitorTotal)
		} else {
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(20, i), "") // Competitor FW Amount
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(21, i), "") // Competitor FW Item
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(22, i), "") // UTT FW Competitor Item brand
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(23, i), "") // UTT FW Competitor Item segment
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(24, i), "") // UTT FW Competitor Item size
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(25, i), "") // Competitor FW unit
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(26, i), "") // Competitor FW box
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(27, i), "")
		}

		// Unilever ปรับผ้านุ่ม
		if v.CategoryName == "ปรับผ้านุ่ม" && v.ProductGroup != "" {
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(28, i), v.Product2All)      //Total FS Amount
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(29, i), v.Product2Unilever) // UTT FS Amount
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(30, i), "")                 // UTT FS Item
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(31, i), v.ProductGroup)     // UTT FS Item brand
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(32, i), v.Sku)              // UTT FS Item segment
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(33, i), v.Size)             // UTT FS Item size
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(34, i), v.Amount1)          // UTT FS unit
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(35, i), v.Amount2)          // UTT FS box
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(36, i), v.Total)
		} else {
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(28, i), v.Product2All)      //Total FS Amount
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(29, i), v.Product2Unilever) // UTT FS Amount
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(30, i), "")                 // UTT FS Item
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(31, i), "")                 // UTT FS Item brand
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(32, i), "")                 // UTT FS Item segment
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(33, i), "")                 // UTT FS Item size
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(34, i), "")                 // UTT FS unit
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(35, i), "")                 // UTT FS box
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(36, i), "")
		}

		// Competitor ปรับผ้านุ่ม
		if v.CategoryName == "ปรับผ้านุ่ม" && v.ProductGroup == "" {
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(37, i), v.Product2Competitor)     // Competitor FS Amount
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(38, i), "")                       // Competitor FS Item
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(39, i), v.CompetitorProductGroup) // UTT FS Competitor Item brand
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(40, i), v.CompetitorSku)          // UTT FS Competitor Item segment
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(41, i), v.CompetitorSize)         // UTT FS Competitor Item size
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(42, i), v.CompetitorAmount1)      // Competitor FS unit
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(43, i), v.CompetitorAmount2)      // Competitor FS box
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(44, i), v.Total)
		} else {
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(37, i), "") // Competitor FS Amount
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(38, i), "") // Competitor FS Item
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(39, i), "") // UTT FS Competitor Item brand
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(40, i), "") // UTT FS Competitor Item segment
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(41, i), "") // UTT FS Competitor Item size
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(42, i), "") // Competitor FS unit
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(43, i), "") // Competitor FS box
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(44, i), "")
		}

		// Unilever เส้นผม
		if v.CategoryName == "แชมพู" && v.ProductGroup != "" {
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(45, i), v.Product3All)      // Total Hair Amount
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(46, i), v.Product3Unilever) // UTT Hair Amount
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(47, i), "")                 // UTT Hair Item
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(48, i), v.ProductGroup)     // UTT Hair Item brand
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(49, i), v.Sku)              // UTT Hair Item segment
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(50, i), v.Size)             // UTT Hair Item size
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(51, i), v.Amount1)          // UTT Hair unit
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(52, i), v.Amount2)          // UTT Hair box
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(53, i), v.Total)
		} else {
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(45, i), "") // Total Hair Amount
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(46, i), "") // UTT Hair Amount
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(47, i), "") // UTT Hair Item
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(48, i), "") // UTT Hair Item brand
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(49, i), "") // UTT Hair Item segment
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(50, i), "") // UTT Hair Item size
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(51, i), "") // UTT Hair unit
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(52, i), "") // UTT Hair box
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(53, i), "")
		}

		// Competitor เส้นผม
		if v.CategoryName == "แชมพู" && v.ProductGroup == "" {
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(54, i), v.Product3Competitor)     // Competitor Amount
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(55, i), "")                       // Competitor Item
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(56, i), v.CompetitorProductGroup) // UTT Hair Competitor Item brand
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(57, i), v.CompetitorSku)          // UTT Hair Competitor Item segment
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(58, i), v.CompetitorSize)         // UTT Hair Competitor Item size
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(59, i), v.CompetitorAmount1)      // Competitor Hair unit
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(60, i), v.CompetitorAmount2)      // Competitor Hair box
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(61, i), v.Total)
		} else {
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(54, i), "") // Competitor Amount
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(55, i), "")                   // Competitor Item
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(56, i), "") // UTT Hair Competitor Item brand
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(57, i), "") // UTT Hair Competitor Item segment
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(58, i), "") // UTT Hair Competitor Item size
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(59, i), "") // Competitor Hair unit
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(60, i), "") // Competitor Hair box
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(61, i), "")
		}

		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(62, i), v.ItemFavourite)

		if lastBillID != v.BillId {
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(63, i), v.BillSummary)
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(64, i), v.UnileverBill)
			_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(66, i), v.BillId)
			lastBillID = v.BillId
		}
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(65, i), v.ShopName)

		i++
	}
	if err := f.SaveAs("/tmp/" + fileName); err != nil {
		fmt.Println(err)
	}
}

func ReportUNDashboardSummary(fileName string, rows *sql.Rows) {
	f := excelize.NewFile()
	sheet1 := "Sheet1"
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(1, 1), "User ID")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(2, 1), "Account")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(3, 1), "City")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(4, 1), "Region")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(5, 1), "Week")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(6, 1), "First Name")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(7, 1), "Last Name")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(8, 1), "Mobile")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(9, 1), "Redeem Gold Q1")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(10, 1), "Redeem Gold Q2")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(11, 1), "Redeem Gold Q3")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(12, 1), "Redeem Gold Q4")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(13, 1), "\"Cumulative spend \n(ตั้งแต่เริ่มแคมเปญ)\"")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(14, 1), "\"Cumulative Store Visit\n(ตั้งแต่เริ่มแคมเปญ)\"")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(15, 1), "#Upload bill")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(16, 1), "Amount total bill")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(17, 1), "UTT Amount")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(18, 1), "FW Amount")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(19, 1), "FS Amount")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(20, 1), "Hair Amount")

	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(1, 2), "รหัสไอดี")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(2, 2), "ชื่อร้าน")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(3, 2), "จังหวัด")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(4, 2), "ภูมิภาค")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(5, 2), "วันที่ เวลา เข้าไปซื้อของ")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(6, 2), "ชื่อ")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(7, 2), "นามสกุล")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(8, 2), "เบอร์โทร")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(9, 2), "รับทองครั้งที่ 1")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(10, 2), "รับทองครั้งที่ 2")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(11, 2), "รับทองครั้งที่ 3")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(12, 2), "รับทองครั้งที่ 4")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(13, 2), "ยอดซื้อรวม")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(14, 2), "จำนวนการเข้าร้านรวม")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(15, 2), "จำนวนบิลที่ส่งมา")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(16, 2), "ยอดซื้อทั้งหมด")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(17, 2), "ยอดซื้อของยูนิลีเวอร์")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(18, 2), "ยอดซื้อผลิตภัณฑ์ซักผ้า")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(19, 2), "ยอดซื้อผลิตภัณฑ์ปรับผ้านุ่ม")
	_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(20, 2), "ยอดซื้อผลิตภัณฑ์ดูแลผม")

	i := 3
	for rows.Next() {
		var v DataReportUNDashboardSummary
		if err := rows.Scan(
			&v.LineUserID,
			&v.FirstName,
			&v.LastName,
			&v.PhoneNumber,
			&v.BillSummary,
			&v.UnileverBill,
			&v.CountBill,
			&v.Product1,
			&v.Product2,
			&v.Product3,
		); err != nil {
			fmt.Println(err)
		}
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(1, i), v.LineUserID)
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(2, i), "")
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(3, i), "")
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(4, i), "")
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(5, i), "")
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(6, i), v.FirstName)
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(7, i), v.LastName)
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(8, i), v.PhoneNumber)
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(9, i), "")
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(10, i), "")
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(11, i), "")
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(12, i), "")
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(13, i), "")
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(14, i), "")
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(15, i), v.CountBill)
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(16, i), v.BillSummary)
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(17, i), v.UnileverBill)
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(18, i), v.Product1)
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(19, i), v.Product2)
		_ = f.SetCellValue(sheet1, helperSDK.ToXlsColumn(20, i), v.Product3)
		i++
	}

	if err := f.SaveAs("/tmp/" + fileName); err != nil {
		fmt.Println(err)
	}
}
